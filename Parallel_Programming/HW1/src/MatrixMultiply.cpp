#include "MatrixMultiply.hpp"

#include <exception>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <numeric>

scottgs::MatrixMultiply::MatrixMultiply()
{
	;
}

scottgs::MatrixMultiply::~MatrixMultiply()
{
	;
}

scottgs::FloatMatrix scottgs::MatrixMultiply::operator()(const scottgs::FloatMatrix &lhs, const scottgs::FloatMatrix &rhs) const
{
	// Verify acceptable dimensions
	if (lhs.size2() != rhs.size1())
		throw std::logic_error("matrix incompatible lhs.size2() != rhs.size1()");

	scottgs::FloatMatrix result(lhs.size1(), rhs.size2());

	// YOUR ALGORIHM WITH COMMENTS GOES HERE:

//Create constants for the matrix sizes used in the loop to avoid repeat function calls
	const unsigned int lr1Size = lhs.size1();
	const unsigned int rc2Size = rhs.size2();
	const unsigned int rc1Size = lhs.size2();

	//Set the block size used for cache blocking
	const unsigned int bsize = 128;

	for (int jj = 0; jj < rc2Size; jj += bsize) //Create a cache block for the j-loop
	{
		for (int kk = 0; kk < rc1Size; kk += bsize) //Create cache block for the k-loop
		{
			for (int i = 0; i < lr1Size; i++) // Access the rows
			{
				//Determine if there is a full cache block for the j loop or if it is the end of the matrix
				const unsigned int jBound = std::min(jj + bsize, rc2Size);
				for (int j = jj; j < jBound; j++) //Access the columns
				{
					double sum = 0.0;
					//Determine if there is a full cache block for the k loop or if it is the end of the matrix
					const unsigned int kBound = std::min(kk + bsize, rc1Size);
					for (int k = kk; k < kBound; k++) //Traverse the vectors to perform dot product
					{
						sum += lhs(i, k) * rhs(k, j); //Dot product
					}
					result(i, j) += sum; //Place result of dot product in result matrix
				}
			}
		}
	}
	return result;
}

scottgs::FloatMatrix scottgs::MatrixMultiply::multiply(const scottgs::FloatMatrix &lhs, const scottgs::FloatMatrix &rhs) const
{
	// Verify acceptable dimensions
	if (lhs.size2() != rhs.size1())
		throw std::logic_error("matrix incompatible lhs.size2() != rhs.size1()");

	return boost::numeric::ublas::prod(lhs, rhs);
}
