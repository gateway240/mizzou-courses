#include "hw3Threader.hpp"

#include <stdexcept>
#include <iostream>

#include <boost/thread/thread.hpp>

using namespace std;

abeattie::HwThreeThreader::HwThreeThreader( vector<float3> dataVector, unsigned dataSize, unsigned numProc, unsigned radius) 
{
	std::cout << "Constructing HwThreeThreader" << std::endl;
	_oldPoints = new float3[dataSize];
	_newPoints = new float3[dataSize];
	std::copy(dataVector.begin(), dataVector.end(), _oldPoints);
	std::cout << "Copied data correctly" << endl;
	_dataSize = dataSize;
	_radius = radius;
	_totalDistance = 0;
	_numProc = numProc;
	_workerNodeBlock = dataSize / numProc;
	_remainder = dataSize % numProc;
	_index = 0;
	_mutex = new boost::mutex();
	_indexLock = new boost::mutex();
	std::cout << "Finished Constructing HwThreeThreader" << std::endl;
}

abeattie::HwThreeThreader::~HwThreeThreader()
{

	delete[] _oldPoints;
	std::cout << "Delete oldPoints" << endl;
	delete _indexLock;
	std::cout << "Delete indexLock" << endl;
	delete _mutex;
	std::cout << "Delete mutex" << endl;
}

void abeattie::HwThreeThreader::resetDistance()
{
	_totalDistance = 0;
}
void abeattie::HwThreeThreader::resetIndex()
{
	_index = 0;
}
double abeattie::HwThreeThreader::getTotalDistance()
{
	return _totalDistance;
}
void abeattie::HwThreeThreader::prepNextIteration(int iteration)
{
	if(iteration == 1){
		delete[] (_oldPoints);
	}
	_oldPoints = _newPoints;
}
float3*abeattie::HwThreeThreader::getOldPoints()
{
	return _oldPoints;
}

void abeattie::HwThreeThreader::operator()()
{
	// Who am I?
	const boost::thread::id id = boost::this_thread::get_id();

	//std::cout << "Entered functor for thread (" << id << ")" << std::endl;

	int threadIndex;

	// NOTE: below, all but the sleep is within MUTEX
	// This is a toy example, in real work you only mutex
	// the critical index manipulation

	// do MUTEX segment
	int workerNum;
	{
		boost::mutex::scoped_lock lock(*(_indexLock));
		workerNum = _index;
		_index++;
	} // mutex released
	//cout << "Worker num: " << workerNum << endl;
	double localDistance = 0; // variable for total distance moved in the process

	unsigned int start = (_workerNodeBlock)*workerNum;  // set wehere the process starts processing data
	unsigned int iterator = start + (_workerNodeBlock); //set where the process ends processing data (add reaminder if last rpocess)
	if (workerNum == _numProc - 1)
	{
		iterator += _remainder;
	}
	unsigned blockSize = sizeof(float3) * (iterator - start);
	float3*coordinateBlock = (float3*)malloc(blockSize); //local storage for process to compute it's block of data
	unsigned coordinateBlockItr = 0;
	for (unsigned i = start; i < iterator; i++)
	{

		const float3 currentPoint = _oldPoints[i];
		float3 newPoint = CoordinateFun::calculateWeightedMean( _oldPoints, _dataSize, currentPoint, _radius);

		double distanceMoved = CoordinateFun::haversine(currentPoint.x, currentPoint.y, newPoint.x, newPoint.y);

		if (distanceMoved > 0)
		{
			localDistance += distanceMoved;
		}
		//store computed values in local storage block
		coordinateBlock[coordinateBlockItr] = newPoint;
		coordinateBlockItr++;
	}
	{
		boost::mutex::scoped_lock lock(*(_mutex));
		//cout << "writing to memory: " << start << endl;
		memcpy(&_newPoints[start], coordinateBlock, blockSize);
		_totalDistance += localDistance;
	}
	free(coordinateBlock);
}
