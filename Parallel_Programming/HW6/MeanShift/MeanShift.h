#ifndef MEAN_SHIFT_H
#define MEAN_SHIFT_H

#include "cuda_runtime.h"

//    //GPU Mean Shift
    double MeanShiftGPU( float3* _oldPoints, float3* _newPoints, float* distanceMoved, int dataSize);

    #endif
