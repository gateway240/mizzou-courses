#ifndef COORDINATES_H_
#define COORDINATES_H_

#include <vector>
#include "cuda_runtime.h"
using namespace std;
class CoordinateFun
{
public:
	static double deg2rad(double deg);
	static double haversine(double lat1d, double lon1d, double lat2d, double lon2d);
	static float3 calculateWeightedMean( const float3*keys, unsigned size, const float3 iKey, const int radius);
}; // namespace coordinates
#endif
