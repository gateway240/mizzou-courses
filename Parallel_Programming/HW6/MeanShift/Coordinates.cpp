#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <deque>
#include <cstring>

#include "Coordinates.hpp"

const double M_PI = 3.14159265359;
using namespace std;
// This function converts decimal degrees to radians
double CoordinateFun::deg2rad(double deg)
{
	return (deg * M_PI / 180);
}

/**
 * Returns the distance between two points on the Earth.
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in meters
 */
double CoordinateFun::haversine(double lat1d, double lon1d, double lat2d, double lon2d)
{
	double lat1r, lon1r, lat2r, lon2r, u, v;
	lat1r = deg2rad(lat1d);
	lon1r = deg2rad(lon1d);
	lat2r = deg2rad(lat2d);
	lon2r = deg2rad(lon2d);

	double dlon = lon2r - lon1r;
	double dlat = lat2r - lat1r;
	double a = pow(sin(dlat / 2), 2) + cos(lat1r) * cos(lat2r) * pow(sin(dlon / 2), 2);
	double c = 2 * asin(sqrt(a));
	double r = 6371008.8; // Mean Radius of earth in meters
	return r * c;
}
float3 CoordinateFun::calculateWeightedMean(const float3 *keys, unsigned size, const float3 iKey, const int radius)
{

	double xVal = 0;
	double yVal = 0;
	double weightVal = 0;

	//calculate weighted mean based on points in neighborhood
	for (unsigned j = 0; j < size; ++j)
	{

		const float3 jKey = keys[j];

		const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

		const float weight = jKey.z;

		if (distance <= radius)
		{
			xVal += jKey.x * weight;
			yVal += jKey.y * weight;
			weightVal += weight;
		}
	}

	return make_float3(xVal / weightVal, yVal / weightVal, weightVal);
}
