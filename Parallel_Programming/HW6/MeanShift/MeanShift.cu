#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <iostream>
#include <time.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>

#include "MeanShift.h"
#define TILE_SIZE 256
#define _radius 150
#define ROW_SIZE 5
#define ROWS_TO_CHECK 25
#define POINT_SPACING 64
#define WINDOW_OFFSET (ROW_SIZE * ROWS_TO_CHECK)

// Texture reference for 2D float texture
//texture<float, 2, cudaReadModeElementType> tex;

__device__ double deg2rad(double deg)
{
	const double M_PI = 3.14159265359;
	return (deg * M_PI / 180);
}

/**
 * Returns the distance between two points on the Earth.
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in meters
 */
__device__ double haversine(double lat1d, double lon1d, double lat2d, double lon2d)
{
	double lat1r, lon1r, lat2r, lon2r;
	//degrees to radians
	lat1r = deg2rad(lat1d);
	lon1r = deg2rad(lon1d);
	lat2r = deg2rad(lat2d);
	lon2r = deg2rad(lon2d);

	double dlon = lon2r - lon1r;
	double dlat = lat2r - lat1r;
	double a = pow(sin(dlat / 2), 2) + cos(lat1r) * cos(lat2r) * pow(sin(dlon / 2), 2);
	
	double c = 2 * asin(sqrt(a));
	double r = 6371008.8; // Mean Radius of earth in meters
	return r * c;
}
__device__ float3 calculateWeightedMean(float3* keys, unsigned size, const float3 iKey, const int radius, const int li)
{

	double xVal = 0;
	double yVal = 0;
	double weightVal = 0;

	// calculate weighted mean based on points in neighborhood
	for (unsigned j = li - WINDOW_OFFSET; j < li + WINDOW_OFFSET; ++j)
	{

		const float3 jKey = keys[j];

		const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

		const double weight = jKey.z;

		if (distance <= radius)
		{
			xVal += jKey.x * weight;
			yVal += jKey.y * weight;
			weightVal += weight;
		}
	}

	float3 newCord = make_float3( xVal / weightVal, yVal / weightVal, iKey.z );
	return newCord;
}

__global__ void meanShiftKernel(float3* inputPoints, float3* outputPoints, int dataSize, float* totalDistance)
{
	const int localSize = (TILE_SIZE + WINDOW_OFFSET * 2);
	__shared__ float3 localPoints[localSize];
	// Set colum for thread.
	int ti = threadIdx.x;
	int i = (blockIdx.x * blockDim.x) + ti;
	int li = WINDOW_OFFSET + ti;
	if (i < dataSize) {

	const float3 currentPoint = inputPoints[i];
	localPoints[li] = currentPoint;
	if (ti == 0) {
		int startGlobalIndex = i - WINDOW_OFFSET;
		for (int j = 0; j < WINDOW_OFFSET; j++) {
			localPoints[j] = inputPoints[startGlobalIndex];
			startGlobalIndex++;
		}
	}
	else if (ti == (TILE_SIZE - 1)) {
		int startGlobalIndex = i;
		for (int k = li; k <= li + WINDOW_OFFSET; k++) {
			localPoints[k] = inputPoints[startGlobalIndex];
			startGlobalIndex++;
		}
	}
	__syncthreads();
	float3 newPoint = calculateWeightedMean(localPoints, localSize, currentPoint, _radius, li);

	float distanceMoved = haversine(currentPoint.x, currentPoint.y, newPoint.x, newPoint.y);
	totalDistance[i] = 0;

	if (distanceMoved > 0 && !isnan(distanceMoved))
	{
		totalDistance[i] = distanceMoved;
	}
	//store computed values in output storage block
	outputPoints[i] = newPoint;
	}
	
}


double MeanShiftGPU(float3*  _oldPoints, float3*_newPoints, float* distanceMoved, int dataSize) {
	//Cuda error and image values.
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEvent_t startCopyIn, stopCopyIn;
	cudaEventCreate(&startCopyIn);
	cudaEventCreate(&stopCopyIn);
	cudaEvent_t startKernel, stopKernel;
	cudaEventCreate(&startKernel);
	cudaEventCreate(&stopKernel);
	cudaEvent_t startCopyOut, stopCopyOut;
	cudaEventCreate(&startCopyOut);
	cudaEventCreate(&stopCopyOut);
	cudaEventRecord(start);
	cudaError_t status;

	//initialize points.
	float3* deviceinputPoints;
	cudaMalloc((void**)&deviceinputPoints, dataSize * sizeof(float3));
	status = cudaGetLastError();
	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMalloc : " << cudaGetErrorString(status) <<
			std::endl;
		return false;
	}
	cudaEventRecord(startCopyIn);
	cudaMemcpy(deviceinputPoints, _oldPoints, dataSize * sizeof(float3), cudaMemcpyHostToDevice);
	cudaEventRecord(stopCopyIn);
	cudaEventSynchronize(stopCopyIn);
	float timeCopyIn = 0;
	cudaEventElapsedTime(&timeCopyIn, startCopyIn, stopCopyIn);
	printf("Copy In time %f ms\n", timeCopyIn);
	status = cudaGetLastError();
	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMemcpy cudaMemcpyHostToDevice: " << cudaGetErrorString(status) <<
			std::endl;
		cudaFree(deviceinputPoints);
		return false;
	}
	float3* deviceOutputPoints;
	cudaMalloc((void**)&deviceOutputPoints, dataSize * sizeof(float3));
	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMalloc : " << cudaGetErrorString(status) <<
			std::endl;
		return false;
	}

	int blockSize = (int)ceil((float)dataSize / (float)TILE_SIZE);
	float threads = TILE_SIZE;

	printf("Block Size: %d\n", blockSize);

	float* totalDistance;
	cudaMalloc((void**)&totalDistance,dataSize * sizeof(float) );
	cudaMemset(totalDistance, 0, dataSize * sizeof(float));

	// call the kernel
	cudaEventRecord(startKernel);
	meanShiftKernel <<<blockSize, threads>>> (deviceinputPoints, deviceOutputPoints, dataSize, totalDistance);
	cudaEventRecord(stopKernel);
	cudaEventSynchronize(stopKernel);
	float timeKernel = 0;
	cudaEventElapsedTime(&timeKernel, startKernel, stopKernel);
	printf("Kenerl time: %f ms\n", timeKernel);

	cudaMemcpy(distanceMoved, totalDistance, dataSize * sizeof(float), cudaMemcpyDeviceToHost);

	// save output points to host.
	cudaEventRecord(startCopyOut);
	cudaMemcpy(_newPoints, deviceOutputPoints, dataSize * sizeof(float3), cudaMemcpyDeviceToHost);
	cudaEventRecord(stopCopyOut);
	cudaEventSynchronize(stopCopyOut);
	float timeCopyOut = 0;
	cudaEventElapsedTime(&timeCopyOut, startCopyOut, stopCopyOut);
	printf("Copy Out time: %f ms\n", timeCopyOut);
	status = cudaGetLastError();

	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMemcpy cudaMemcpyDeviceToHost: " << cudaGetErrorString(status) <<
			std::endl;
		cudaFree(deviceinputPoints);
		cudaFree(deviceOutputPoints);
		return false;
	}
	//Free the memory
	cudaFree(deviceinputPoints);
	cudaFree(deviceOutputPoints);
	cudaFree(totalDistance);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float time = 0;
	cudaEventElapsedTime(&time, start, stop);
	printf("Total Time GPU: %f ms\n", time);
	return true;
}
