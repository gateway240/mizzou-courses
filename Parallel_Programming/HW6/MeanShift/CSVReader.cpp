#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <chrono>
#include <time.h>


#include "CSVReader.hpp"
#include "parser.hpp"

using namespace std;
using namespace aria::csv;


vector<float3> CSVReader::getData(int colFilter, double rowThreshold)
{

	ifstream file(fileName);
	CsvParser parser = CsvParser(file)
		.delimiter(':');
	vector<float3> dataVector;
	//if (file.fail()) {
	//	cerr << "Error: " << strerror(errno) << endl;
	//	return dataVector;
	//}
	string line = "";
	linesParsed = 0;
	
	// Iterate through each line and split the content using delimeter
	for (auto& vec : parser) {
		if (stod(vec[colFilter + 2]) >= rowThreshold)
		{
			float3 coordinate = make_float3(stod(vec[1]), stod(vec[0]), stod(vec[colFilter + 2]));

			dataVector.push_back(coordinate);

		}
		linesParsed++;
	}
	
	// Close the File
	file.close();
	return dataVector;
}
