#include<iostream>
#include <ctime>
#include <chrono>

#include "MeanShift.h"
#include "CSVReader.hpp"
#include "Coordinates.hpp"
#include "hw3Threader.hpp"
#include <boost/thread.hpp>

#define ITERATIONS ( 1 )
using namespace std;

int main(int argc, char* argv[])
{

	if (argc != 4) {
		std::cout << "Invalid number of arguments entered : " << argc << std::endl;
		return 1;
	}

	string fileStr = argv[1];
	int column = atoi(argv[2]);
	double threshold = stof(argv[3]);

	cout << "--------------------------begenning of program--------------------------" << endl;
	cout << "Input Parameters: " << endl;
	cout << "Threshold: " << threshold << endl;
	cout << "Column: " << column << endl;
	cout << "File: " << fileStr << endl;

	// Creating an object of CSVWriter
	CSVReader reader(fileStr);
	// Get the data from CSV File
	auto parseStart = chrono::system_clock::now();
	vector<float3> dataVector = reader.getData(column, threshold);
	auto parseEnd = chrono::system_clock::now();
	chrono::duration<double> parseElapsedTime = parseEnd - parseStart;

	cout << "Lines in File : " << reader.linesParsed << endl;
	cout << "Records Loaded : " << dataVector.size() << endl;
	cout << "Time to Load File : " << parseElapsedTime.count() << " s " << endl;

	const unsigned int dataSize = dataVector.size();

	unsigned int threads = 8;

	unsigned workerNodeBlock = dataSize / threads; //size of block to use for each worker
	unsigned remainder = dataSize % threads;       // reaminder for last worker
	cout << "WorkerNodeBlock: " << workerNodeBlock << " Remainder: " << remainder << endl;
	abeattie::HwThreeThreader hwThreeThreader( dataVector, dataSize, threads, 150);
	// Create a thread group
	boost::thread_group tg;

	float3* _oldPoints;
	float3* _newPoints;
	std::cout << "Setting up dataset" << std::endl;
	_oldPoints = new float3[dataSize];

	std::copy(dataVector.begin(), dataVector.end(), _oldPoints);
	
	std::cout << "Old Points first XVal: " << _oldPoints[0].x << " , Y: " << _oldPoints[0].y << " WeightVal: " << _oldPoints[0].z << std::endl;

	float* distanceMoved;
	distanceMoved = new float[dataSize];

	for (int i = 0; i < ITERATIONS; i++)
	{
		//GPU
		_newPoints = new float3[dataSize];
		MeanShiftGPU(_oldPoints, _newPoints, distanceMoved, dataSize);

		//CPU
		hwThreeThreader.resetDistance();
		hwThreeThreader.resetIndex();
		auto weightedMeanStart = chrono::system_clock::now();
		for (unsigned t = 0; t < threads; ++t)
		{
			tg.create_thread(boost::ref(hwThreeThreader));
		}

		// Block until all threads are completed
		// The print answer
		std::cout << "About to block and wait on all my threads." << std::endl;
		tg.join_all();
		double itrDistanceCPU = hwThreeThreader.getTotalDistance();
		cout << "Total Distance Moved during Iteration CPU : " << itrDistanceCPU << " m" << endl;
		auto weightedMeanEnd = chrono::system_clock::now();
		chrono::duration<double> weightedMeanElapsedTime = weightedMeanEnd - weightedMeanStart;
		cout << "Compute Time for Weighted Mean Iteration 1 CPU is: " << (weightedMeanElapsedTime.count()) << " s " << endl;
		hwThreeThreader.prepNextIteration(i);
	}
	float3* cpuPoints = hwThreeThreader.getOldPoints();
	float totalDistance = 0;
	float errDist = 0;
	cout << "Comparing GPU and CPU results" << endl;
	for (int i = 0; i < dataSize; i++) {
		errDist += CoordinateFun::haversine(cpuPoints[i].x, cpuPoints[i].y, _newPoints[i].x, _newPoints[i].y);
	}
	for (int i = 0; i < dataSize; i++) {
		totalDistance += distanceMoved[i];
	}
	std::cout << "Error Distance (m) : " << errDist << std::endl;
	std::cout << "Total Distance: " << totalDistance << std::endl;
	std::cout << "New Points first XVal: " << _newPoints[0].x << " , Y: " << _newPoints[0].y << " WeightVal: " << _newPoints[0].z << std::endl;
	


	cout << "--------------------------end of program--------------------------" << endl;
	
}
