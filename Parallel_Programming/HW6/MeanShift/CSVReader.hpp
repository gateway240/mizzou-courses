#include <string>
#include <map>
#include <vector>
#include "cuda_runtime.h"

using namespace std;
/*
* A class to read data from a csv file.
*/
class CSVReader
{
	string fileName;
	char delimeter;

public:
	CSVReader(string filename, char delm = ':') : fileName(filename), delimeter(delm)
	{
	}
	// Function to fetch data from a CSV File
	vector<float3> getData( int colFilter, double rowThreshold);
	int linesParsed;
	
};