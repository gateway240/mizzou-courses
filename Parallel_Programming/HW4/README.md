See PDF for full details.

=======================================================
YOU MUST SPECIFY HOW TO BUILD YOUR CODE IN THIS SECTION
-------------------------------------------------------
1. run `module load openmpi`
2. run `module load boost`
3. navigate to the hw4 folder
4. change to the `src` directory (`cd src/`)
5. run `make`
6. execute the program with the following syntax:
variables in brackets are command line arguments specified in PDF
`srun -n [P] ./hw4 [F] [L] [T] [R]`
example:
`srun -n 2 ./hw4 /data/scottgs/springfield_road_scan.csv 7 .95 150`

execute tests:
`sbatch testing.sh`



=======================================================

Data File on TC:

/data/scottgs/springfield_road_scan.csv



For submission, you must have only source code and headers and build sytem support files.

Depending on your build system:
Use make clean or make distclean or remove an out of source build folder.

Test your submission by doing a fresh clone from your master into a temp folder and ensure it builds / runs.
e.g., 
git clone <addr_to_student_work> temp_folder_name


