#! /bin/bash

#SBATCH -p hpc3  # use the Lewis partition
#SBATCH -J hpc_hw4_debug  # give the job a custom name
#SBATCH -o results-%j.out  # give the job output a custom name
#SBATCH -t 0-00:10  # 10 minute time limit

#SBATCH --ntasks=2  # number of cores (AKA tasks)
#SBATCH --cpus-per-task=1 # number of cpus per task



# Commands here run only on the first core
echo "$(hostname), reporting for duty."
mkdir output
mkdir debug

# Commands with srun will run on all cores in the allocation
srun -n 2 ./hw4 /data/scottgs/springfield_road_scan.csv 7 .97 150 > ./debug/testOut-p1-t97.txt



