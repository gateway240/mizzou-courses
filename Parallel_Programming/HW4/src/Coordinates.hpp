#ifndef COORDINATES_H_
#define COORDINATES_H_

#include <vector>
using namespace std;
typedef struct Coordinates
{
	double x;
	double y;
	double weight;
} Coordinates;
class CoordinateFun
{
public:
	static vector<Coordinates> findCoLocatedPoints(vector<double> &countVec, Coordinates *keys, unsigned size, const int radius);
	static double haversine(double lat1d, double lon1d, double lat2d, double lon2d);
	static Coordinates calculateWeightedMean(const Coordinates *keys, unsigned size, const Coordinates iKey, const int radius);
}; // namespace coordinates
#endif
