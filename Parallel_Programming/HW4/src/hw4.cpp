#include <iostream>
#include <algorithm>
#include <chrono>
#include <time.h>
#include <mpi.h>

#include "CSVReader.hpp"
#include "Coordinates.hpp"

#define CONVERGANCE_DIST 1

#define GJS_DEBUG_PRINT 0

#define MASTER 0 /* task ID of master task */

using namespace std;

namespace abeattie
{

// const int RECIEVE_BUFFER_SIZE = 1000;

enum MPI_TAGS
{
    TERMINATE = 0,
    PROCESS = 1,
    DATA_SIZE = 2,
    POINTS = 3,
    DISTANCE = 4,
    INDEX = 5
};
// Function Prototypes

double meanShift(Coordinates *_oldPoints, Coordinates *_newPoints, int dataSize); // coordinating function
void calcPoints(int _radius);
void mergeCalcPoint(Coordinates *_newPoints, Coordinates *_localResult, int startIndex, int bytesToCopy); // worker function

}; // namespace abeattie
int main(int argc, char *argv[])
{

    int rank,     /* task ID - also used as seed number */
        numtasks, /* number of tasks */
        rc;       /* return code */

    /* Obtain number of tasks and task ID */
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    printf("MPI task %d has started...\n", rank);
    auto programStart = chrono::system_clock::now();
    double radius = stof(argv[4]);
#if GJS_DEBUG_PRINT
    cout << "Radius: " << radius << endl;
#endif
    if (rank == MASTER)
    {
        // ++++++++++++++++++++++++++++++
        // Master process
        // ++++++++++++++++++++++++++++++
        if (argc != 5)
        {
            cout << "Invalid number of arguments entered : " << argc << endl;
            for (int rank = 1; rank < numtasks; ++rank)
            {
                int terminate = -1;
                MPI_Send(&terminate,          /* message buffer */
                         1,                   /* buffer size */
                         MPI_INT,             /* data item is an integer */
                         rank,                /* destination process rank */
                         abeattie::DATA_SIZE, /* user chosen message tag */
                         MPI_COMM_WORLD);     /* default communicator */
            }
            return 1;
        }

        string fileStr = argv[1];
        int column = atoi(argv[2]);
        double threshold = stof(argv[3]);

        cout << "--------------------------begenning of program--------------------------" << endl;
        cout << "Input Parameters: " << endl;
        cout << "Threshold: " << threshold << endl;
        cout << "Column: " << column << endl;
        cout << "File: " << fileStr << endl;

        // Creating an object of CSVWriter
        CSVReader reader(fileStr);
        // Get the data from CSV File
        auto parseStart = chrono::system_clock::now();
        vector<Coordinates> dataVector = reader.getData(column, threshold);
        auto parseEnd = chrono::system_clock::now();
        chrono::duration<double> parseElapsedTime = parseEnd - parseStart;

        cout << "Lines in File : " << reader.linesParsed << endl;
        cout << "Records Loaded : " << dataVector.size() << endl;
        cout << "Time to Load File : " << parseElapsedTime.count() << " s " << endl;

        const unsigned int dataSize = dataVector.size();

        cout << " dataVectorSize: " << dataVector.size() << endl;

        Coordinates *_oldPoints;
        Coordinates *_newPoints;
        std::cout << "Setting up dataset" << std::endl;
        _oldPoints = new Coordinates[dataSize];

        std::copy(dataVector.begin(), dataVector.end(), _oldPoints);

        unsigned iteration = 0;
        double grandTotalDistance = 0;

        bool convergance = true;
        do
        {
            _newPoints = new Coordinates[dataSize];
            auto weightedMeanStart = chrono::system_clock::now();
            convergance = true;
            double itrDistance = abeattie::meanShift(_oldPoints, _newPoints, dataSize);

// Barrier to get all the desired output together
// Warning, if threads do not call Barrier, then
// the MPI_Barrier busy waits

// MPI_Barrier(MPI_COMM_WORLD);
#if GJS_DEBUG_PRINT
            cout << "Total Distance Moved during Iteration : " << itrDistance << " m" << endl;
#endif
            grandTotalDistance += itrDistance;
            if (itrDistance > CONVERGANCE_DIST)
            {
#if GJS_DEBUG_PRINT
                cout << "process did not converge yet" << endl;
#endif
                convergance = false;
            }

            auto weightedMeanEnd = chrono::system_clock::now();
            chrono::duration<double> weightedMeanElapsedTime = weightedMeanEnd - weightedMeanStart;
            iteration++;
            cout << "Compute Time for Weighted Mean Iteration " << iteration << " is: " << (weightedMeanElapsedTime.count()) << " s " << endl;
            //prep next iteration
            _oldPoints = _newPoints;
            // for (int i = 0; i < dataSize; i++)
            // {
            //     Coordinates coordinate = _oldPoints[i];
            //     std::cout << "Resultant X: " << coordinate.x << " , Y: " << coordinate.y << std::endl;
            // }
        } while (!convergance);

        // Tell all the workers to exit by sending an empty message with the
        // TERMINATE tag
        for (int rank = 1; rank < numtasks; ++rank)
        {
            int terminate = -1;
            MPI_Send(&terminate,          /* message buffer */
                     1,                   /* buffer size */
                     MPI_INT,             /* data item is an integer */
                     rank,                /* destination process rank */
                     abeattie::DATA_SIZE, /* user chosen message tag */
                     MPI_COMM_WORLD);     /* default communicator */
        }

        cout << "Total Number of Iterations: " << iteration << endl;
        vector<double> count;
        auto colocatedStart = chrono::system_clock::now();

        vector<Coordinates> coLocatedKeys = CoordinateFun::findCoLocatedPoints(count, _oldPoints, dataSize, CONVERGANCE_DIST * 2);
        auto colocatedEnd = chrono::system_clock::now();
        chrono::duration<double> colocatedElapsedTime = colocatedEnd - colocatedStart;
        cout << "Compute Time for Colocated Points is: " << (colocatedElapsedTime.count()) << " s " << endl;
        // unsigned int i = 0;
        // unsigned int totalCount = 0;
        cout << "Colocated points: " << endl;
        // for (const Coordinates data : coLocatedKeys)
        // {
        //     cout << std::setprecision(15) << "Point X: " << data.x << " Y: " << data.y << endl;
        //     cout << "Count : " << count[i] << endl;
        //     totalCount += count[i];
        //     i++;
        // }
        cout << "Total Distance Moved during Algorithm : " << grandTotalDistance << " m" << endl;
        cout << "Total Clusters: " << (int)coLocatedKeys.size() << endl;
        // cout << "Total Count: " << totalCount << endl;
        cout << "Done processing" << endl;

        cout << endl;
        auto programEnd = chrono::system_clock::now();
        chrono::duration<double> programElapsedTime = programEnd - programStart;
        cout << "Program runtime is: " << (programElapsedTime.count()) << " s " << endl;
        cout << "--------------------------end of program--------------------------" << endl;
    }
    else
    {
        // ++++++++++++++++++++++++++++++
        // Workers
        // ++++++++++++++++++++++++++++++
        abeattie::calcPoints(radius);
    }
    // Shut down MPI
    MPI_Finalize();

    return 0;

} /* ----------  end of function main  ---------- */

double abeattie::meanShift(Coordinates *_oldPoints, Coordinates *_newPoints, int dataSize)
{
    // =========================
    // Master (thread 0)
    // =========================
    int sendSize = dataSize * 3;
    int threadCount;
    MPI_Comm_size(MPI_COMM_WORLD, &threadCount);
#if GJS_DEBUG_PRINT
    std::cout << "abeattie::meanShift, " << (threadCount - 1) << " workers are available to process " << dataSize << " points" << std::endl;
#endif
    int numWorkers = threadCount - 1;
    unsigned workerNodeBlock = dataSize / numWorkers; //size of block to use for each worker

    double totalDistance = 0;
    auto weightedMeanStart = chrono::system_clock::now();
    // Start with 1, because the master is =0

    for (int rank = 1; rank < threadCount; ++rank)
    {
        MPI_Send(&dataSize,           /* message buffer */
                 1,                   /* buffer size */
                 MPI_INT,             /* data item is an integer */
                 rank,                /* destination process rank */
                 abeattie::DATA_SIZE, /* user chosen message tag */
                 MPI_COMM_WORLD);     /* default communicator */
        MPI_Send(_oldPoints,          /* message buffer */
                 sendSize,            /* buffer size */
                 MPI_DOUBLE,          /* data item is an integer */
                 rank,                /* destination process rank */
                 abeattie::POINTS,    /* user chosen message tag */
                 MPI_COMM_WORLD);     /* default communicator */
    }

    // receive all the outstanding results from the workers
    for (int rank = 1; rank < threadCount; ++rank)
    {

        MPI_Status status;

        int blockSize;
        // Receive a message from the master
        MPI_Recv(&blockSize, /* message buffer */
                 1,          /* buffer size */
                 MPI_INT,    /* data item is an integer */
                 rank,       /* Receive from worker */
                 abeattie::DATA_SIZE,
                 MPI_COMM_WORLD, /* default communicator */
                 &status);
        Coordinates *coordinateBlock = (Coordinates *)malloc(sizeof(Coordinates) * blockSize); //local storage for process to compute it's block of data
        // Receive a message from the worker
        MPI_Recv(coordinateBlock, /* message buffer */
                 blockSize * 3,   /* buffer size */
                 MPI_DOUBLE,      /* data item is an integer */
                 rank,            /* Receive from worker */
                 abeattie::POINTS,
                 MPI_COMM_WORLD, /* default communicator */
                 &status);
        double localDistance;
        // Receive a message from the master
        MPI_Recv(&localDistance, /* message buffer */
                 1,              /* buffer size */
                 MPI_DOUBLE,     /* data item is an integer */
                 rank,           /* Receive from worker */
                 abeattie::DISTANCE,
                 MPI_COMM_WORLD, /* default communicator */
                 &status);
        const int incomingTag = status.MPI_TAG;
        const int sourceCaught = status.MPI_SOURCE;
        int start = (rank - 1) * workerNodeBlock;
#if GJS_DEBUG_PRINT
        std::cout << "start: " << start << " Block size: " << blockSize << std::endl;
        std::cout << "local distance: " << localDistance << std::endl;
#endif

#if GJS_DEBUG_PRINT
        std::cout << "abeattie::MeanShift, " << numWorkers << ", recieved " << std::endl;
#endif

        abeattie::mergeCalcPoint(_newPoints, coordinateBlock, start, blockSize);
        free(coordinateBlock);
        totalDistance += localDistance;
    }

    std::cout << "abeattie::MeanShift distance: " << totalDistance << std::endl;
#if GJS_DEBUG_PRINT
    std::cout << "abeattie::MeanShift, completed processing iteration" << std::endl;
#endif

    return totalDistance;
}
void abeattie::calcPoints(int _radius)
{
    // =========================
    // worker function
    // =========================
    int rank;
    int threadCount;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &threadCount);
    std::cout << "abeattie::calcPoints[" << rank << "], waiting for work" << std::endl;
    int numWorkers = threadCount - 1;

    MPI_Status status;

    while (1)
    {

        int msgSize;
        // Receive a message from the master
        MPI_Recv(&msgSize, /* message buffer */
                 1,        /* buffer size */
                 MPI_INT,  /* data item is an integer */
                 0,        /* Receive from master */
                 abeattie::DATA_SIZE,
                 MPI_COMM_WORLD, /* default communicator */
                 &status);
#if GJS_DEBUG_PRINT
        cout << "Msg Size: " << msgSize << endl;
#endif
        if (msgSize == -1)
        {
            std::cout << "abeattie::calcPoints[" << rank << "], recieved terminate signal" << std::endl;
            return;
        }

        // Receive a message from the master
        Coordinates *oldPoints = (Coordinates *)malloc(sizeof(Coordinates) * msgSize); //local storage for process to compute it's block of data
        MPI_Recv(oldPoints,                                                            /* message buffer */
                 msgSize * 3,                                                          /* buffer size */
                 MPI_DOUBLE,                                                           /* data item is an integer */
                 0,                                                                    /* Receive from master */
                 abeattie::POINTS,
                 MPI_COMM_WORLD, /* default communicator */
                 &status);

        // Check if we have been terminated by the master
        // exit from the worker loop

        const int mpiTag = status.MPI_TAG;

        const int dataSize = msgSize;

        unsigned workerNodeBlock = dataSize / numWorkers; //size of block to use for each worker
        unsigned remainder = dataSize % numWorkers;       // reaminder for last worker
#if GJS_DEBUG_PRINT
        cout << "WorkerNodeBlock: " << workerNodeBlock << " Remainder: " << remainder << endl;
#endif
        // Merge the results from this thread to the global results
        unsigned int start = (workerNodeBlock) * (rank - 1); // set wehere the process starts processing data
        unsigned int iterator = start + (workerNodeBlock);   //set where the process ends processing data (add reaminder if last rpocess)
        if (rank == numWorkers)
        {
            iterator += remainder;
        }
        int blockSize = sizeof(Coordinates) * (iterator - start);
        // Convert the message into a string for parse work
#if GJS_DEBUG_PRINT
        std::cout << "abeattie::calcPoints[" << rank << "], recieved " << std::endl;
#endif
        double localDistance = 0; // variable for total distance moved in the process

        Coordinates *coordinateBlock = (Coordinates *)malloc(sizeof(Coordinates) * blockSize); //local storage for process to compute it's block of data
        unsigned coordinateBlockItr = 0;
        for (unsigned i = start; i < iterator; i++)
        {

            const Coordinates currentPoint = oldPoints[i];

            Coordinates newPoint = CoordinateFun::calculateWeightedMean(oldPoints, dataSize, currentPoint, _radius);

            double distanceMoved = CoordinateFun::haversine(currentPoint.x, currentPoint.y, newPoint.x, newPoint.y);
            // if (i == start)
            // {
            //     std::cout << "Inside worker: " << rank - 1 << std::endl;
            //     std::cout << "Current X: " << currentPoint.x << " , Y: " << currentPoint.y << std::endl;
            //     std::cout << "New X: " << newPoint.x << " , Y: " << newPoint.y << std::endl;
            //     std::cout << "Distance Moved : " << distanceMoved << std::endl;
            // }

            if (distanceMoved > 0)
            {
                localDistance += distanceMoved;
            }
            //store computed values in local storage block
            coordinateBlock[coordinateBlockItr] = newPoint;
            coordinateBlockItr++;
        }
#if GJS_DEBUG_PRINT
        std::cout << "Local distance: " << localDistance << std::endl;
#endif
#if GJS_DEBUG_PRINT
        std::cout << "abeattie::calcPoints[" << rank << "], result sending " << std::endl;
#endif
        MPI_Send(&blockSize,          /* message buffer */
                 1,                   /* buffer size */
                 MPI_INT,             /* data item is an integer */
                 0,                   /* destination process rank */
                 abeattie::DATA_SIZE, /* user chosen message tag */
                 MPI_COMM_WORLD);     /* default communicator */
        MPI_Send(coordinateBlock,     /* message buffer */
                 blockSize * 3,       /* buffer size */
                 MPI_DOUBLE,          /* data item is an integer */
                 0,                   /* destination process rank, the master */
                 abeattie::POINTS,    /* user chosen message tag */
                 MPI_COMM_WORLD);     /* default communicator */
        MPI_Send(&localDistance,      /* message buffer */
                 1,                   /* buffer size */
                 MPI_DOUBLE,          /* data item is an integer */
                 0,                   /* destination process rank, the master */
                 abeattie::DISTANCE,  /* user chosen message tag */
                 MPI_COMM_WORLD);     /* default communicator */

        free(coordinateBlock);
        free(oldPoints);
        // MPI_Barrier(MPI_COMM_WORLD); // this is linked to the above barrier
    } // end of an infinite loop
    return;
}
void abeattie::mergeCalcPoint(Coordinates *_newPoints, Coordinates *_localResult, int startIndex, int bytesToCopy)
{
    memcpy(&_newPoints[startIndex], _localResult, bytesToCopy);
}
