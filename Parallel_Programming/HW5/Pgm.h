#ifndef PGM_H
#define PGM_H

#include <iostream>
#include <fstream>
#include <stdint.h>
#include "common/inc/helper_image.h"

class PGM {
public:
	PGM() : image(NULL) {}

	PGM(int w, int h) {
		image = new unsigned char[(w * h)];
		info.width = w;
		info.height = h;
	}

	bool Save(const char* filename) {
		if (image == NULL) {
			std::cerr << "Image unitialized" << std::endl;
			return false;
		}

		if (sdkSavePGM<unsigned char>(filename, (unsigned char*) image, (unsigned int) Width(), (unsigned int) Height()) != true)
		{
			printf("Failed to load PGM image file: %s\n", filename);
			exit(EXIT_FAILURE);
		}


		return true;
	}

	bool Load(const char* filename) {
		if (image != NULL) {
			delete[] image;
		}


		unsigned int w, h;
		size_t file_length = strlen(filename);

		if (!strcmp(&filename[file_length - 3], "pgm"))
		{
			if (sdkLoadPGM<unsigned char>(filename, &image, &w, &h) != true)
			{
				printf("Failed to load PGM image file: %s\n", filename);
				exit(EXIT_FAILURE);
			}
		}
		else if (!strcmp(&filename[file_length - 3], "ppm"))
		{
			if (sdkLoadPPM4(filename, &image, &w, &h) != true)
			{
				printf("Failed to load PPM image file: %s\n", filename);
				exit(EXIT_FAILURE);
			}
		}
		else
		{
			exit(EXIT_FAILURE);
		}

		info.width = (int)w;
		info.height = (int)h;



		return true;
	}

	~PGM() {
		if (image != NULL) {
			delete[] image;
		}
	}

	int Width() {
		return info.width;
	}

	int Height() {
		return info.height;
	}

	char GetPixel(int x, int y) {
		return image[y * info.width + x];
	}

	void SetPixel(int x, int y, char color) {
		image[y * info.width + x] = color;
	}

	unsigned char* image;

private:

	struct pgmInfo {
		int32_t width;
		int32_t height;
	};

	pgmInfo info;

};

#endif
