#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <iostream>
#include "MedianFilter.h"
#include <time.h>
#include <thrust/sort.h>
#include <thrust/execution_policy.h>
#define TILE_SIZE 4


__global__ void medianFilterKernel(unsigned char* inputImageKernel, unsigned char* outputImagekernel, int imageWidth, int imageHeight, int winSize)
{
	// Set row and colum for thread.
	int row = blockIdx.y * blockDim.y + threadIdx.y;
	int col = blockIdx.x * blockDim.x + threadIdx.x;
	const int cumWinSize = winSize * winSize;
	int winOffset = (winSize - 1) / 2; //Set Offset for window
	unsigned char* filterVector = new unsigned char[cumWinSize]();   //Take fiter window
	

	if ((row < winOffset) || (col < winOffset) || (row > imageHeight - winOffset) || (col > imageWidth - winOffset))
		outputImagekernel[row * imageWidth + col] = 0; //Deal with boundry conditions
	else {
		for (int x = 0; x < winSize; x++) {
			for (int y = 0; y < winSize; y++) {
				filterVector[x * winSize + y] = inputImageKernel[(row + x - winOffset) * imageWidth + (col + y - winOffset)];   // setup the filtering window.
			}
		}
		int i, j, min_idx;
		////Selection sort
		//for (i = 0; i < cumWinSize - 1; i++) {
		//	min_idx = i;
		//	for (j = i + 1; j < cumWinSize; j++) {
		//		if (filterVector[j] < filterVector[min_idx]) {
		//			min_idx = j;
		//		}
		//		//Swap the variables.
		//		char tmp = filterVector[min_idx];
		//		filterVector[min_idx] = filterVector[i];
		//		filterVector[i] = tmp;

		//	}
		//}
		thrust::sort(thrust::seq, filterVector, filterVector + cumWinSize);
		//Bubble sort
		//for (int i = 0; i < cumWinSize; i++) {
		//	for (int j = i + 1; j < cumWinSize; j++) {
		//		if (filterVector[i] > filterVector[j]) {
		//			//Swap the variables.
		//			char tmp = filterVector[i];
		//			filterVector[i] = filterVector[j];
		//			filterVector[j] = tmp;
		//		}
		//	}
		//}
		outputImagekernel[row * imageWidth + col] = filterVector[4];   //Set the output variables.
	}
	delete[] filterVector;
}

bool MedianFilterGPU(PGM* image, PGM* outputImage, int winSize) {
	//Cuda error and image values.
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEvent_t startCopyIn, stopCopyIn;
	cudaEventCreate(&startCopyIn);
	cudaEventCreate(&stopCopyIn);
	cudaEvent_t startKernel, stopKernel;
	cudaEventCreate(&startKernel);
	cudaEventCreate(&stopKernel);
	cudaEvent_t startCopyOut, stopCopyOut;
	cudaEventCreate(&startCopyOut);
	cudaEventCreate(&stopCopyOut);
	cudaEventRecord(start);
	cudaError_t status;
	int width = image->Width();
	int height = image->Height();

	int size = width * height * sizeof(char);
	//initialize images.
	unsigned char* deviceinputimage;
	cudaMalloc((void**)&deviceinputimage, size);
	status = cudaGetLastError();
	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMalloc : " << cudaGetErrorString(status) <<
			std::endl;
		return false;
	}
	cudaEventRecord(startCopyIn);
	cudaMemcpy(deviceinputimage, image->image, size, cudaMemcpyHostToDevice);
	cudaEventRecord(stopCopyIn);
	cudaEventSynchronize(stopCopyIn);
	float timeCopyIn = 0;
	cudaEventElapsedTime(&timeCopyIn, startCopyIn, stopCopyIn);
	printf("Copy In time %f ms\n", timeCopyIn);
	status = cudaGetLastError();
	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMemcpy cudaMemcpyHostToDevice: " << cudaGetErrorString(status) <<
			std::endl;
		cudaFree(deviceinputimage);
		return false;
	}
	unsigned char* deviceOutputImage;
	cudaMalloc((void**)&deviceOutputImage, size);
	//take block and grids.
	dim3 dimBlock(TILE_SIZE, TILE_SIZE);
	dim3 dimGrid((int)ceil((float)image->Width() / (float)TILE_SIZE),
		(int)ceil((float)image->Height() / (float)TILE_SIZE));

	std::cout << "WinSize: " << winSize << std::endl;
	// call the kernel
	cudaEventRecord(startKernel);
	medianFilterKernel <<<dimGrid, dimBlock>>> (deviceinputimage, deviceOutputImage, width, height, winSize);
	cudaEventRecord(stopKernel);
	cudaEventSynchronize(stopKernel);
	float timeKernel = 0;
	cudaEventElapsedTime(&timeKernel, startKernel, stopKernel);
	printf("Kenerl time %f ms\n", timeKernel);
	
	// save output image to host.
	cudaEventRecord(startCopyOut);
	cudaMemcpy(outputImage->image, deviceOutputImage, size, cudaMemcpyDeviceToHost);
	cudaEventRecord(stopCopyOut);
	cudaEventSynchronize(stopCopyOut);
	float timeCopyOut = 0;
	cudaEventElapsedTime(&timeCopyOut, startCopyOut, stopCopyOut);
	printf("Copy Out time %f ms\n", timeCopyOut);
	status = cudaGetLastError();

	if (status != cudaSuccess) {
		std::cout << "Kernel failed for cudaMemcpy cudaMemcpyDeviceToHost: " << cudaGetErrorString(status) <<
			std::endl;
		cudaFree(deviceinputimage);
		cudaFree(deviceOutputImage);
		return false;
	}
	//Free the memory
	cudaFree(deviceinputimage);
	cudaFree(deviceOutputImage);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float time = 0;
	cudaEventElapsedTime(&time, start, stop);
	printf("time %f ms\n", time);
	return true;
}
