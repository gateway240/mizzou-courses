#ifndef MEDIAN_FILTER_H
#define MEDIAN_FILTER_H

#include "Pgm.h"



//    //CPU Median Filtering
    void MedianFilterCPU( PGM* image, PGM* outputImage, int winSize );

//    //GPU Median Filtering
    bool MedianFilterGPU( PGM* image, PGM* outputImage, int winSize );
//
    #endif
