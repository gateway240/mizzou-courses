#include<iostream>
#include "MedianFilter.h"
#include "Bitmap.h"
#include "Pgm.h"
#include <ctime>


#define ITERATIONS ( 1 )

int ComparePGMs( PGM* inputA, PGM* inputB ){
	int differentpixels = 0; //Initializing the diffrerce Variable.
	if((inputA->Height() != inputB->Height()) || (inputA->Width() != inputB->Width())) // Check the condition for height and width matching.
		return -1;
	for(int height=1; height<inputA->Height()-1; height++){
		for(int width=1; width<inputA->Width()-1; width++){
			if (inputA->GetPixel(width, height) != inputB->GetPixel(width, height))
				differentpixels++; // increment the differences.
		}
	}
	return differentpixels;
}

void MedianFilterCPU( PGM* image, PGM* outputImage, int winSize )
{
	unsigned char* filterVector = new unsigned char[(winSize * winSize)]();   //Take fiter window
	int winOffset = (winSize - 1) / 2; //Set Offset for window
	for(int row=0; row<image->Height(); row++){
		for(int col=0; col<image->Width(); col++){
			if((row < winOffset) || (col < winOffset) || (row > image->Height()-winOffset) || (col > image->Width()- winOffset)) //Check the boundry condition.
				outputImage->SetPixel(col, row, 0);
			else {
				for (int x = 0; x < winSize; x++) {
					for (int y = 0; y < winSize; y++){
						filterVector[x*winSize+y] = image->GetPixel( (col+y-winOffset),(row+x-winOffset)); //Fill the Filter Vector
					}
				}

				for (int i = 0; i < winSize * winSize; i++) {
					for (int j = i + 1; j < winSize * winSize; j++) {
						//Bubble sort
						if (filterVector[i] > filterVector[j]) {
							//Swap the variables.
							char tmp = filterVector[i];
							filterVector[i] = filterVector[j];
							filterVector[j] = tmp;
						}
					}
				}
				outputImage->SetPixel(col, row, filterVector[4]); //Finally assign value to output pixels
			}
		}
	}
	delete[] filterVector;
}

int main(int argc, char* argv[])
{

	if (argc != 4) {
		std::cout << "Invalid number of arguments entered : " << argc << std::endl;
	}

	const char* inputImg = argv[1];
	const char* outputImg = argv[2];
	const int window_size = atoi(argv[3]);


	PGM* originalImage = new PGM();
	PGM* resultImageCPU = new PGM();
	PGM* resultImageGPU = new PGM();


	float tcpu, tLoadFile; //timing variables.
	clock_t start, end;
	clock_t startFile, endFile;
	
	std::cout << "Loading image" << std::endl;
	startFile = clock();
	originalImage->Load(inputImg);
	endFile = clock();
	tLoadFile = ((float)(endFile - startFile) + 1) * 1000 / (float)CLOCKS_PER_SEC / ITERATIONS;
	std::cout << "Load File Time " << tLoadFile << "ms" << std::endl;
	resultImageCPU->Load(inputImg);
	resultImageGPU->Load(inputImg);
	std::cout << "Operating on a " << originalImage->Width() << " x " << originalImage->Height() << " image..." << std::endl;
	start = clock(); //Stat the clock
	for (int i = 0; i < ITERATIONS; i++)
	{
		MedianFilterCPU(originalImage, resultImageCPU, window_size);
	}
	end = clock();
	std::cout << start << " --- " << end << std::endl;
	tcpu = ((float)(end-start) + 1) * 1000 / (float)CLOCKS_PER_SEC/ITERATIONS;
	std::cout<< "cputime " << tcpu<<"ms"<<std::endl;
	
	for (int i = 0; i < ITERATIONS; i++)
	{
		MedianFilterGPU(originalImage, resultImageGPU, window_size);
	}

	float differences = ComparePGMs(resultImageCPU, resultImageGPU);
	float percentDif = (differences / (float)(resultImageCPU->Height() * resultImageCPU->Width()));
	std::cout << "Percentage difference: " << std::fixed << percentDif << std::endl;
	resultImageCPU->Save("output/Lenna_cpu.pgm");
	resultImageGPU->Save(outputImg);
}
