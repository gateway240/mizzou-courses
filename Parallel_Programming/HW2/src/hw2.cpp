#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <deque>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <unistd.h>
#include <cstring>

#include "CSVReader.hpp"

#define CONVERGANCE_DIST 1

using namespace std;
// This function converts decimal degrees to radians
double deg2rad(double deg)
{
    return (deg * M_PI / 180);
}

//  This function converts radians to decimal degrees
double rad2deg(double rad)
{
    return (rad * 180 / M_PI);
}

/**
 * Returns the distance between two points on the Earth.
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in meters
 */
double haversine(double lat1d, double lon1d, double lat2d, double lon2d)
{
    double lat1r, lon1r, lat2r, lon2r, u, v;
    lat1r = deg2rad(lat1d);
    lon1r = deg2rad(lon1d);
    lat2r = deg2rad(lat2d);
    lon2r = deg2rad(lon2d);

    double dlon = lon2r - lon1r;
    double dlat = lat2r - lat1r;
    double a = pow(sin(dlat / 2), 2) + cos(lat1r) * cos(lat2r) * pow(sin(dlon / 2), 2);
    double c = 2 * asin(sqrt(a));
    double r = 6371008.8; // Mean Radius of earth in meters
    return r * c;
}

static vector<Coordinates> findCoLocatedPoints(vector<double> &countVec, Coordinates *keys, unsigned size, const int radius)
{
    deque<Coordinates> q(deque<Coordinates>(keys, keys + size)); //create a deque with the keys obtained from clustering

    cout << "q (size): " << q.size() << endl;
    vector<Coordinates> coLocatedKeys;
    unsigned iterationCount = 0;
    while (!q.empty())
    {
        Coordinates iKey = q.front();
        q.pop_front();
        unsigned count = 1;
        for (int i = 0; i < q.size(); i++)
        {

            const Coordinates jKey = q[i];

            const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

            if (distance <= radius) // if a q is in the radius increment the count and remove it from the dequeue
            {
                count += 1;
                iterationCount++;
                q.erase(q.begin() + i);
            }
        }
        coLocatedKeys.push_back(iKey); // store the key
        countVec.push_back(count); // store the count
    }

    return coLocatedKeys;
}
static Coordinates calculateWeightedMean(const vector<double> weights, const Coordinates *keys, unsigned size, const Coordinates iKey, const int radius)
{

    double xVal = 0;
    double yVal = 0;
    double weightVal = 0;

    //calculate weighted mean based on points in neighborhood
    for (unsigned j = 0; j < size; ++j)
    {

        const Coordinates jKey = keys[j];

        const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

        const double weight = weights[j];
   
        if (distance <= radius)
        {
            xVal += jKey.x * weight;
            yVal += jKey.y * weight;
            weightVal += weight;
        }
    }

    return (Coordinates){xVal / weightVal, yVal / weightVal};

}

int main(int argc, char *argv[])
{

    if (argc != 6)
    {
        cout << "Invalid number of arguments entered : " << argc << endl;
        return 1;
    }
    auto programStart = chrono::system_clock::now();
    unsigned numProc = atoi(argv[1]);
    string fileStr = argv[2];
    unsigned column = atoi(argv[3]);
    double threshold = stof(argv[4]);
    double radius = stof(argv[5]);

    cout << "--------------------------begenning of program--------------------------" << endl;
    cout << "Input Parameters: " << endl;
    cout << "NumProc: " << numProc << endl;
    cout << "Threshold: " << threshold << endl;
    cout << "Column: " << column << endl;
    cout << "Radius: " << radius << endl;
    cout << "File: " << fileStr << endl;

    pid_t pid; // process identifier, pid_t is a process id type defined in sys/types

    // =============================
    // BEGIN: Do the Semaphore Setup
    // The semaphore will be a mutex
    // =============================
    int semId;                      // ID of semaphore set
    key_t semKey = 598201;          // key to pass to semget(), key_t is an IPC key type defined in sys/types
    int semFlag = IPC_CREAT | 0666; // Flag to create with rw permissions

    int semCount = 1; // number of semaphores to pass to semget()
    int numOps = 1;   // number of operations to do

    // Create the semaphore
    // The return value is the semaphore set identifier
    // The flag is a or'd values of CREATE and ugo permission of RW,
    //			just like used for creating a file
    if ((semId = semget(semKey, semCount, semFlag)) == -1)
    {
        std::cerr << "Failed to semget(" << semKey << "," << semCount << "," << semFlag << ")" << std::endl;
        exit(1);
    }
    else
    {
        std::cout << "Successful semget resulted in (" << semId << ")" << std::endl;
    }

    // Initialize the semaphore
    union semun {
        int val;
        struct semid_ds *buf;
        ushort *array;
    } argument;

    argument.val = 1; // NOTE: We are setting this to one to make it a MUTEX
    if (semctl(semId, 0, SETVAL, argument) < 0)
    {
        std::cerr << "Init: Failed to initialize (" << semId << ")" << std::endl;
        exit(1);
    }
    else
    {
        std::cout << "Init: Initialized (" << semId << ")" << std::endl;
    }

    // =============================
    // END: Do the Semaphore Setup
    // =============================

    // Creating an object of CSVWriter
    CSVReader reader(fileStr);
    // Get the data from CSV File
    auto parseStart = chrono::system_clock::now();
    vector<double> weights;
    vector<Coordinates> dataVector = reader.getData(weights, column, threshold);
    auto parseEnd = chrono::system_clock::now();
    chrono::duration<double> parseElapsedTime = parseEnd - parseStart;

    cout << "Lines in File : " << reader.linesParsed << endl;
    cout << "Records Loaded : " << dataVector.size() << endl;
    cout << "Time to Load File : " << parseElapsedTime.count() << " s " << endl;

    const unsigned int dataSize = dataVector.size();

    cout << "Weights size: " << weights.size() << " dataVectorSize: " << dataVector.size() << endl;

    // =========================================
    // BEGIN: Do the Shared Memory Segment Setup
    //
    // =========================================

    int shmId_old = 354123;         // ID of shared memory segment
    key_t shmKey_old;               // key to pass to shmget(), key_t is an IPC key type defined in sys/types
    int shmFlag = IPC_CREAT | 0666; // Flag to create with rw permissions

    int shmId_new = 732462; // ID of shared memory segment
    key_t shmKey_new;       // key to pass to shmget(), key_t is an IPC key type defined in sys/types

    int shmId_totalDistance = 845627; // ID of shared memory segment
    key_t shmKey_totalDistance;       // key to pass to shmget(), key_t is an IPC key type defined in sys/types

    // This will be shared:
    Coordinates *oldPoints;
    Coordinates *newPoints;
    double *totalDistance;

    //Get shared memory id's
    if ((shmId_old = shmget(shmKey_old, (dataSize) * sizeof(Coordinates), shmFlag)) < 0)
    {
        std::cerr << "Init: Failed to initialize shared memory (oldPoints) (" << shmId_old << ")" << std::endl;
        cerr << strerror(errno) << endl;
        exit(1);
    }
    cout << "shmId_old: " << shmId_old << endl;
    if ((shmId_new = shmget(shmKey_new, (dataSize) * sizeof(Coordinates), shmFlag)) < 0)
    {
        // cout << "shmId_new: " << shmId_old << endl;
        std::cerr << "Init: Failed to initialize shared memory (newPoints) (" << shmId_new << ")" << std::endl;
        cerr << strerror(errno) << endl;
        exit(1);
    }
    if ((shmId_totalDistance = shmget(shmKey_totalDistance, sizeof(double), shmFlag)) < 0)
    {
        // cout << "shmId_new: " << shmId_old << endl;
        std::cerr << "Init: Failed to initialize shared memory (totalDistance) (" << shmId_totalDistance << ")" << std::endl;
        cerr << strerror(errno) << endl;
        exit(1);
    }

    //Attach shared memory

    if ((oldPoints = (Coordinates *)shmat(shmId_old, NULL, 0)) == (Coordinates *)-1)
    {
        std::cerr << "Init: Failed to attach shared memory (" << shmId_old << ")" << std::endl;
        exit(1);
    }
    if ((newPoints = (Coordinates *)shmat(shmId_new, NULL, 0)) == (Coordinates *)-1)
    {
        std::cerr << "Init: Failed to attach shared memory (" << shmId_new << ")" << std::endl;
        exit(1);
    }
    if ((totalDistance = (double *)shmat(shmId_totalDistance, NULL, 0)) == (double *)-1)
    {
        std::cerr << "Init: Failed to attach shared memory (" << shmId_totalDistance << ")" << std::endl;
        exit(1);
    }
    std::copy(dataVector.begin(), dataVector.end(), oldPoints);

    // =========================================
    // END: Do the Shared Memory Segment Setup
    // =========================================
    pid_t pids[numProc];

    unsigned iteration = 0;
    double grandTotalDistance = 0;

    unsigned workerNodeBlock = dataSize / numProc; //size of block to use for each worker
    unsigned remainder = dataSize % numProc; // reaminder for last worker
    cout << "WorkerNodeBlock: " << workerNodeBlock << " Remainder: " << remainder << endl;
    bool convergance = true;
    do
    {
        totalDistance[0] = 0;
        auto weightedMeanStart = chrono::system_clock::now();
        convergance = true;
        for (unsigned p = 0; p < numProc; ++p)
        {
            unsigned workerNum = p; // determine which worker we are in
            pids[p] = fork(); // store the pid of the process spawnded
            double localDistance = 0; // variable for total distance moved in the process
            if (pids[p] < 0)
            {
                std::cerr << "Could not fork!!! (" << pid << ")" << std::endl;
                exit(2);
            }
            if (pids[p] == 0) // Child process
            {
                std::cout << "In the child worker node: " << workerNum << std::endl;
                unsigned start = (workerNodeBlock)*p; // set wehere the process starts processing data
                unsigned iterator = start + (workerNodeBlock); //set where the process ends processing data (add reaminder if last rpocess)
                if (workerNum == numProc - 1)
                {
                    iterator += remainder;
                }
                unsigned blockSize = sizeof(Coordinates) * (iterator - start);
                Coordinates *coordinateBlock = (Coordinates *)malloc(blockSize); //local storage for process to compute it's block of data
                unsigned coordinateBlockItr = 0;
                for (unsigned i = start; i < iterator; i++)
                {

                    const Coordinates currentPoint = oldPoints[i];
                    Coordinates newPoint = calculateWeightedMean(weights, oldPoints, dataSize, currentPoint, radius);

                    double distanceMoved = haversine(currentPoint.x, currentPoint.y, newPoint.x, newPoint.y);
               
                    if (distanceMoved > 0)
                    {
                        localDistance += distanceMoved;
                    }
                    //store computed values in local storage block
                    coordinateBlock[coordinateBlockItr] = newPoint;
                    coordinateBlockItr++;
                }
                //Computed all the data now ready to write back to shared memory
                // Get the semaphore (P-op)
                struct sembuf operations[1];
                operations[0].sem_num = 0; // use the first(only, because of semCount above) semaphore
                operations[0].sem_flg = 0; // allow the calling process to block and wait

                // Set up the sembuf structure.
                operations[0].sem_op = -1; // this is the operation... the value is added to semaphore (a P-Op = -1)

                int retval = semop(semId, operations, numOps);
                bool ready = false;
                if (0 == retval)
                {
                    ready = true;
                    cout << "Ready to write" << endl;
                }
                else
                {
                    //If unable to get the semaphore to write try again a maximum of 100 tims
                    const unsigned MAX_RETRY = 100;
                    for (int i = 0; i < MAX_RETRY; ++i)
                    {
                        cout << "Not ready to write yet" << endl;
                        retval = semop(semId, operations, numOps);
                        if (retval == 0)
                        {
                            ready = true;
                            break;
                        }
                    }
                }
                cout << "writing to memory: " << start << endl;
                memcpy(&newPoints[start], coordinateBlock, blockSize);
                totalDistance[0] += localDistance;
                free(coordinateBlock);

                // Release the semaphore (V-op)
                operations[0].sem_op = 1; // this the operation... the value is added to semaphore (a V-Op = 1)

                std::cout << "In the child (if): about to release semaphore" << std::endl;
                retval = semop(semId, operations, numOps);
                if (0 == retval)
                {
                    std::cout << "In the child (if): Successful V-operation on (" << semId << ")" << std::endl;
                }
                else
                {
                    std::cerr << "In the child (if): Failed V-operation on (" << semId << ")" << std::endl;
                }

                _exit(0);
            }
        }

        int status;
        int procCount = numProc;

        cout << "waiting for children to finish" << endl;
        for (unsigned i = 0; i < numProc; ++i)
        {

            pid_t w = waitpid(pids[i], &status, 0);
            printf("Child with PID %ld exited with status 0x%x.\n", (long)pids[i], status);
            if (w == -1)
            {
                std::cerr << "Error waiting for child process (" << pid << ")" << std::endl;
                break;
            }

            if (WIFEXITED(status))
            {
                if (status > 0)
                {
                    std::cerr << "Child process (" << pid << ") exited with non-zero status of " << WEXITSTATUS(status) << std::endl;
                    continue;
                }
                else
                {
                    std::cout << "Child process (" << pid << ") exited with status of " << WEXITSTATUS(status) << std::endl;
                    continue;
                }
            }
            else if (WIFSIGNALED(status))
            {
                std::cout << "Child process (" << pid << ") killed by signal (" << WTERMSIG(status) << ")" << std::endl;
                continue;
            }
            else if (WIFSTOPPED(status))
            {
                std::cout << "Child process (" << pid << ") stopped by signal (" << WSTOPSIG(status) << ")" << std::endl;
                continue;
            }
            else if (WIFCONTINUED(status))
            {
                std::cout << "Child process (" << pid << ") continued" << std::endl;
                continue;
            }
        }
        cout << "Total Distance Moved during Iteration : " << totalDistance[0] << " m" << endl;
        grandTotalDistance += totalDistance[0];
        if (totalDistance[0] > CONVERGANCE_DIST)
        {
            cout << "process did not converge yet" << endl;
            convergance = false;
        }
        std::cout << "In the parent (if-else): " << std::endl;

        auto weightedMeanEnd = chrono::system_clock::now();
        chrono::duration<double> weightedMeanElapsedTime = weightedMeanEnd - weightedMeanStart;
        iteration++;
        cout << "Compute Time for Weighted Mean Iteration " << iteration << " is: " << (weightedMeanElapsedTime.count()) << " s " << endl;

        oldPoints = newPoints;
    } while (!convergance);

    cout << "Total Number of Iterations: " << iteration << endl;
    vector<double> count;
    auto colocatedStart = chrono::system_clock::now();

    vector<Coordinates> coLocatedKeys = findCoLocatedPoints(count, oldPoints, dataSize, CONVERGANCE_DIST * 2);
    auto colocatedEnd = chrono::system_clock::now();
    chrono::duration<double> colocatedElapsedTime = colocatedEnd - colocatedStart;
    cout << "Compute Time for Colocated Points is: " << (colocatedElapsedTime.count()) << " s " << endl;
    unsigned i = 0;
    unsigned totalCount = 0;
    cout << "Colocated points: " << endl;
    // for (const Coordinates data : coLocatedKeys)
    // {
    //     cout << std::setprecision(15) << "Point X: " << data.x << " Y: " << data.y << endl;
    //     cout << "Count : " << count[i] << endl;
    //     totalCount += count[i];
    //     i++;
    // }
    cout << "Total Distance Moved during Algorithm : " << grandTotalDistance << " m" << endl;
    cout << "Total Clusters: " << coLocatedKeys.size() << endl;
    cout << "Total Count: " << totalCount << endl;
    cout << "Done processing" << endl;

    cout << endl;

    auto programEnd = chrono::system_clock::now();
    chrono::duration<double> programElapsedTime = programEnd - programStart;
    cout << "Program runtime is: " << (programElapsedTime.count()) << " s " << endl;

    shmctl(shmId_old, IPC_RMID, NULL);
    shmctl(shmId_new, IPC_RMID, NULL);

    cout << "--------------------------end of program--------------------------" << endl;
    return 0;

} /* ----------  end of function main  ---------- */
