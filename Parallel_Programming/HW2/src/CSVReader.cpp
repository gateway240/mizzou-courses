#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <chrono>
#include <time.h>

#include "CSVReader.hpp"

using namespace std;

namespace functions
{
vector<string> split(const string &s, char delim)
{
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim))
	{
		tokens.push_back(item);
	}
	return tokens;
}

} // namespace functions

vector<Coordinates> CSVReader::getData(vector<double> &weights, int colFilter, double rowThreshold)
{
	ifstream file(fileName);
	vector<Coordinates> dataVector;
	string line = "";
	linesParsed = 0;
	// Iterate through each line and split the content using delimeter
	while (getline(file, line))
	{
		const vector<string> vec = functions::split(line, delimeter);
		if (stof(vec[colFilter + 2]) >= rowThreshold)
		{
			// cout  << setprecision(15) << stod(vec[1]) << endl;
			// pair<double, double> key(stof(vec[0]), stof(vec[1]));
			Coordinates coordinate = (Coordinates) {stod(vec[1]),stod(vec[0])};
			// vec.erase(vec.begin(), vec.begin() + 2);
			dataVector.push_back(coordinate);
			weights.push_back(stof(vec[colFilter + 2]));
			// keys.push_back(key);
			
		}
		linesParsed++;
	}
	// Close the File
	file.close();
	return dataVector;
}
