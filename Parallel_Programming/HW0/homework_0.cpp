/*
 * =====================================================================================
 *
 *       Filename:  homework_0.cpp
 *
 *    Description:  File Parsing, Computation, Homework Code Packaging, C/C++, warm-up
 *
 *        Version:  1.0
 *        Created:  08/25/2019 12:44:25 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <chrono>
#include <time.h>

using namespace std;

namespace functions
{

pair<pair<float, float>, pair<float, float>> findMinMaxKeys(vector<pair<float, float>> keys)
{
	pair<float, float> minKey = keys[0];
	pair<float, float> maxKey = keys[0];
	for (pair<float, float> key : keys)
	{
		if (key.first < minKey.first)
		{
			minKey.first = key.first;
		}
		if (key.second < minKey.second)
		{
			minKey.second = key.second;
		}
		if (key.first > maxKey.first)
		{
			maxKey.first = key.first;
		}
		if (key.second > maxKey.second)
		{
			maxKey.second = key.second;
		}
	}
	return make_pair(minKey, maxKey);
}

pair<vector<string>, vector<string>> findMinMaxVector(vector<pair<float, float>> keys, map<pair<float, float>, vector<string>> dataMap)
{
	vector<string> minBounds = dataMap.begin()->second;
	vector<string> maxBounds = dataMap.begin()->second;
	for (pair<float, float> key : keys)
	{
		vector<string> currentVec = dataMap[key];
		for (int i = 0; i < currentVec.size(); i++)
		{
			if (stof(currentVec[i]) < stof(minBounds[i]))
			{
				minBounds[i] = currentVec[i];
			}
			if (stof(currentVec[i]) > stof(maxBounds[i]))
			{
				maxBounds[i] = currentVec[i];
			}
		}
	}
	return make_pair(minBounds, maxBounds);
}

void printVector(vector<string> vec)
{
	cout << "(" << vec[0];

	for (auto loop = ++(vec.begin()); loop != vec.end(); ++loop)
	{
		cout << fixed << " , " << stof(*loop);
	}
	cout << ")" << endl;
}
vector<string> split(const string &s, char delim)
{
	stringstream ss(s);
	string item;
	vector<string> tokens;
	while (getline(ss, item, delim))
	{
		tokens.push_back(item);
	}
	return tokens;
}
} // namespace functions

/*
* A class to read data from a csv file.
*/
class CSVReader
{
	string fileName;
	char delimeter;

public:
	CSVReader(string filename, char delm = ':') : fileName(filename), delimeter(delm)
	{
	}
	// Function to fetch data from a CSV File
	map<pair<float, float>, vector<string>> getData();
	int linesParsed;
	vector<pair<float, float>> keys;
};
/*
* Parses through csv file line by line and returns the data
* in vector of vector of strings.
*/
map<pair<float, float>, vector<string>> CSVReader::getData()
{
	ifstream file(fileName);
	map<pair<float, float>, vector<string>> dataMap;
	string line = "";
	linesParsed = 0;
	// Iterate through each line and split the content using delimeter
	while (getline(file, line))
	{
		vector<string> vec;
		vec = functions::split(line, delimeter);
		pair<float, float> key(stof(vec[0]), stof(vec[1]));
		vec.erase(vec.begin(), vec.begin() + 2);
		dataMap[key] = vec;
		keys.push_back(key);
		linesParsed++;
	}
	// Close the File
	file.close();
	return dataMap;
}
int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		cout << "Invalid number of arguments entered : " << argc << endl;
		return 1;
	}
	/* initialize random seed: */
	srand(time(NULL));

	string fileStr = argv[1];
	// Creating an object of CSVWriter
	CSVReader reader(fileStr);
	// Get the data from CSV File
	auto parseStart = chrono::system_clock::now();
	map<pair<float, float>, vector<string>> dataMap = reader.getData();
	auto parseEnd = chrono::system_clock::now();
	chrono::duration<double> parseElapsedTime = parseEnd - parseStart;

	cout << "Lines in File : " << reader.linesParsed << endl;
	cout << "Records Loaded : " << dataMap.size() << endl;
	cout << "Time to Load File : " << parseElapsedTime.count() << " ms " << endl;

	auto minMaxStart = chrono::system_clock::now();
	pair<vector<string>, vector<string>> minMaxPair = functions::findMinMaxVector(reader.keys, dataMap);
	pair<pair<float, float>, pair<float, float>> minMaxKeys = functions::findMinMaxKeys(reader.keys);
	auto minMaxEnd = chrono::system_clock::now();
	chrono::duration<double> minMaxElapsedTime = minMaxEnd - minMaxStart;

	cout << "Minimum values for (X,Y) = " << endl;
	cout << fixed << minMaxKeys.first.first << " , " << minMaxKeys.first.second << endl;

	cout << "Maximum values for (X,Y) = " << endl;
	cout << fixed << minMaxKeys.second.first << " , " << minMaxKeys.second.second << endl; 

	cout << "Minimum Bounds Feature Vector : " << endl;
	functions::printVector(minMaxPair.first);

	cout << "Maximum Bounds Feature Vector : " << endl;
	functions::printVector(minMaxPair.second);

	cout << "Time to Process Data Structure for Bounds : " << minMaxElapsedTime.count() << " ms " << endl;

	int randIndex = rand() % dataMap.size();
	pair<float, float> key = reader.keys[randIndex];
	cout << "Random Index Selected : " << randIndex << endl;
	cout << "Values for (X,Y) = " << endl;
	cout << fixed << key.first << " , " << key.second << endl;
	cout << fixed << "Feature Vector : " << endl;
	functions::printVector(dataMap[key]);
	cout << endl;
	return 0;

} /* ----------  end of function main  ---------- */
