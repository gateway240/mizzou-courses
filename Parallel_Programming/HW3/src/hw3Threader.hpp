#ifndef _HW3THREADER_HPP
#define _HW3THREADER_HPP


#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/utility.hpp> // for noncopyable

#include "Coordinates.hpp"

namespace abeattie {

class HwThreeThreader : boost::noncopyable
{
public:
	HwThreeThreader(std::vector<double> weights, std::vector<Coordinates> dataVector, unsigned dataSize, unsigned numProc, unsigned radius);
	~HwThreeThreader();
	void resetDistance();
	void resetIndex();
	void operator()();
	double getTotalDistance();
	void prepNextIteration(int iteration);
	Coordinates* getOldPoints();
private:
	unsigned _dataSize;

	unsigned _radius;

	unsigned _numProc;

	unsigned _workerNodeBlock;
    
	unsigned _remainder;

	unsigned _index;

	std::vector<double> _weights;
	
	Coordinates * _oldPoints;

    Coordinates * _newPoints;
	
	// total distance worked this iteration
	double _totalDistance;
	
	// mutex
	boost::mutex * _mutex;

	boost::mutex * _indexLock;
};

}; // end of abeattie namespace

#endif // HW3THREADER_HPP

