#! /bin/bash

#SBATCH -p hpc3  # use the Lewis partition
#SBATCH -J hpc_hw3  # give the job a custom name
#SBATCH -o results-%j.out  # give the job output a custom name
#SBATCH -t 0-04:00  # two hour time limit

#SBATCH --ntasks=1  # number of cores (AKA tasks)
#SBATCH --cpus-per-task=2 # number of cpus per task



# Commands here run only on the first core
echo "$(hostname), reporting for duty."
mkdir output

# Commands with srun will run on all cores in the allocation
srun ./hw3 2 /data/scottgs/springfield_road_scan.csv 7 .97 150 > ./output/testOut-p2-t97.txt
srun ./hw3 2 /data/scottgs/springfield_road_scan.csv 7 .88 150 > ./output/testOut-p2-t88.txt
srun ./hw3 2 /data/scottgs/springfield_road_scan.csv 7 .73 150 > ./output/testOut-p2-t73.txt
srun ./hw3 2 /data/scottgs/springfield_road_scan.csv 7 .56 150 > ./output/testOut-p2-t56.txt
srun ./hw3 2 /data/scottgs/springfield_road_scan.csv 7 .41 150 > ./output/testOut-p2-t41.txt


