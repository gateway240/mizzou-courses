#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <deque>
#include <unistd.h>
#include <cstring>

#include "Coordinates.hpp"

using namespace std;
// This function converts decimal degrees to radians
double deg2rad(double deg)
{
	return (deg * M_PI / 180);
}

//  This function converts radians to decimal degrees
double rad2deg(double rad)
{
	return (rad * 180 / M_PI);
}

/**
 * Returns the distance between two points on the Earth.
 * @param lat1d Latitude of the first point in degrees
 * @param lon1d Longitude of the first point in degrees
 * @param lat2d Latitude of the second point in degrees
 * @param lon2d Longitude of the second point in degrees
 * @return The distance between the two points in meters
 */
double CoordinateFun::haversine(double lat1d, double lon1d, double lat2d, double lon2d)
{
	double lat1r, lon1r, lat2r, lon2r, u, v;
	lat1r = deg2rad(lat1d);
	lon1r = deg2rad(lon1d);
	lat2r = deg2rad(lat2d);
	lon2r = deg2rad(lon2d);

	double dlon = lon2r - lon1r;
	double dlat = lat2r - lat1r;
	double a = pow(sin(dlat / 2), 2) + cos(lat1r) * cos(lat2r) * pow(sin(dlon / 2), 2);
	double c = 2 * asin(sqrt(a));
	double r = 6371008.8; // Mean Radius of earth in meters
	return r * c;
}
Coordinates CoordinateFun::calculateWeightedMean(const vector<double> weights, const Coordinates *keys, unsigned size, const Coordinates iKey, const int radius)
{

	double xVal = 0;
	double yVal = 0;
	double weightVal = 0;

	//calculate weighted mean based on points in neighborhood
	for (unsigned j = 0; j < size; ++j)
	{

		const Coordinates jKey = keys[j];

		const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

		const double weight = weights[j];

		if (distance <= radius)
		{
			xVal += jKey.x * weight;
			yVal += jKey.y * weight;
			weightVal += weight;
		}
	}

	return (Coordinates){xVal / weightVal, yVal / weightVal};
}
vector<Coordinates> CoordinateFun::findCoLocatedPoints(vector<double> &countVec, Coordinates *keys, unsigned size, const int radius)
{
	deque<Coordinates> q(deque<Coordinates>(keys, keys + size)); //create a deque with the keys obtained from clustering

	cout << "q (size): " << q.size() << endl;
	vector<Coordinates> coLocatedKeys;
	unsigned iterationCount = 0;
	while (!q.empty())
	{
		Coordinates iKey = q.front();
		q.pop_front();
		unsigned count = 1;
		for (int i = 0; i < q.size(); i++)
		{

			const Coordinates jKey = q[i];

			const double distance = haversine(iKey.x, iKey.y, jKey.x, jKey.y);

			if (distance <= radius) // if a q is in the radius increment the count and remove it from the dequeue
			{
				count += 1;
				iterationCount++;
				q.erase(q.begin() + i);
			}
		}
		coLocatedKeys.push_back(iKey); // store the key
		countVec.push_back(count);	 // store the count
	}

	return coLocatedKeys;
}