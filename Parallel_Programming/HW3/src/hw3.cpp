#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <algorithm>
#include <chrono>
#include <deque>
#include <time.h>
#include <unistd.h>
#include <cstring>
#include <boost/thread.hpp>
#include <boost/lexical_cast.hpp>

#include "hw3Threader.hpp"
#include "CSVReader.hpp"
#include "Coordinates.hpp"

#define CONVERGANCE_DIST 1

using namespace std;

int main(int argc, char *argv[])
{

    if (argc != 6)
    {
        cout << "Invalid number of arguments entered : " << argc << endl;
        return 1;
    }
    auto programStart = chrono::system_clock::now();
    unsigned numProc = atoi(argv[1]);
    string fileStr = argv[2];
    unsigned column = atoi(argv[3]);
    double threshold = stof(argv[4]);
    double radius = stof(argv[5]);

    cout << "--------------------------begenning of program--------------------------" << endl;
    cout << "Input Parameters: " << endl;
    cout << "NumProc: " << numProc << endl;
    cout << "Threshold: " << threshold << endl;
    cout << "Column: " << column << endl;
    cout << "Radius: " << radius << endl;
    cout << "File: " << fileStr << endl;


    // Creating an object of CSVWriter
    CSVReader reader(fileStr);
    // Get the data from CSV File
    auto parseStart = chrono::system_clock::now();
    vector<double> weights;
    vector<Coordinates> dataVector = reader.getData(weights, column, threshold);
    auto parseEnd = chrono::system_clock::now();
    chrono::duration<double> parseElapsedTime = parseEnd - parseStart;

    cout << "Lines in File : " << reader.linesParsed << endl;
    cout << "Records Loaded : " << dataVector.size() << endl;
    cout << "Time to Load File : " << parseElapsedTime.count() << " s " << endl;

    const unsigned int dataSize = dataVector.size();

    cout << "Weights size: " << weights.size() << " dataVectorSize: " << dataVector.size() << endl;


    unsigned iteration = 0;
    double grandTotalDistance = 0;

    unsigned int threads = boost::lexical_cast<unsigned int>(numProc);

    unsigned workerNodeBlock = dataSize / threads; //size of block to use for each worker
    unsigned remainder = dataSize % threads;       // reaminder for last worker
    cout << "WorkerNodeBlock: " << workerNodeBlock << " Remainder: " << remainder << endl;
    bool convergance = true;

    abeattie::HwThreeThreader hwThreeThreader(weights, dataVector, dataSize, threads, radius);

    // Create a thread group
    boost::thread_group tg;

    do
    {
        hwThreeThreader.resetDistance();
        hwThreeThreader.resetIndex();
        auto weightedMeanStart = chrono::system_clock::now();
        convergance = true;
        for (unsigned t = 0; t < threads; ++t)
        {
            tg.create_thread(boost::ref(hwThreeThreader));
        }

        // Block until all threads are completed
        // The print answer
        std::cout << "About to block and wait on all my threads." << std::endl;
        tg.join_all();
        double itrDistance = hwThreeThreader.getTotalDistance();
        cout << "Total Distance Moved during Iteration : " << itrDistance << " m" << endl;
        grandTotalDistance += itrDistance;
        if (itrDistance > CONVERGANCE_DIST)
        {
            cout << "process did not converge yet" << endl;
            convergance = false;
        }
        std::cout << "In the parent (if-else): " << std::endl;

        auto weightedMeanEnd = chrono::system_clock::now();
        chrono::duration<double> weightedMeanElapsedTime = weightedMeanEnd - weightedMeanStart;
        iteration++;
        cout << "Compute Time for Weighted Mean Iteration " << iteration << " is: " << (weightedMeanElapsedTime.count()) << " s " << endl;
        hwThreeThreader.prepNextIteration(iteration);
    } while (!convergance);

    cout << "Total Number of Iterations: " << iteration << endl;
    vector<double> count;
    auto colocatedStart = chrono::system_clock::now();

    vector<Coordinates> coLocatedKeys = CoordinateFun::findCoLocatedPoints(count, hwThreeThreader.getOldPoints(), dataSize, CONVERGANCE_DIST * 2);
    auto colocatedEnd = chrono::system_clock::now();
    chrono::duration<double> colocatedElapsedTime = colocatedEnd - colocatedStart;
    cout << "Compute Time for Colocated Points is: " << (colocatedElapsedTime.count()) << " s " << endl;
    unsigned int i = 0;
    unsigned int totalCount = 0;
    cout << "Colocated points: " << endl;
    // for (const Coordinates data : coLocatedKeys)
    // {
    //     cout << std::setprecision(15) << "Point X: " << data.x << " Y: " << data.y << endl;
    //     cout << "Count : " << count[i] << endl;
    //     totalCount += count[i];
    //     i++;
    // }
    cout << "Total Distance Moved during Algorithm : " << grandTotalDistance << " m" << endl;
    cout << "Total Clusters: " << (int) coLocatedKeys.size() << endl;
    // cout << "Total Count: " << totalCount << endl;
    cout << "Done processing" << endl;

    cout << endl;

    auto programEnd = chrono::system_clock::now();
    chrono::duration<double> programElapsedTime = programEnd - programStart;
    cout << "Program runtime is: " << (programElapsedTime.count()) << " s " << endl;

    cout << "--------------------------end of program--------------------------" << endl;
    return 0;

} /* ----------  end of function main  ---------- */
