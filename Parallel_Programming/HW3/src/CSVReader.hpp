#include <string>
#include <map>
#include <vector>

#include "Coordinates.hpp"
using namespace std;
/*
* A class to read data from a csv file.
*/
class CSVReader
{
	string fileName;
	char delimeter;

public:
	CSVReader(string filename, char delm = ':') : fileName(filename), delimeter(delm)
	{
	}
	// Function to fetch data from a CSV File
	vector<Coordinates> getData(vector<double> &weights, int colFilter, double rowThreshold);
	int linesParsed;
	
};