See PDF for full details.

=======================================================
YOU MUST SPECIFY HOW TO BUILD YOUR CODE IN THIS SECTION
-------------------------------------------------------
1. run `module load gcc/gcc-8.3.0`
2. run `module load boost`
3. navigate to the hw3 folder
4. run `make`
5. change to the `src` directory (`cd src/`)
6. execute the program with the following syntax:
variables in brackets are command line arguments specified in PDF
`./hw3 [P] [F] [L] [T] [R]`
example:
`./hw3 1 /data/scottgs/springfield_road_scan.csv 7 .95 150`

- To execute all of the tests for timings execute `bash testing.sh` in the `src` directory

- This will execute a series of slurm bash commands
- The outputs of these commands will be place in the `output` folder in the `src` directory


=======================================================

Data File on TC:

/data/scottgs/springfield_road_scan.csv



For submission, you must have only source code and headers and build sytem support files.

Depending on your build system:
Use make clean or make distclean or remove an out of source build folder.

Test your submission by doing a fresh clone from your master into a temp folder and ensure it builds / runs.
e.g., 
git clone <addr_to_student_work> temp_folder_name


