**************************************
*
* Name: Steve Vai
* ID: 123456
* Date: 03/23/18
* Lab4
*
* Program description:
* This program will go through a table of 1-byte numbers with sentinel $00 and
* will send each number to a subroutine via call-by-value in register. 
* The subroutine will generate the Fibonacci sequence number according to the value sent down
* and will send the 4-byte result back to the main program via call-by-value over stack. 
* The main program will store the result in the RESARR array. 
*
* Pseudocode of Main Program:
*
* unsigned int NARR[]
* unsigned int RESARR[]
*
* pointer1=&NARR[0];
* pointer2=&RESARR[0];
* while (*pointer1 != sentinel){
*	B-register=*pointer1;
*	call subroutine
*	get upper 2 bytes of fibonacci number off the stack
*	store it to memory where pointer2 is pointing to
*	get lower 2 bytes of fibonacci number off the stack
*	store it to memory where pointer2+2 is pointing to
*	pointer2+=4
*	pointer1++
* }
* END
*
*
*---------------------------------------
*
* Local subroutine variables
*
* unsigned int RESULT (4-byte variable)
* unsigned int PREV (4-byte variable)
* unsigned int NEXT (4-byte variable)
* unsigned int N    -  holds the value sent down to subroutine
* unsigned int TMPY  -  holds original Y-register content 	
*
* Pseudocode Subroutine
*
*
* N = value sent to the subroutine
* RESULT=1
* PREV=1
* while(N>2){
*       NEXT(lower two bytes) = RESULT (lower two bytes)+ PREV (lower two bytes); 
*       NEXT(upper two bytes) = RESULT (upper two bytes); 
*	if( C-flag ==1) NEXT(upper two bytes) ++; 
*       NEXT(upper two bytes) += PREV (upper two bytes); 
*
*	 	(the above implements a 4-byte addition using two 2-byte additions) 
*
*       PREV(upper two bytes) = RESULT (upper two bytes); 
*       PREV(lower two bytes) = RESULT (lower two bytes); 
*
*       RESULT(upper two bytes) = NEXT (upper two bytes); 
*       RESULT(lower two bytes) = NEXT (lower two bytes); 
*
*       N--;
*    } 
* }
* TMPY = Y (temporarily store Y register)
* pull return address off
* push lower two bytes of result onto stack
* push upper two bytes of result onto stack
* push return address back
* Y = TMPY (restore Y register)
* RTS 
*
**************************************


* start of data section
	ORG $B000
NARR	FCB	1, 2, 5, 10, 20, 128, 254, 255, $00
SENTIN	EQU	$00

	ORG $B010
RESARR	RMB	32	



* define any variables that your MAIN program might need here
* REMEMBER: Your subroutine must not access any of the main
* program variables including NARR and RESARR.



	ORG $C000
	LDS	#$01FF	        initialize stack pointer
* start of your main program
	LDX	#NARR		initialize pointer1
	LDY	#RESARR		initialize pointer2
WHILE	LDAB	0,X		while(*pointer1 !=SENTINEL){
	CMPB	#SENTIN		
	BEQ	ENDWHILE	  (*pointer in B register)
	JSR	FIBO		  jump to subroutine
	PULA
	PULB			  get upper two result bytes off the stack
	STD	0,Y		  store them in RESARR array
	PULA
	PULB 			  get lower two result bytes off the stack
	STD	2,Y		  store them in RESARR array
	LDAB	#$4
	ABY			  pointer2+=4
	INX			  pointer1++;
	BRA	WHILE	       }
ENDWHILE
DONE	BRA	DONE



* define any variables that your SUBROUTINE might need here
RESULT  RMB     4 
PREV	RMB 	4	
NEXT	RMB 	4	
N	RMB	1
TMPY	RMB	2



	ORG $D000
* start of your subroutine
FIBO	STAB	N		N = value sent to subroutine	
	LDD	#0		clear upper two bytes of RESULT
	STD	RESULT
	STD	PREV		clear upper two bytes of PREV 
	LDD	#1
	STD	RESULT+2	RESULT = 1
	STD	PREV+2		PREV = 1	

WHILE1	LDAA	N
	CMPA	#2		while(N>2){
	BLS	ENDWHI1
	LDD	RESULT+2
	ADDD	PREV+2 		NEXT(lower two bytes) = RESULT (lower two bytes) 
	STD	NEXT+2 		                        + PREV (lower two bytes); 
	LDD	RESULT          NEXT(upper two bytes) = RESULT (upper two bytes); 
IF	BCC	ENDIF		IF_C-Flag ==1) 
THEN	ADDD	#1 			NEXT(upper two bytes) = ++; 
ENDIF	ADDD	PREV
	STD	NEXT            NEXT(upper two bytes) += PREV (upper two bytes); 


	LDD	RESULT		
	STD	PREV            PREV(upper two bytes) = RESULT (upper two bytes); 
	LDD	RESULT+2
	STD	PREV+2          PREV(lower two bytes) = RESULT (lower two bytes); 


	LDD	NEXT		
	STD	RESULT          RESULT(upper two bytes) = NEXT (upper two bytes); 
	LDD	NEXT+2
	STD	RESULT+2	RESULT(upper two bytes) = NEXT (upper two bytes); 

	DEC	N		N--
	BRA	WHILE1	 	} 
ENDWHI1	STY	TMPY		temporarily store Y register
	PULY			pull return address off
	LDD	RESULT+2
	PSHB
	PSHA			push lower two bytes of result onto stack
	LDD	RESULT
	PSHB
	PSHA			push upper two bytes of result onto stack
	PSHY			push return address back
	LDY	TMPY		restore Y register
	RTS
	END
