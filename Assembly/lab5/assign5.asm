**************************************
*
* Name: Alexander Beattie
* ID:16193698
* Date:4/12/2018
* Lab5
*
* Program description:
* This program will go through a table of 1-byte numbers with sentinel $00 and
* will send each number to a subroutine via call-by-value in register. 
* The subroutine will generate the Fibonacci sequence number according to the value sent down
* and will send the 4-byte result back to the main program via call-by-value over stack. 
* The subroutine is transparent to the main program and uses only local dynamic varialbes.
* The main program will store the result in the RESARR array. 
*
* Pseudocode of Main Program:
*
*  unsigned int NARR[]
*  unsigned int RESARR[]
*
*  pointer1=&NARR[0];
*  pointer2=&RESARR[0];
*  while (pointer1 != sentinel){
* 	B-register= *pointer1; (call-by-value in register)
* 	call subroutine
* 	get upper 2 bytes of fibonacci number off the stack
* 	store it to memory where pointer2 is pointing to
* 	get lower 2 bytes of fibonacci number off the stack
* 	store it to memory where pointer2+2 is pointing to
* 	pointer2+=4
* 	pointer1++
*  }
*  END
*---------------------------------------
*
*
*  Pseudocode Subroutine
*
*  Decrement stack pointer four times to open hole for return parameter
*
*  Push X register onto the stack
*  Push Y register onto the stack
*  Push A register onto the stack
*  Push B register onto the stack (contains value of n from main program)
*  Save condition code register in stack
*  Decrement stack pointer four times to open hole for RESULT
*  Decrement stack pointer four times to open hole for PREV
*  Decrement stack pointer four times to open hole for NEXT
*  Decrement stack pointer two times to open hole for N
*  Transfer stack pointer to X register
*
*  Store N value passed from b register in stack
*  RESULT=1
*  PREV=1
*  while(N>2){
*        NEXT(lower two bytes) = RESULT (lower two bytes)+ PREV (lower two bytes);
*        NEXT(upper two bytes) = RESULT (upper two bytes);
* 	if( C-flag ==1) NEXT(upper two bytes) ++;
*        NEXT(upper two bytes) += PREV (upper two bytes);
*
* 	 	(the above implements a 4-byte addition using two 2-byte additions)
*
*        PREV(upper two bytes) = RESULT (upper two bytes);
*        PREV(lower two bytes) = RESULT (lower two bytes);
*
*        RESULT(upper two bytes) = NEXT (upper two bytes);
*        RESULT(lower two bytes) = NEXT (lower two bytes);
*
*        N--;
*     }
*  }
*  load return address from stack into Y register
*  move the return address up 4 in the stack
*  store the value of RESULT in the parameter hole in the stack
*  increment the stack pointer 18 times to close the variable holes
*  restore condition code register
*  restore B register
*  restore A register
*  restore Y register
*  restore X register
*  RTS
**************************************



* start of data section
	ORG $B000
NARR	FCB	1, 2, 5, 10, 20, 128, 254, 255, $00
SENTIN	EQU	$00

	ORG $B010
RESARR	RMB	32	



* define any variables that your MAIN program might need here
* REMEMBER: Your subroutine must not access any of the main
* program variables including NARR and RESARR.



	ORG $C000
	LDS	#$01FF		initialize stack pointer
* start of your main program
	LDX	#NARR		initialize pointer1
	LDY	#RESARR		initialize pointer2
WHILE	LDAB	0,X		while(*pointer1 !=SENTINEL){
	CMPB	#SENTIN			
	BEQ	ENDWHILE	  value of N passed to subroutine through B register
	JSR	FIBO		  jump to subroutine
	PULA
	PULB			  get upper two result bytes off the stack
	STD	0,Y		  store them in RESARR array
	PULA
	PULB 			  get lower two result bytes off the stack
	STD	2,Y		  store them in RESARR array
	LDAB	#$4
	ABY			  pointer2+=4
	INX			  pointer1++;
	BRA	WHILE	       }
ENDWHILE
DONE	BRA	DONE


* NOTE: NO STATIC VARIABLES ALLOWED IN SUBROUTINE
*       AND SUBROUTINE MUST BE TRANSPARENT TO MAIN PROGRAM


	ORG $D000
* start of your subroutine

FIBO	DES			Open hole for return parameter
	DES
	DES
	DES
	PSHX			Save X Register
	PSHY			Save Y Register
	PSHA			Save A Register
	PSHB			Save B Register	
	TPA			Push CC Register to A
	PSHA			Save CC Register

	DES			Open hole for RESULT
	DES
	DES
	DES

	DES			Open hole for PREV
	DES			
	DES
	DES

	DES			Open hole for NEXT
	DES
	DES
	DES
	
	DES			Open hole for N
	DES

	TSX
* Variable locations on Stack:			
* RESULT = 0,X
* PREV = 4,X
* NEXT = 8,X
* N = 13,X  

	
	STAB	13,X		Store value of N passed through register to dynamic N stack variable
	LDD	#0		clear upper two bytes of RESULT
	STD	0,X
	STD	4,X		clear upper two bytes of PREV 
	LDD	#1
	STD	2,X		RESULT = 1
	STD	6,X		PREV = 1	

WHILE1	LDAA	13,X
	CMPA	#2		while(N>2){
	BLS	ENDWHI1
	LDD	2,X
	ADDD	6,X		NEXT(lower two bytes) = RESULT (lower two bytes) 
	STD	10,X 		                        + PREV (lower two bytes); 
	LDD	0,X          NEXT(upper two bytes) = RESULT (upper two bytes); 
IF	BCC	ENDIF		IF_C-Flag ==1) 
THEN	ADDD	#1 			NEXT(upper two bytes) = ++; 
ENDIF	ADDD	4,X
	STD	8,X            NEXT(upper two bytes) += PREV (upper two bytes); 


	LDD	0,X		
	STD	4,X            PREV(upper two bytes) = RESULT (upper two bytes); 
	LDD	2,X
	STD	6,X          PREV(lower two bytes) = RESULT (lower two bytes); 


	LDD	8,X		
	STD	0,X          RESULT(upper two bytes) = NEXT (upper two bytes); 
	LDD	10,X
	STD	2,X		RESULT(upper two bytes) = NEXT (upper two bytes); 

	DEC	13,X		N--
	BRA	WHILE1	 	} 
ENDWHI1
	LDY	25,X		load return address from stack
	STY	21,X		move return address up 4 in the stack
	LDD	0,X		load lower two bytes of result
	STAA	23,X
	STAB	24,X		push lower two bytes of result onto stack
	LDD	2,X		load upper two bytes of result
	STAA	25,X
	STAB	26,X		push upper two bytes of result onto stack

	INS			Close RESULT variable hole
	INS
	INS
	INS

	INS			Close PREV variable hole
	INS
	INS
	INS

	INS			Close NEXT variable hole
	INS
	INS
	INS

	INS			Close N varaiable hole
	INS
	
	PULA			Restore CC register
	TAP
	PULB			Restore B register
	PULA			Restore A regitser
	PULY			Restore Y register
	PULX			Restore X register
	
	RTS
	END
