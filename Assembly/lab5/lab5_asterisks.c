/**
 * @Author: Alex Beattie
 * @Date:   2018-04-12T22:19:34-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: lab5_asterisks.c
 * @Last modified by:   Alex Beattie
 * @Last modified time: 2018-04-12T22:46:42-05:00
 */



*  unsigned int NARR[]
*  unsigned int RESARR[]
*
*  pointer1=&NARR[0];
*  pointer2=&RESARR[0];
*  while (pointer1 != sentinel){
* 	B-register= *pointer1; (call-by-value in register)
* 	call subroutine
* 	get upper 2 bytes of fibonacci number off the stack
* 	store it to memory where pointer2 is pointing to
* 	get lower 2 bytes of fibonacci number off the stack
* 	store it to memory where pointer2+2 is pointing to
* 	pointer2+=4
* 	pointer1++
*  }
*  END
*
*
* ---------------------------------------
*
*
*  Pseudocode Subroutine
*
*  Decrement stack pointer four times to open hole for return parameter
*
*  Push X register onto the stack
*  Push Y register onto the stack
*  Push A register onto the stack
*  Push B register onto the stack (contains value of n from main program)
*  Save condition code register in stack
*  Decrement stack pointer four times to open hole for RESULT
*  Decrement stack pointer four times to open hole for PREV
*  Decrement stack pointer four times to open hole for NEXT
*  Decrement stack pointer two times to open hole for N
*  Transfer stack pointer to X register
*
*  Store N value passed from b register in stack
*  RESULT=1
*  PREV=1
*  while(N>2){
*        NEXT(lower two bytes) = RESULT (lower two bytes)+ PREV (lower two bytes);
*        NEXT(upper two bytes) = RESULT (upper two bytes);
* 	if( C-flag ==1) NEXT(upper two bytes) ++;
*        NEXT(upper two bytes) += PREV (upper two bytes);
*
* 	 	(the above implements a 4-byte addition using two 2-byte additions)
*
*        PREV(upper two bytes) = RESULT (upper two bytes);
*        PREV(lower two bytes) = RESULT (lower two bytes);
*
*        RESULT(upper two bytes) = NEXT (upper two bytes);
*        RESULT(lower two bytes) = NEXT (lower two bytes);
*
*        N--;
*     }
*  }
*  load return address from stack into Y register
*  move the return address up 4 in the stack
*  store the value of RESULT in the parameter hole in the stack
*  increment the stack pointer 18 times to close the variable holes
*  restore condition code register
*  restore B register
*  restore A register
*  restore Y register
*  restore X register
*  RTS
