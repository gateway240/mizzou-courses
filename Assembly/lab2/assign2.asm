**************************************
*
* Name: Alex Beattie
* ID: 16193698
* Date: 2/22/2018
* Lab2
*
* Program description: This program calculates
* the first N numbers in the Fibonacci sequence
*
* Pseudocode:
*
*int main (){
*    unsigned int n = 10;
*    int result;
*    unsigned int i;
*    int second;
*    int next;
*
*    result = 0;
*    second = 1;
*    i = 0;
*    do{
*        next = result+second;
*        result = second;
*        second = next;
*        i++;
*    }while(i<n);
*    //until(i>n);
*    return 0;

**************************************

* start of data section

	ORG $B000
N		FCB	10	unsigned int n = 10;

	ORG $B010
RESULT	RMB 	2	int result;
i		RMB	1	unsigned int i;
SECOND	RMB	2	int second;
NEXT 		RMB	2	int next;



	ORG $C000
	
* start of your program
	CLR	RESULT 	result = 0;
	CLR 	RESULT+1

	LDAA 	#1		second = 1;
	CLR	SECOND
	STAA	SECOND+1

		
	CLR  i		i = 0;

	CLR	NEXT		next = 0;
	CLR	NEXT+1

DO	LDD	SECOND	do{next = result+second;
	ADDD	RESULT
	STD	NEXT

	LDD	SECOND	result = second; 	
	STD	RESULT

	LDD	NEXT		second = next
	STD	SECOND

	LDAA	i 		i++
	INCA
	STAA 	i	
	
WHILE	LDAA 	i		}while(i<n);
	CMPA	N		
	BLO	DO


DONE	BRA	DONE

