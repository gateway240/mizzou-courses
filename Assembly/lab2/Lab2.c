#include <stdio.h>

//Generate N numbers in fibonachi sequence


int main (){
    unsigned int n = 10;
    unsigned int i;
    int result;
    int second;
    int next;

    printf("Enter the number of terms: ");
    scanf("%d",&n);
    result = 0;
    second = 1;
    i = 0;
    do{
        next = result+second;
        result = second;
        second = next;
        i++;
    }while(i<n);
    //until(i>n);
    printf("Result for %d values of n: %d hex: %x\n",n,result,result);
    return 0;
}
