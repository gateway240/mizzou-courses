**************************************
*
* Name: Alex Beattie
* ID: 16193698
* Date: 3/6/2018
* Lab3
*
* Program description:This program calculates
* the first N numbers in the Fibonacci sequence.
* The result is stored as a 4-byte Big Endian value.
*
* Pseudocode:

*int main (){
*    unsigned int n = 10;
*    unsigned int result[4];
*    unsigned int i;
*
*    unsigned int first[4];
*    unsigned int second[4];
*
*    int* firstPtr = &first[0];
*    int* secondPtr = &second[0];
*    i = 0;
*    *(firstPtr) = 0;
*    *(firstPtr+1) = 0;
*    *(firstPtr+2) = 0;
*    *(firstPtr+3) = 0;
*
*    *(secondPtr) = 0;
*    *(secondPtr+1) = 0;
*    *(secondPtr+2) = 0;
*    *(secondPtr+3) = 0;
*
*    result[0] = 0;
*    result[1] = 0;
*    result[2] = 0;
*    result[3] = 0;
*
*
*    int CF;
*
*    *(firstPtr+3)+=1;
*
*    while (i != n){
*     int byteCount = 4;
*        firstPtr + 3;
*        secondPtr + 3;
*        CF = 0;
*        do{
*            *firstPtr = *firstPtr + *secondPtr + CF;
*            firstPtr--;
*            secondPtr--;
*            byteCount--;
*        } while(byteCount != 0);
*
*        firstPtr = &first[0];
*        secondPtr = &second[0];
*
*        result[0] = *(firstPtr);
*        result[1] = *(firstPtr + 1);
*        result[2] = *(firstPtr + 2);
*        result[3] = *(firstPtr + 3);
*
*        *secondPtr = *firstPtr;
*        *(secondPtr) = *(firstPtr+1);
*        *(secondPtr) = *(firstPtr+2);
*        *(secondPtr) = *(firstPtr+3);
*
*        *firstPtr = result[0];
*        *(firstPtr+1) = result[1];
*        *(firstPtr +2) = result[2];
*        *(firstPtr +3) = result[3];
*        i++;
*
*
*    }
*
*    return 0;
*}
**************************************
* start of data section


	ORG 	$B000
N       FCB    	40	unsigned  int n = 40;

	ORG 	$B010
RESULT  RMB     4		unsigned int result[4];
* define any other variables that you might need here

i		RMB	1	unsigned int i;
FIRST		RMB	4	unsigned int first[4];
SECOND	RMB	4	unsigned int second[4];


* start of your program

	ORG  	$C000

	LDX	#FIRST	int* firstPtr = &first[0];
	LDY	#SECOND	int* secondPtr = &second[0];

	CLR	i	 i = 0;
	CLR	0,X	*(firstPtr) = 0;
	CLR	1,X	*(firstPtr + 1) = 0;
	CLR	2,X	*(firstPtr + 2) = 0;
	CLR	3,X	*(firstPtr + 3) = 0;

	CLR	0,Y	*(secondPtr) = 0;
	CLR	1,Y	*(secondPtr + 1) = 0;
	CLR	2,Y	*(secondPtr + 2) = 0;
	CLR	3,Y	*(secondPtr + 3) = 0;

	CLR	RESULT	result[0] = 0;
	CLR	RESULT+3	result[3] = 0;

	LDAA	3,X		  *(firstPtr+3)+=1;
	INCA
	STAA	3,X

WHILE	LDAA	i	while (i != n){
	CMPA	N
	BEQ	ENDWHILE

	LDAB	#4		 int byteCount = 4;

	LDX	#FIRST+$3	  firstPtr + 3;
	LDY	#SECOND+$3	   secondPtr + 3;

	CLC		  		CF = 0;
DO	LDAA	0,X
	ADCA	0,Y	*firstPtr = *firstPtr + *secondPtr + CF;
	STAA	0,X

	DEX		firstPtr--;
	DEY		secondPtr--;

UNTIL	DECB		byteCount--;
	BNE	DO	until(byteCount == 0)
ENDDO


	LDX	#FIRST	until(byteCount == 0)
	LDY	#SECOND	secondPtr = &second[0];

	LDD	0,X		result[0] = *(firstPtr);result[1] = *(firstPtr + 1);
	STD	RESULT
	LDD	2,X		result[2] = *(firstPtr + 2);result[3] = *(firstPtr + 3);
	STD	RESULT+2

	LDD	SECOND	*secondPtr = *firstPtr;*(secondPtr) = *(firstPtr+1);
	STD	FIRST
	LDD	SECOND+2	*(secondPtr) = *(firstPtr+2);*(secondPtr) = *(firstPtr+3);
	STD	FIRST+2

	LDD	RESULT	*firstPtr = result[0];*(firstPtr+1) = result[1];
	STD	SECOND
	LDD	RESULT+2	*(firstPtr +2) = result[2];*(firstPtr +3) = result[3];
	STD	SECOND+2


	LDAA	i		  i++;
	INCA
	STAA  i

	BRA	WHILE
ENDWHILE

DONE	BRA	DONE
