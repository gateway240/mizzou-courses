* start of data section


	ORG 	$B000
N       FCB    	40

	ORG 	$B010
RESULT  RMB     4
* define any other variables that you might need here

i		RMB	1	* counter variable for fib sequence
FIRST	RMB	4
SECOND	RMB	4


* start of your program
 
	ORG  	$C000

	LDX	#FIRST
	LDY	#SECOND

	CLR	i	* set count to zero
	CLR	0,X		* clear out x, and y - useful when running multiple times
	CLR	1,X
	CLR	2,X
	CLR	3,X	

	CLR	0,Y
	CLR	1,Y
	CLR	2,Y
	CLR	3,Y

	CLR	RESULT
	CLR	RESULT+3

	INC 	3,X	* debug, checking if add works	

WHILE	LDAA	i	* -- fibonacci loop -- *
	CMPA	N
	BEQ	ENDWHILE	* break when n is equal to i

	LDAB	#4	* load 4 into B, as counter

	LDX	#FIRST+$3	* set pointer to left most
	LDY	#SECOND+$3

	CLC
DO	LDAA	0,X	* - ripple carry loop - *
	ADCA	0,Y	* add c, y to a
	STAA	0,X	* store result in x

	DEX		*decrement pointers
	DEY	

UNTIL	DECB		*- end ripple carry loop - *
	BNE	DO
ENDDO	

	CLRA		

	LDX	#FIRST
	LDY	#SECOND

	LDD	0,X	* store first 2 bytes
	STD	RESULT
	LDD	2,X	* load last 2 bytes, store them
	STD	RESULT+2
	
	LDD	SECOND	* store B in A
	STD	FIRST
	LDD	SECOND+2
	STD	FIRST+2
	
	LDD	RESULT	* store result in B
	STD	SECOND
	LDD	RESULT+2
	STD	SECOND+2
	
	
	INC	i	* increment count
	BRA	WHILE
ENDWHILE			* -- end fibonacci loop -- *

DONE	BRA	DONE
	END