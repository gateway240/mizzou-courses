// Alex Beattie
// OS FS20 
// A5.0 File System PseudoCode

// Propose Layout
# DEFINE MAX_SIZE 64 // Max number of files avilable in the directory

struct directory {
    char* dirName; // Name of the directory
    size_t size; // Number of files in the directory. 
    // Assumes blocks are stored consecutively starting at 0
    void* files[MAX_SIZE]; // Array of file ptrs for the the files in the dir. Size 
}

// This will represent a file object
struct SP20FS {
    char* fileName; // Name of the file

    int linkCount; // Number of links in the file

    int accessRestrictions; // Unix style access restrictions for the file

    struct iNode* iNode; // Pointer to the corresponding iNode for the file
}

// Index Node (inode)
// Size is 64 bytes as defined by project spec
struct iNode {
    size_t fileSize;  

    void* directPtr; // Direct pointer to a block of data

    void* inderctPtr; // Pointer to a block of pointers that refrences data

    void* doubleInderctptr; // Pointer to a block of pointers of pointers that refrences data
}

void* createFileOrDir(char* filename, bool fileOrDir) {
    if inputs are invalid 
        return NULL;
    endif;
    if (fileOrDir == 0) { // directory
       struct directory* dir = malloc(sizeof(struct directory));
       dir -> dirName = filename;
       dir -> size = 0;
       // intalize the files array and set bit in bitmap to in use
       return dir;
    }
    else { //file
        struct SP20FS* file = malloc(sizeof(struct SP20FS));
        file -> fileName = filename;
        file -> linkCount = 0;
        file -> accessRestrictions = 777;
        file -> iNode = ;// Initalize iNode for file
        // Set bit in bitmap to in use
        return file;
    }
}

bool writeFile( int fd, void* buffer, size_t size) {
    if inputs are invalid
        return false;
    endif;
    struct SP20FS* file = attempt to open fd from bitmap; // return false if failure
    // copy size blocks from buffer to file iNode ptrs appropraitely 
    // update linkCount;
    return true;
}

bool deleteFile (char *fileName) {
    if inputs are invalid
        return false;
    endif;
    struct SP20FS* file = attempt to open fd; // return false if failure
    // recursively delete/free iNode refrences from file iNode block
    // clear bit set in bitmap
    return true;
}