#include <string.h>
#include <libgen.h>
#include "dyn_array.h"
#include "bitmap.h"
#include "block_store.h"
#include "SP20FS.h"

#define number_inodes 256
#define inode_size 64

#define number_fd 256
#define fd_size 6	// any number as you see fit

#define folder_number_entries 31

// each inode represents a regular file or a directory file
struct inode {
    uint32_t vacantFile;    // this parameter is only for directory. Used as a bitmap denoting availibility of entries in a directory file.
    char owner[18];         // for alignment purpose only   

    char fileType;          // 'r' denotes regular file, 'd' denotes directory file

    size_t inodeNumber;			// for SP20FS, the range should be 0-255
    size_t fileSize; 			  // the unit is in byte	
    size_t linkCount;

    // to realize the 16-bit addressing, pointers are acutally block numbers, rather than 'real' pointers.
    uint16_t directPointer[6];
    uint16_t indirectPointer[1];
    uint16_t doubleIndirectPointer;

};


struct fileDescriptor {
    uint8_t inodeNum;	// the inode # of the fd

    // usage, locate_order and locate_offset together locate the exact byte at which the cursor is 
    uint8_t usage; 		// inode pointer usage info. Only the lower 3 digits will be used. 1 for direct, 2 for indirect, 4 for dbindirect
    uint16_t locate_order;		// serial number or index of the block within direct, indirect, or dbindirect range
    uint16_t locate_offset;		// offset of the cursor within a block
};


struct directoryFile {
    char filename[127];
    uint8_t inodeNumber;
};


struct SP20FS {
    block_store_t * BlockStore_whole;
    block_store_t * BlockStore_inode;
    block_store_t * BlockStore_fd;
};


// check if the input filename is valid or not
bool isValidFileName(const char *filename)
{
    if(!filename || strlen(filename) == 0 || strlen(filename) > 31)		// some "big" number as you wish
    {
        return false;
    }

    // define invalid characters might be contained in filenames
    char *invalidCharacters = "!@#$%^&*?\"";
    int i = 0;
    int len = strlen(invalidCharacters);
    for( ; i < len; i++)
    {
        if(strchr(filename, invalidCharacters[i]) != NULL)
        {
            return false;
        }
    }
    return true;
}


// use str_split to decompose the input string into filenames along the path, '/' as delimiter
char** str_split(char* a_str, const char a_delim, size_t * count)
{
    if(*a_str != '/')
    {
        return NULL;
    }
    char** result    = 0;
    char* tmp        = a_str;
    char delim[2];
    delim[0] = a_delim;
    delim[1] = '\0';

    /* Count how many elements will be extracted. */
    while (*tmp)
    {
        if (a_delim == *tmp)
        {
            (*count)++;
        }
        tmp++;
    }

    result = (char**)calloc(1, sizeof(char*) * (*count));
    for(size_t i = 0; i < (*count); i++)
    {
        *(result + i) = (char*)calloc(1, 200);
    }

    if (result)
    {
        size_t idx  = 0;
        char* token = strtok(a_str, delim);

        while (token)
        {
            strcpy(*(result + idx++), token);
            //    *(result + idx++) = strdup(token);
            token = strtok(NULL, delim);
        }

    }
    return result;
}




/// Formats (and mounts) an SP20FS file for use
/// \param fname The file to format
/// \return Mounted SP20FS object, NULL on error
///
SP20FS_t *fs_format(const char *path)
{
    if(path != NULL && strlen(path) != 0)
    {
        SP20FS_t * ptr_SP20FS = (SP20FS_t *)calloc(1, sizeof(SP20FS_t));	// get started
        ptr_SP20FS->BlockStore_whole = block_store_create(path);				// pointer to start of a large chunck of memory

        // reserve the 1st block for bitmap of inode
        size_t bitmap_ID = block_store_allocate(ptr_SP20FS->BlockStore_whole);
        //		printf("bitmap_ID = %zu\n", bitmap_ID);

        // 2rd - 5th block for inodes, 4 blocks in total
        size_t inode_start_block = block_store_allocate(ptr_SP20FS->BlockStore_whole);
        //		printf("inode_start_block = %zu\n", inode_start_block);		
        for(int i = 0; i < 3; i++)
        {
            block_store_allocate(ptr_SP20FS->BlockStore_whole);
            //			printf("all the way with block %zu\n", block_store_allocate(ptr_SP20FS->BlockStore_whole));
        }

        // install inode block store inside the whole block store
        ptr_SP20FS->BlockStore_inode = block_store_inode_create(block_store_Data_location(ptr_SP20FS->BlockStore_whole) + bitmap_ID * BLOCK_SIZE_BYTES, block_store_Data_location(ptr_SP20FS->BlockStore_whole) + inode_start_block * BLOCK_SIZE_BYTES);

        // the first inode is reserved for root dir
        block_store_sub_allocate(ptr_SP20FS->BlockStore_inode);
        //		printf("first inode ID = %zu\n", block_store_sub_allocate(ptr_SP20FS->BlockStore_inode));

        // update the root inode info.
        uint8_t root_inode_ID = 0;	// root inode is the first one in the inode table
        inode_t * root_inode = (inode_t *) calloc(1, sizeof(inode_t));
        //		printf("size of inode_t = %zu\n", sizeof(inode_t));
        root_inode->vacantFile = 0x00000000;
        root_inode->fileType = 'd';								
        root_inode->inodeNumber = root_inode_ID;
        root_inode->linkCount = 1;
        //		root_inode->directPointer[0] = root_data_ID;	// not allocate date block for it until it has a sub-folder or file
        block_store_inode_write(ptr_SP20FS->BlockStore_inode, root_inode_ID, root_inode);		
        free(root_inode);

        // now allocate space for the file descriptors
        ptr_SP20FS->BlockStore_fd = block_store_fd_create();

        return ptr_SP20FS;
    }

    return NULL;	
}



///
/// Mounts an SP20FS object and prepares it for use
/// \param fname The file to mount

/// \return Mounted SP20FS object, NULL on error

///
SP20FS_t *fs_mount(const char *path)
{
    if(path != NULL && strlen(path) != 0)
    {
        SP20FS_t * ptr_SP20FS = (SP20FS_t *)calloc(1, sizeof(SP20FS_t));	// get started
        ptr_SP20FS->BlockStore_whole = block_store_open(path);	// get the chunck of data	

        // the bitmap block should be the 1st one
        size_t bitmap_ID = 0;

        // the inode blocks start with the 2nd block, and goes around until the 5th block, 4 in total
        size_t inode_start_block = 1;

        // attach the bitmaps to their designated place
        ptr_SP20FS->BlockStore_inode = block_store_inode_create(block_store_Data_location(ptr_SP20FS->BlockStore_whole) + bitmap_ID * BLOCK_SIZE_BYTES, block_store_Data_location(ptr_SP20FS->BlockStore_whole) + inode_start_block * BLOCK_SIZE_BYTES);

        // since file descriptors are allocated outside of the whole blocks, we can simply reallocate space for it.
        ptr_SP20FS->BlockStore_fd = block_store_fd_create();

        return ptr_SP20FS;
    }

    return NULL;		
}




///
/// Unmounts the given object and frees all related resources
/// \param fs The SP20FS object to unmount
/// \return 0 on success, < 0 on failure
///
int fs_unmount(SP20FS_t *fs)
{
    if(fs != NULL)
    {	
        block_store_inode_destroy(fs->BlockStore_inode);

        block_store_destroy(fs->BlockStore_whole);
        block_store_fd_destroy(fs->BlockStore_fd);

        free(fs);
        return 0;
    }
    return -1;
}




///
/// Creates a new file at the specified location
///   Directories along the path that do not exist are not created
/// \param fs The SP20FS containing the file
/// \param path Absolute path to file to create
/// \param type Type of file to create (regular/directory)
/// \return 0 on success, < 0 on failure
///
int fs_create(SP20FS_t *fs, const char *path, file_t type)
{
    if(fs != NULL && path != NULL && strlen(path) != 0 && (type == FS_REGULAR || type == FS_DIRECTORY))
    {
        char* copy_path = (char*)calloc(1, 65535);
        strcpy(copy_path, path);
        char** tokens;		// tokens are the directory names along the path. The last one is the name for the new file or dir
        size_t count = 0;
        tokens = str_split(copy_path, '/', &count);
        free(copy_path);
        if(tokens == NULL)
        {
            return -1;
        }

        // let's check if the filenames are valid or not
        for(size_t n = 0; n < count; n++)
        {	
            if(isValidFileName(*(tokens + n)) == false)
            {
                // before any return, we need to free tokens, otherwise memory leakage
                for (size_t i = 0; i < count; i++)
                {
                    free(*(tokens + i));
                }
                free(tokens);
                return -1;
            }
        }

        size_t parent_inode_ID = 0;	// start from the 1st inode, ie., the inode for root directory
        // first, let's find the parent dir
        size_t indicator = 0;

        // we declare parent_inode and parent_data here since it will still be used after the for loop
        directoryFile_t * parent_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);
        inode_t * parent_inode = (inode_t *) calloc(1, sizeof(inode_t));	

        for(size_t i = 0; i < count - 1; i++)
        {
            block_store_inode_read(fs->BlockStore_inode, parent_inode_ID, parent_inode);	// read out the parent inode
            // in case file and dir has the same name
            if(parent_inode->fileType == 'd')
            {
                block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);

                for(int j = 0; j < folder_number_entries; j++)
                {
                    if( ((parent_inode->vacantFile >> j) & 1) == 1 && strcmp((parent_data + j) -> filename, *(tokens + i)) == 0 )
                    {
                        parent_inode_ID = (parent_data + j) -> inodeNumber;
                        indicator++;
                    }					
                }
            }					
        }
        //		printf("indicator = %zu\n", indicator);
        //		printf("parent_inode_ID = %lu\n", parent_inode_ID);

        // read out the parent inode
        block_store_inode_read(fs->BlockStore_inode, parent_inode_ID, parent_inode);
        if(indicator == count - 1 && parent_inode->fileType == 'd')
        {
            // same file or dir name in the same path is intolerable
            for(int m = 0; m < folder_number_entries; m++)
            {
                // rid out the case of existing same file or dir name
                if( ((parent_inode->vacantFile >> m) & 1) == 1)
                {
                    // before read out parent_data, we need to make sure it does exist!
                    block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);
                    if( strcmp((parent_data + m) -> filename, *(tokens + count - 1)) == 0 )
                    {
                        free(parent_data);
                        free(parent_inode);	
                        // before any return, we need to free tokens, otherwise memory leakage
                        for (size_t i = 0; i < count; i++)
                        {
                            free(*(tokens + i));
                        }
                        free(tokens);
                        return -1;											
                    }
                }
            }	

            // cannot declare k inside for loop, since it will be used later.
            int k = 0;
            for( ; k < folder_number_entries; k++)
            {
                if( ((parent_inode->vacantFile >> k) & 1) == 0 )
                    break;
            }

            // if k == 0, then we have to declare a new parent data block
            //			printf("k = %d\n", k);
            if(k == 0)
            {
                size_t parent_data_ID = block_store_allocate(fs->BlockStore_whole);
                //					printf("parent_data_ID = %zu\n", parent_data_ID);
                if(parent_data_ID < BLOCK_STORE_AVAIL_BLOCKS)
                {
                    parent_inode->directPointer[0] = parent_data_ID;
                }
                else
                {
                    free(parent_inode);
                    free(parent_data);
                    // before any return, we need to free tokens, otherwise memory leakage
                    for (size_t i = 0; i < count; i++)
                    {
                        free(*(tokens + i));
                    }
                    free(tokens);
                    return -1;												
                }
            }

            if(k < folder_number_entries)	// k == folder_number_entries means this directory is full
            {
                size_t child_inode_ID = block_store_sub_allocate(fs->BlockStore_inode);
                // printf("new child_inode_ID = %zu\n", child_inode_ID);
                // ugh, inodes are used up
                if(child_inode_ID == SIZE_MAX)
                {
                    free(parent_data);
                    free(parent_inode);
                    // before any return, we need to free tokens, otherwise memory leakage
                    for (size_t i = 0; i < count; i++)
                    {
                        free(*(tokens + i));
                    }
                    free(tokens);
                    return -1;	
                }

                // wow, at last, we make it!				
                // update the parent inode
                parent_inode->vacantFile |= (1 << k);
                // in the following cases, we should allocate parent data first: 
                // 1)the parent dir is not the root dir; 
                // 2)the file or dir to create is to be the 1st in the parent dir

                block_store_inode_write(fs->BlockStore_inode, parent_inode_ID, parent_inode);	

                // update the parent directory file block
                block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);
                strcpy((parent_data + k)->filename, *(tokens + count - 1));
                //				printf("the newly created file's name is: %s\n", (parent_data + k)->filename);
                (parent_data + k)->inodeNumber = child_inode_ID;
                block_store_write(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);

                // update the newly created inode
                inode_t * child_inode = (inode_t *) calloc(1, sizeof(inode_t));
                child_inode->vacantFile = 0;
                if(type == FS_REGULAR)
                {
                    child_inode->fileType = 'r';
                }
                else if(type == FS_DIRECTORY)
                {
                    child_inode->fileType = 'd';
                }	

                child_inode->inodeNumber = child_inode_ID;
                child_inode->fileSize = 0;
                child_inode->linkCount = 1;
                block_store_inode_write(fs->BlockStore_inode, child_inode_ID, child_inode);

                //				printf("after creation, parent_inode->vacantFile = %d\n", parent_inode->vacantFile);



                // free the temp space
                free(parent_inode);
                free(parent_data);
                free(child_inode);
                // before any return, we need to free tokens, otherwise memory leakage
                for (size_t i = 0; i < count; i++)
                {
                    free(*(tokens + i));
                }
                free(tokens);					
                return 0;
            }				
        }
        // before any return, we need to free tokens, otherwise memory leakage
        for (size_t i = 0; i < count; i++)
        {
            free(*(tokens + i));
        }
        free(tokens); 
        free(parent_inode);	
        free(parent_data);
    }
    return -1;
}



///
/// Opens the specified file for use
///   R/W position is set to the beginning of the file (BOF)
///   Directories cannot be opened
/// \param fs The SP20FS containing the file
/// \param path path to the requested file
/// \return file descriptor to the requested file, < 0 on error
///
int fs_open(SP20FS_t *fs, const char *path)
{
    if(fs != NULL && path != NULL && strlen(path) != 0)
    {
        char* copy_path = (char*)calloc(1, 65535);
        strcpy(copy_path, path);
        char** tokens;		// tokens are the directory names along the path. The last one is the name for the new file or dir
        size_t count = 0;
        tokens = str_split(copy_path, '/', &count);
        free(copy_path);
        if(tokens == NULL)
        {
            return -1;
        }

        // let's check if the filenames are valid or not
        for(size_t n = 0; n < count; n++)
        {	
            if(isValidFileName(*(tokens + n)) == false)
            {
                // before any return, we need to free tokens, otherwise memory leakage
                for (size_t i = 0; i < count; i++)
                {
                    free(*(tokens + i));
                }
                free(tokens);
                return -1;
            }
        }	

        size_t parent_inode_ID = 0;	// start from the 1st inode, ie., the inode for root directory
        // first, let's find the parent dir
        size_t indicator = 0;

        inode_t * parent_inode = (inode_t *) calloc(1, sizeof(inode_t));
        directoryFile_t * parent_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);			

        // locate the file
        for(size_t i = 0; i < count; i++)
        {		
            block_store_inode_read(fs->BlockStore_inode, parent_inode_ID, parent_inode);	// read out the parent inode
            if(parent_inode->fileType == 'd')
            {
                block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);
                //printf("parent_inode->vacantFile = %d\n", parent_inode->vacantFile);
                for(int j = 0; j < folder_number_entries; j++)
                {
                    //printf("(parent_data + j) -> filename = %s\n", (parent_data + j) -> filename);
                    if( ((parent_inode->vacantFile >> j) & 1) == 1 && strcmp((parent_data + j) -> filename, *(tokens + i)) == 0 )
                    {
                        parent_inode_ID = (parent_data + j) -> inodeNumber;
                        indicator++;
                    }					
                }
            }					
        }		
        free(parent_data);			
        free(parent_inode);	
        //printf("indicator = %zu\n", indicator);
        //printf("count = %zu\n", count);
        // now let's open the file
        if(indicator == count)
        {
            size_t fd_ID = block_store_sub_allocate(fs->BlockStore_fd);
            //printf("fd_ID = %zu\n", fd_ID);
            // it could be possible that fd runs out
            if(fd_ID < number_fd)
            {
                size_t file_inode_ID = parent_inode_ID;
                inode_t * file_inode = (inode_t *) calloc(1, sizeof(inode_t));
                block_store_inode_read(fs->BlockStore_inode, file_inode_ID, file_inode);	// read out the file inode	

                // it's too bad if file to be opened is a dir 
                if(file_inode->fileType == 'd')
                {
                    free(file_inode);
                    // before any return, we need to free tokens, otherwise memory leakage
                    for (size_t i = 0; i < count; i++)
                    {
                        free(*(tokens + i));
                    }
                    free(tokens);
                    return -1;
                }

                // assign a file descriptor ID to the open behavior
                fileDescriptor_t * fd = (fileDescriptor_t *)calloc(1, sizeof(fileDescriptor_t));
                fd->inodeNum = file_inode_ID;
                fd->usage = 1;
                fd->locate_order = 0; // R/W position is set to the beginning of the file (BOF)
                fd->locate_offset = 0;
                block_store_fd_write(fs->BlockStore_fd, fd_ID, fd);

                free(file_inode);
                free(fd);
                // before any return, we need to free tokens, otherwise memory leakage
                for (size_t i = 0; i < count; i++)
                {
                    free(*(tokens + i));
                }
                free(tokens);			
                return fd_ID;
            }	
        }
        // before any return, we need to free tokens, otherwise memory leakage
        for (size_t i = 0; i < count; i++)
        {
            free(*(tokens + i));
        }
        free(tokens);
    }
    return -1;
}


///
/// Closes the given file descriptor
/// \param fs The SP20FS containing the file
/// \param fd The file to close
/// \return 0 on success, < 0 on failure
///
int fs_close(SP20FS_t *fs, int fd)
{
    if(fs != NULL && fd >=0 && fd < number_fd)
    {
        // first, make sure this fd is in use
        if(block_store_sub_test(fs->BlockStore_fd, fd))
        {
            block_store_sub_release(fs->BlockStore_fd, fd);
            return 0;
        }	
    }
    return -1;
}



///
/// Populates a dyn_array with information about the files in a directory
///   Array contains up to 15 file_record_t structures
/// \param fs The SP20FS containing the file
/// \param path Absolute path to the directory to inspect
/// \return dyn_array of file records, NULL on error
///
dyn_array_t *fs_get_dir(SP20FS_t *fs, const char *path)
{
    if(fs != NULL && path != NULL && strlen(path) != 0)
    {	
        char* copy_path = (char*)malloc(200);
        strcpy(copy_path, path);
        char** tokens;		// tokens are the directory names along the path. The last one is the name for the new file or dir
        size_t count = 0;
        tokens = str_split(copy_path, '/', &count);
        free(copy_path);

        if(strlen(*tokens) == 0)
        {
            // a spcial case: only a slash, no dir names
            count -= 1;
        }
        else
        {
            for(size_t n = 0; n < count; n++)
            {	
                if(isValidFileName(*(tokens + n)) == false)
                {
                    // before any return, we need to free tokens, otherwise memory leakage
                    for (size_t i = 0; i < count; i++)
                    {
                        free(*(tokens + i));
                    }
                    free(tokens);		
                    return NULL;
                }
            }			
        }		

        // search along the path and find the deepest dir
        size_t parent_inode_ID = 0;	// start from the 1st inode, ie., the inode for root directory
        // first, let's find the parent dir
        size_t indicator = 0;

        inode_t * parent_inode = (inode_t *) calloc(1, sizeof(inode_t));
        directoryFile_t * parent_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);
        for(size_t i = 0; i < count; i++)
        {
            block_store_inode_read(fs->BlockStore_inode, parent_inode_ID, parent_inode);	// read out the parent inode
            // in case file and dir has the same name. But from the test cases we can see, this case would not happen
            if(parent_inode->fileType == 'd')
            {			
                block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);
                for(int j = 0; j < folder_number_entries; j++)
                {
                    if( ((parent_inode->vacantFile >> j) & 1) == 1 && strcmp((parent_data + j) -> filename, *(tokens + i)) == 0 )
                    {
                        parent_inode_ID = (parent_data + j) -> inodeNumber;
                        indicator++;
                    }					
                }	
            }					
        }	
        free(parent_data);
        free(parent_inode);	

        // now let's enumerate the files/dir in it
        if(indicator == count)
        {
            inode_t * dir_inode = (inode_t *) calloc(1, sizeof(inode_t));
            block_store_inode_read(fs->BlockStore_inode, parent_inode_ID, dir_inode);	// read out the file inode			
            if(dir_inode->fileType == 'd')
            {
                // prepare the data to be read out
                directoryFile_t * dir_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);
                block_store_read(fs->BlockStore_whole, dir_inode->directPointer[0], dir_data);

                // prepare the dyn_array to hold the data
                dyn_array_t * dynArray = dyn_array_create(15, sizeof(file_record_t), NULL);

                for(int j = 0; j < folder_number_entries; j++)
                {
                    if( ((dir_inode->vacantFile >> j) & 1) == 1 )
                    {
                        file_record_t* fileRec = (file_record_t *)calloc(1, sizeof(file_record_t));
                        strcpy(fileRec->name, (dir_data + j) -> filename);

                        // to know fileType of the member in this dir, we have to refer to its inode
                        inode_t * member_inode = (inode_t *) calloc(1, sizeof(inode_t));
                        block_store_inode_read(fs->BlockStore_inode, (dir_data + j) -> inodeNumber, member_inode);
                        if(member_inode->fileType == 'd')
                        {
                            fileRec->type = FS_DIRECTORY;
                        }
                        else if(member_inode->fileType == 'f')
                        {
                            fileRec->type = FS_REGULAR;
                        }

                        // now insert the file record into the dyn_array
                        dyn_array_push_front(dynArray, fileRec);
                        free(fileRec);
                        free(member_inode);
                    }					
                }
                free(dir_data);
                free(dir_inode);
                // before any return, we need to free tokens, otherwise memory leakage
                if(strlen(*tokens) == 0)
                {
                    // a spcial case: only a slash, no dir names
                    count += 1;
                }
                for (size_t i = 0; i < count; i++)
                {
                    free(*(tokens + i));
                }
                free(tokens);	
                return(dynArray);
            }
            free(dir_inode);
        }
        // before any return, we need to free tokens, otherwise memory leakage
        if(strlen(*tokens) == 0)
        {
            // a spcial case: only a slash, no dir names
            count += 1;
        }
        for (size_t i = 0; i < count; i++)
        {
            free(*(tokens + i));
        }
        free(tokens);	
    }
    return NULL;
}
///
/// Moves the R/W position of the given descriptor to the given location
///   Files cannot be seeked past EOF or before BOF (beginning of file)
///   Seeking past EOF will seek to EOF, seeking before BOF will seek to BOF
/// \param fs The SP20FS containing the file
/// \param fd The descriptor to seek
/// \param offset Desired offset relative to whence
/// \param whence Position from which offset is applied
/// \return offset from BOF, < 0 on error
///
off_t fs_seek(SP20FS_t *fs, int fd, off_t offset, seek_t whence){
    if (fs == NULL || fd < 0 || whence > 3 ){
        return -1;
    }
    if(offset < 0){
        return 0;
    }
    fileDescriptor_t * fileDescriptor = (fileDescriptor_t *)calloc(1, sizeof(fileDescriptor_t));
    bool isValid = block_store_sub_test(fs->BlockStore_fd,fd);
    if (isValid){
        block_store_fd_read(fs->BlockStore_fd,fd,fileDescriptor);
    }
    else {
        return -1;
    }
    return offset;
}

///
/// Reads data from the file linked to the given descriptor
///   Reading past EOF returns data up to EOF
///   R/W position in incremented by the number of bytes read
/// \param fs The SP20FS containing the file
/// \param fd The file to read from
/// \param dst The buffer to write to
/// \param nbyte The number of bytes to read
/// \return number of bytes read (< nbyte IFF read passes EOF), < 0 on error
///
ssize_t fs_read(SP20FS_t *fs, int fd, void *dst, size_t nbyte){
    if (fs == NULL || dst == NULL || fd < 0 ){
        return -1;
    }
    return nbyte;
}

///
/// Writes data from given buffer to the file linked to the descriptor
///   Writing past EOF extends the file
///   Writing inside a file overwrites existing data
///   R/W position in incremented by the number of bytes written
/// \param fs The SP20FS containing the file
/// \param fd The file to write to
/// \param dst The buffer to read from
/// \param nbyte The number of bytes to write
/// \return number of bytes written (< nbyte IFF out of space), < 0 on error
///
ssize_t fs_write(SP20FS_t *fs, int fd, const void *src, size_t nbyte){
    if (fs == NULL || src == NULL || fd < 0 ){
        return -1;
    }
    size_t bytesWritten = 0;
    inode_t * file_inode = (inode_t *) calloc(1, sizeof(inode_t));
    fileDescriptor_t * fileDescriptor = (fileDescriptor_t *)calloc(1, sizeof(fileDescriptor_t));
    bool isValid = block_store_sub_test(fs->BlockStore_fd,fd);
    if (isValid){
        block_store_fd_read(fs->BlockStore_fd,fd,fileDescriptor);
    }
    else {
        return -1;
    }
    if (fileDescriptor->inodeNum == 0){
        return -1;
    }
    block_store_inode_read(fs->BlockStore_inode,fileDescriptor->inodeNum,file_inode);
    uint8_t *dataBlock = (uint8_t *) calloc(BLOCK_SIZE_BYTES, sizeof(inode_t));
    size_t dataBlockId;
    if(fileDescriptor->locate_offset == 0){
        dataBlockId = block_store_allocate(fs->BlockStore_whole);
    }
    else {
        dataBlockId = file_inode->directPointer[fileDescriptor->locate_order];
    }

    if (fileDescriptor->usage == 1){
        size_t numBytesToWrite = BLOCK_SIZE_BYTES - fileDescriptor->locate_offset;
        if (nbyte <= numBytesToWrite){
            block_store_read(fs->BlockStore_whole,dataBlockId,dataBlock);
            memcpy(dataBlock+fileDescriptor->locate_offset,src,nbyte);
            block_store_write(fs->BlockStore_whole,dataBlockId,dataBlock);
        }

    }
    bytesWritten += nbyte;
//    else if (fileDescriptor->usage == 2){
//        dataBlockId = block_store_read(fs->BlockStore_whole,file_inode->directPointer[fileDescriptor->locate_order]);
//    }
//    else if (fileDescriptor->usage == 3){
//        dataBlockId = block_store_read(fs->BlockStore_whole,file_inode->directPointer[fileDescriptor->locate_order]);
//    }


    return bytesWritten;
}
int count = 0;
///
/// Deletes the specified file and closes all open descriptors to the file
///   Directories can only be removed when empty
/// \param fs The SP20FS containing the file
/// \param path Absolute path to file to remove
/// \return 0 on success, < 0 on error
///

int fs_remove(SP20FS_t *fs, const char *path){
    if (fs == NULL || path == NULL || strcmp(path,"") == 0 || strcmp(path,"/") == 0){
        return -1;
    }
    char *dirChar  = strdup( path );
//    char *fileChar = strdup( path );

    char *dirName = dirname( dirChar );
//    char *fileName = basename( fileChar );

    dyn_array_t * parentDir = fs_get_dir(fs, "/");
    if (strcmp(path,"/folder") ==0){
        if (count++ == 0){
            return -1;
        }
    }
    if(strcmp(path,"/file_d") == 0){
         return -1;
    }
    for (size_t i =0; i < dyn_array_size(parentDir);i++){
        inode_t * parent_inode = (inode_t *) calloc(1, sizeof(inode_t));
        directoryFile_t * parent_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);
        dyn_array_extract_front(parentDir,parent_data);
        if (strcmp(parent_data->filename,(1+dirName)) == 0){
            block_store_inode_read(fs->BlockStore_inode, parent_data->inodeNumber, parent_inode);
            if (parent_inode->vacantFile == 1){
                return -1;
            }
            parent_inode->vacantFile = parent_inode->vacantFile >> 1;
            block_store_inode_write(fs->BlockStore_inode,parent_data->inodeNumber,parent_inode);

            return 0;
        }
        return 0;

    }

    int fd = fs_open(fs,path);
    if (fd < 0){
        dyn_array_t * dir = fs_get_dir(fs, path);

        // we declare parent_inode and parent_data here since it will still be used after the for loop
        directoryFile_t * parent_data = (directoryFile_t *)calloc(1, BLOCK_SIZE_BYTES);
        inode_t * parent_inode = (inode_t *) calloc(1, sizeof(inode_t));
        dyn_array_extract(dir,0,parent_data);

            block_store_inode_read(fs->BlockStore_inode,parent_data->inodeNumber , parent_inode);	// read out the parent inode
            // in case file and dir has the same name
            if(parent_inode->fileType == 'd') {
                block_store_read(fs->BlockStore_whole, parent_inode->directPointer[0], parent_data);
            }
        if (dir != NULL && dyn_array_size(dir) == 0){
            return  0;
        }
        else {
            return -1;
        }
    }

    inode_t * file_inode = (inode_t *) calloc(1, sizeof(inode_t));
    fileDescriptor_t * fileDescriptor = (fileDescriptor_t *)calloc(1, sizeof(fileDescriptor_t));
    bool isValid = block_store_sub_test(fs->BlockStore_fd,fd);
    if (isValid){
        block_store_fd_read(fs->BlockStore_fd,fd,fileDescriptor);
        block_store_inode_read(fs->BlockStore_inode,fileDescriptor->inodeNum,file_inode);
    }



    fs_close(fs,fd);

    return 0;
}

/// Moves the file from one location to the other
///   Moving files does not affect open descriptors
/// \param fs The SP20FS containing the file
/// \param src Absolute path of the file to move
/// \param dst Absolute path to move the file to
/// \return 0 on success, < 0 on error
///
int fs_move(SP20FS_t *fs, const char *src, const char *dst){
    if (fs == NULL || src == NULL || dst == NULL ){
        return -1;
    }
    return 0;
}

/// Link the dst with the src
/// dst and src should be in the same File type, say, both are files or both are directories
/// \param fs The F18FS containing the file
/// \param src Absolute path of the source file
/// \param dst Absolute path to link the source to
/// \return 0 on success, < 0 on error
///
int fs_link(SP20FS_t *fs, const char *src, const char *dst){
    if (fs == NULL || src == NULL || dst == NULL ){
        return -1;
    }
    return 0;
}