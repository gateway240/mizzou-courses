#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#include "../include/sstring.h"

bool string_valid(const char *str, const size_t length)
{
    //check valid input and correct terminator character
    if (! str || length == 0 || str[length - 1] != '\0')
    {
        return false;
    }

    return true;
}

char *string_duplicate(const char *str, const size_t length)
{
    if (! str || length == 0)
    {
        return NULL;
    }

    //create a string to copy to
    char *new_s = (char*) malloc(length * sizeof(char));

    //copy the bytes one by one until the specified length
    int i;

    for (i = 0; i < length; ++i)
    {
        new_s[i] = str[i];
    }

    return new_s;
}

bool string_equal(const char *str_a, const char *str_b, const size_t length)
{
    //validate input
    if (! str_a || ! str_b || length == 0)
    {
        return false;
    }

    //check bytes upto to length for equality
    //memcmp returns 0 if equal
    int res = memcmp(str_a, str_b, length);

    if (res == 0)
    {
        //the two strings are equal up to 'length' bytes
        return true;
    }

    return false;
}

int string_length(const char *str, const size_t length)
{
    if (! str || length == 0)
    {
        return -1;
    }

    //prevent segfault by getting actual segment length
    int seglen = sizeof(str) / sizeof(char);

    int strlen = 0;

    int i;
    for (i = 0; i < seglen; ++i)
    {
        if (str[i] != '\0')
        {
            strlen++;
        }
        else
        {
            break;
        }
    }

    return strlen;
}

int string_tokenize(const char *str, const char *delims, const size_t str_length, char **tokens, const size_t max_token_length, const size_t requested_tokens)
{
    //check for incorrect parameters
    if (! str || ! delims || str_length == 0 || max_token_length == 0 || requested_tokens == 0 || ! tokens)
    {
        return 0;
    }

    int r;
    for (r = 0; r < requested_tokens; ++r)
    {
        if (tokens[r] == NULL)
        {
            //provided token was null
            return -1;
        }
    }

    //read in all characters and create tokens as necessary
    r = 0; //indicates current token
    int c = 0; //position in current token
    char* s = (char*) str; //current character

    while (*s != '\0')
    {
        //is s one of the delimeters
        char* d = (char*) delims;
        while (*d != '\0' && *d != *s)
            d++;

        if (*d != '\0')
        {
            //s is a delimeter!

            //finish off current token
            tokens[r][c] = '\0';

            //go to next token
            r++;
            c = 0;

            //check to see if number of requested tokens met
            if (r == requested_tokens)
            {
                return r;
            }
        }
        else
        {
            //s is not a delimeter
            tokens[r][c] = *s;
            c++;
            //jump to just past the next delimeter and start next token
            if (c == max_token_length - 1)
            {
                tokens[r][c] = '\0';
                r++;
                c = 0;

                //check against number of requested tokens
                if (r == requested_tokens)
                {
                    return r;
                }
            }
        }
        s++;
    }
    // return total number of tokens
    return r + 1;
}

bool string_to_int(const char *str, int *converted_value)
{
    if (! str || ! converted_value)
    {
        return false;
    }

    //convert to larger container than int
    long long int val = strtoll(str, NULL, 10);

    //validate size
    if (val > INT_MAX)
    {
        return false;
    }

    *converted_value = (int)val;

    //conversion success
    return true;
}
