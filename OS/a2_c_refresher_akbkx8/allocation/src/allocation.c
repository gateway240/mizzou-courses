#define _XOPEN_SOURCE 700
#include "../include/allocation.h"
#include <stdlib.h>
#include <stdio.h>


void* allocate_array(size_t member_size, size_t nmember,bool clear)
{
    if (member_size == 0 || nmember == 0)
    {
        return NULL;
    }

    if (clear)
    {
        return calloc(nmember, member_size);
    }
    else
    {
        return malloc(nmember * member_size);
    }
}

void* reallocate_array(void* ptr, size_t size)
{
    if (size == 0)
    {
        return NULL;
    }
    return realloc(ptr, size);
}

void deallocate_array(void** ptr)
{
    if (! ptr)
    {
        return;
    }
    free(*ptr);
    *ptr = NULL;
}

char* read_line_to_buffer(char* filename)
{
    //get size of file
    FILE* file = fopen(filename, "r");

    //check to see if file opened
    if (file == NULL)
    {
        return NULL;
    }

    char* line = NULL;
    size_t n = 0;

    int res = getline(&line, &n, file);

    fclose(file);

    //if error
    if (res == -1)
    {
        return NULL;
    }
    return line;
}
