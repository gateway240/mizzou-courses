#include "../include/bitmap.h"

#include <math.h>

// data is an array of uint8_t and needs to be allocated in bitmap_create
//      and used in the remaining bitmap functions. You will use data for any bit operations and bit logic
// bit_count the number of requested bits, set in bitmap_create from n_bits
// byte_count the total number of bytes the data contains, set in bitmap_create


bitmap_t *bitmap_create(size_t n_bits)
{
    if (n_bits == 0 || n_bits == SIZE_MAX)
    {
        return NULL;
    }
    bitmap_t *map = (bitmap_t *) malloc(sizeof(bitmap_t));

    if (map == NULL)
    {
        return NULL;
    }

    //allocate specified bits
    map->bit_count = n_bits;

    map->byte_count = (size_t) ceil((double) n_bits / 8);

    map->data = (uint8_t *) calloc(map->byte_count, 1);

    return map;
}

bool bitmap_set(bitmap_t *const bitmap, const size_t bit)
{
    if (! bitmap || bit >= bitmap->bit_count)
    {
        return false;
    }

    uint8_t *b = bitmap->data;

    //figure out number of bytes and jump there
    b += (bit / 8);

    //figure out bit in byte
    size_t bit_num = bit % 8;

    //create mask to set bit
    uint8_t mask = 0x01 << bit_num;

    //apply mask
    (*b) = (*b) | mask;

    return true;
}

bool bitmap_reset(bitmap_t *const bitmap, const size_t bit)
{
    if (! bitmap || bit >= bitmap->bit_count)
    {
        return false;
    }

    uint8_t *b = bitmap->data;

    //figure out number of bytes and jump there
    b += (bit / 8);

    //figure out bit position
    size_t bit_num = bit % 8;

    //create mask to reset bit
    uint8_t mask = 0x01 << bit_num;

    //flip bit
    mask = 0xFF ^ mask;

    //apply mask
    (*b) = (*b) & mask;

    return true;
}

bool bitmap_test(const bitmap_t *const bitmap, const size_t bit)
{
    if (! bitmap || bit >= bitmap->bit_count)
    {
        return false;
    }

    uint8_t *b = bitmap->data;

    b += (bit / 8);

    size_t byte = *b;
    size_t bit_num = bit % 8;

    uint8_t mask = 0x01 << bit_num;
    byte &= mask;

    if (byte > 0)
    {
        return true;
    }

    return false;
}

size_t bitmap_ffs(const bitmap_t *const bitmap)
{
    if (! bitmap || ! bitmap->data)
    {
        return SIZE_MAX;
    }

    size_t i = 0; //current byte index
    uint8_t *d = bitmap->data; //current data pointer

    //while still bitmap and d equals 0
    while (i < bitmap->byte_count && *d == 0)
    {
        ++d;
        ++i;
    }

    if (i == bitmap->byte_count)
    {
        //apply mask of zeros to extra bits in last byte
        uint8_t mask = 0xFF >> (8 - bitmap->bit_count % 8);

        if (((*d) & mask) == 0x00)
        {
            //no ones at the end
            return SIZE_MAX;
        }
    }

    //figure out which bit
    size_t bit_num = 1;

    while (*d >> bit_num != 0)
        bit_num++;

    //i = num bytes
    //bit_num = zero based num bits in ith byte
    return (i * 8) + (bit_num - 1); //subtract 1 to get zeroth index
}

size_t bitmap_ffz(const bitmap_t *const bitmap)
{
    if (! bitmap || ! bitmap->data)
    {
        return SIZE_MAX;
    }

    size_t i = 0; //current byte index
    uint8_t *d = bitmap->data; //current data pointer

    //while there's still bitmap and d equals 1 (in hex)
    while (i < bitmap->byte_count && *d == 0xFF)
    {
        ++d;
        ++i;
    }

    if (i + 1 == bitmap->byte_count)
    {
        //apply mask of ones to empty bits in final byte
        uint8_t mask = 0xFF << bitmap->bit_count % 8;

        if (((*d) | mask) == 0xFF)
        {
            // no zeros found
            return SIZE_MAX;
        }
    }

    //figure out which bit is first zero in byte
    size_t bit_num = 1;

    uint8_t t = (*d) ^ 0xFF;

    while ((uint8_t)(t << bit_num) != 0)
        bit_num++;
    //i = how many bytes
    //bit_num = zero based num bits in the ith byte
    return (i * 8) + (8 - bit_num); //subtract 1 for zero based index
}

bool bitmap_destroy(bitmap_t *bitmap)
{
    if (! bitmap)
    {
        return false;
    }

    if (bitmap->data != NULL)
    {
        free(bitmap->data);
    }

    free(bitmap);
    return true;

}
