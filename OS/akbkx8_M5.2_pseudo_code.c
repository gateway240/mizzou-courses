// Alex Beattie
// M5.1 PseudoCode

///
/// Moves the R/W position of the given descriptor to the given location
///   Files cannot be seeked past EOF or before BOF (beginning of file)
///   Seeking past EOF will seek to EOF, seeking before BOF will seek to BOF
/// \param fs The SP20FS containing the file
/// \param fd The descriptor to seek
/// \param offset Desired offset relative to whence
/// \param whence Position from which offset is applied
/// \return offset from BOF, < 0 on error
///
off_t fs_seek(SP20FS_t *fs, int fd, off_t offset, seek_t whence){
	if input invalid resturn -1
	iNode file = search fs.blockstore for given filedescriptor
	locate the whence position on the file 
	if going to the offset from whence != BOF or EOF
		return the value from the BOF to the current position		
	
}

///
/// Reads data from the file linked to the given descriptor
///   Reading past EOF returns data up to EOF
///   R/W position in incremented by the number of bytes read
/// \param fs The SP20FS containing the file
/// \param fd The file to read from
/// \param dst The buffer to write to
/// \param nbyte The number of bytes to read
/// \return number of bytes read (< nbyte IFF read passes EOF), < 0 on error
///
ssize_t fs_read(SP20FS_t *fs, int fd, void *dst, size_t nbyte){
	if input invalid resturn -1
	iNode file = search fs.blockstore for given filedescriptor
	traverse through the blocks pointed to by the iNode for the fd
		for a given block copy the number of bytes specified to the dst buffer
		if there are more bytes then in the current block	
			store the position of the dst buffer
			go to the next block and write to the dst buffer starting with the previous offset
	return number of bytes read (should == nbyte)
	
}

///
/// Writes data from given buffer to the file linked to the descriptor
///   Writing past EOF extends the file
///   Writing inside a file overwrites existing data
///   R/W position in incremented by the number of bytes written
/// \param fs The SP20FS containing the file
/// \param fd The file to write to
/// \param dst The buffer to read from
/// \param nbyte The number of bytes to write
/// \return number of bytes written (< nbyte IFF out of space), < 0 on error
///
ssize_t fs_write(SP20FS_t *fs, int fd, const void *src, size_t nbyte){
	if input invalid resturn -1
	iNode file = search fs.blockstore for given filedescriptor
	traverse through the blocks pointed to by the iNode for the fd
		for a given block copy the number of bytes specified from the src buffer to the inode data block
		if there are more bytes then in the current block	
			store the position of the src buffer
			go to the next block and write to it from the src buffer starting with the previous offset
	return number of bytes written (should == nbyte)
	
}


///
/// Deletes the specified file and closes all open descriptors to the file
///   Directories can only be removed when empty
/// \param fs The SP20FS containing the file
/// \param path Absolute path to file to remove
/// \return 0 on success, < 0 on error
///
int fs_remove(SP20FS_t *fs, const char *path){
	if input invalid resturn -1
	iNode file = search through fs.inode to find file
	recursively free all data block pointers (direct, inderect and double inderect)
	remove the refrence to the inode from the inode table
	remove the refrence from the file descriptor block store if exists
	return 0
	
}


/// Moves the file from one location to the other
///   Moving files does not affect open descriptors
/// \param fs The SP20FS containing the file
/// \param src Absolute path of the file to move
/// \param dst Absolute path to move the file to
/// \return 0 on success, < 0 on error
///
int fs_move(SP20FS_t *fs, const char *src, const char *dst){
	if input invalid resturn -1
	iNode file = search through fs.inode to find file
	create new directory descriptor for the desired moved path
	point the newly created directory at the file iNode
	remove the previous (old) directory descriptor refrence to the file iNode
	
}

/// Link the dst with the src
/// dst and src should be in the same File type, say, both are files or both are directories
/// \param fs The F20FS containing the file
/// \param src Absolute path of the source file
/// \param dst Absolute path to link the source to
/// \return 0 on success, < 0 on error
///
int fs_link(SP20FS_t *fs, const char *src, const char *dst){
	if input invalid resturn -1
	iNode file = search through fs.inode to find file
	create new directory descriptor for the desired linked path
	point the newly created directory at the file iNode
	return 0;
	
}