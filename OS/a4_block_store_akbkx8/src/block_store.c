#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "bitmap.h"
#include "block_store.h"
// include more if you need

typedef struct block_store{
    char* data[BLOCK_STORE_AVAIL_BLOCKS][BLOCK_STORE_AVAIL_BLOCKS];
    bitmap_t* bitmap;
} block_store_t;

// You might find this handy.  I put it around unused parameters, but you should
// remove it before you submit. Just allows things to compile initially.
#define UNUSED(x) (void)(x)

block_store_t *block_store_create()
{
    block_store_t* blockStore = malloc(sizeof(block_store_t));
    if (blockStore == NULL) {
        return NULL;
    }
    // Set all bytes to 0 for serialization and deserialization
    memset(blockStore->data,0,BLOCK_STORE_AVAIL_BLOCKS*BLOCK_STORE_AVAIL_BLOCKS);
    blockStore->bitmap = bitmap_create(((BITMAP_SIZE_BYTES*BITMAP_SIZE_BYTES)/2));
    return blockStore;
}
void block_store_destroy(block_store_t *const bs)
{
    if( bs != NULL && bs->bitmap != NULL){
        bitmap_destroy(bs->bitmap);
        free(bs);
    }
    else if (bs != NULL){
        free(bs);
    }
}
size_t block_store_allocate(block_store_t *const bs)
{
    if (bs != NULL) {
        size_t firstZeroAddr = bitmap_ffz(bs->bitmap);
        if (firstZeroAddr != SIZE_MAX && firstZeroAddr != BLOCK_STORE_AVAIL_BLOCKS ){
            // Set a block in use and return the first zero address
            bitmap_set(bs->bitmap,firstZeroAddr);
            return firstZeroAddr;
        }
    }
    return SIZE_MAX;
}

bool block_store_request(block_store_t *const bs, const size_t block_id)
{
    if (bs != NULL && block_id <= BLOCK_STORE_AVAIL_BLOCKS && block_id != 0) {
        if (!bitmap_test(bs->bitmap,block_id)) {
            // Try to set the desired bit
            bitmap_set(bs->bitmap, block_id);
            // Return result of setting the bit
            return bitmap_test(bs->bitmap,block_id);
        }
    }
    // return 0 for failure conditions in if statement
    return false;
}

void block_store_release(block_store_t *const bs, const size_t block_id)
{
    if (bs != NULL){
        bitmap_reset(bs->bitmap,block_id);
    }
}

size_t block_store_get_used_blocks(const block_store_t *const bs)
{
    if (bs != NULL){
        return bitmap_total_set(bs->bitmap);
    }
    // Return size max for error ex. block store is null
    return SIZE_MAX;
}

size_t block_store_get_free_blocks(const block_store_t *const bs)
{
    if (bs != NULL) {
        return BLOCK_STORE_AVAIL_BLOCKS-bitmap_total_set(bs->bitmap);
    }
    // Return size max for error ex. block store is null
    return SIZE_MAX;
}

size_t block_store_get_total_blocks()
{
    return BLOCK_STORE_AVAIL_BLOCKS;
}

size_t block_store_read(const block_store_t *const bs, const size_t block_id, void *buffer)
{
    if (bs != NULL && block_id <= BLOCK_STORE_AVAIL_BLOCKS && buffer != NULL){
        memcpy(buffer,bs->data[block_id],BLOCK_SIZE_BYTES);
        return BLOCK_SIZE_BYTES;
    }
    // Return 0 on error condition
    return 0;
}

size_t block_store_write(block_store_t *const bs, const size_t block_id, const void *buffer)
{
    if (bs != NULL && buffer != NULL){
        memcpy(bs->data[block_id],buffer,BLOCK_SIZE_BYTES);
        return BLOCK_SIZE_BYTES;
    }
    // Return 0 on error condition
    return 0;
}

block_store_t *block_store_deserialize(const char *const filename)
{
    const int BITS_IN_BYTE = 8;
    if (filename != NULL){
        block_store_t *bs = malloc(sizeof(block_store_t));
        int inFile = open(filename,O_RDONLY);
        if (inFile == -1){
            printf("Something went wrong with open()! %s\n", strerror(errno));
            return 0;
        }
        // Read in the raw data first and place into the data section
        const int RAW_DATA_READ = BLOCK_STORE_AVAIL_BLOCKS*BLOCK_STORE_AVAIL_BLOCKS;
        int inRead = read(inFile,bs->data,RAW_DATA_READ);
        if (inRead == -1){
            printf("Something went wrong with read()! %s\n", strerror(errno));
            return 0;
        }
        //  Read in the bitmap back to memory from the file and place in the bitmap section
        void *bitmap_buffer = malloc(BITMAP_SIZE_BYTES);
        int bitmapRead = pread(inFile,bitmap_buffer,BITMAP_SIZE_BYTES,RAW_DATA_READ);
        if (bitmapRead == -1){
            printf("Something went wrong with pread()! %s\n", strerror(errno));
            return 0;
        }
        bs->bitmap = bitmap_import(BITMAP_SIZE_BYTES*BITS_IN_BYTE-1,bitmap_buffer);
        // Free the memory
        free(bitmap_buffer);
        close(inFile);
        return bs;
    }
    // return NULL on ERROR
    return NULL;
}

size_t block_store_serialize(const block_store_t *const bs, const char *const filename)
{
    if (bs != NULL && filename != NULL){
        // Create or overwrite a file with read and write permissions for any user
        int outFile = open(filename,O_WRONLY | O_CREAT | O_TRUNC, 0666);
        if (outFile == -1){
            printf("Something went wrong with open()! %s\n", strerror(errno));
            return 0;
        }
        const int RAW_DATA_WRITE = BLOCK_STORE_AVAIL_BLOCKS*BLOCK_STORE_AVAIL_BLOCKS;
        // Write the block store to file starting with the data
        int bytesWritten = write(outFile,bs->data,RAW_DATA_WRITE);
        if (bytesWritten == -1){
            printf("Something went wrong with write()! %s\n", strerror(errno));
            return 0;
        }
        // wirte the bitmap to a file using the offset of the data section
        const uint8_t *rawBitmap = bitmap_export(bs->bitmap);
        int bitmapWritten = pwrite(outFile,rawBitmap,(BITMAP_SIZE_BYTES*BITMAP_SIZE_BYTES/2)-1,RAW_DATA_WRITE);
        if (bitmapWritten == -1){
            printf("Something went wrong with pwrite()! %s\n", strerror(errno));
            return 0;
        }
        close(outFile);
        return bytesWritten + bitmapWritten;
    }
    // return 0 on ERROR
    return 0;
}