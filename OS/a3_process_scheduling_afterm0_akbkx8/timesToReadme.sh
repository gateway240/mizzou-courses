#!/usr/bin/env bash
# Script to run all timings and append output to readme.md
(time ./analysis PCBs.bin FCFS ) &>> readme.md
(time ./analysis PCBs.bin RR 4 ) &>> readme.md
(time ./analysis PCBs.bin SJF ) &>> readme.md
(time ./analysis PCBs.bin SRTF ) &>> readme.md

