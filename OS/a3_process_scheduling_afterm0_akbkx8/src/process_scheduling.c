#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "dyn_array.h"
#include "processing_scheduling.h"


// private function
void virtual_cpu(ProcessControlBlock_t *process_control_block) {
    // decrement the burst time of the pcb
    --process_control_block->remaining_burst_time;
}

bool first_come_first_serve(dyn_array_t *ready_queue, ScheduleResult_t *result) {
    if (ready_queue == NULL || result == NULL) {
        return false;
    }
    // Figure out how many processes are on the ready queue
    size_t size = dyn_array_size(ready_queue);
    size_t totalTime = 0;
    size_t totalTurnaroundTime = 0;
    size_t totalWaitingTime = 0;

    for (size_t i = 0; i < size; i++) {
        size_t turnaroundTime = 0;
        size_t waitingTime = 0;
        ProcessControlBlock_t *currentPCB = dyn_array_at(ready_queue, size - i - 1);
        size_t burstTime = currentPCB->remaining_burst_time;
        // Execute the process until it has no more burst time remaining
        while (currentPCB->remaining_burst_time > 0) {
            virtual_cpu(currentPCB);
            totalTime++;
        }
        // Calculate the timing statistics for the individual process
        turnaroundTime = totalTime - currentPCB->arrival;
        waitingTime = turnaroundTime - burstTime;
        // Add the timing statistics to the global total
        totalTurnaroundTime += turnaroundTime;
        totalWaitingTime += waitingTime;
    }
    // Calculate average statistics
    float avgWaitTime = (float) totalWaitingTime / (float) size;
    float avgTurnaroundTime = (float) totalTurnaroundTime / (float) size;
    result->total_run_time = totalTime;
    result->average_turnaround_time = avgTurnaroundTime;
    result->average_waiting_time = avgWaitTime;

    return true;
}

/*
 * Compare the arrival time of two PCBS
 */
int compare(const void *pcb, const void *pcb2) {
    return ((ProcessControlBlock_t *) pcb)->arrival > ((ProcessControlBlock_t *) pcb2)->arrival ? -1 :
           ((ProcessControlBlock_t *) pcb)->arrival < ((ProcessControlBlock_t *) pcb2)->arrival ? 1 : 0;
}

int compareBurst(const void *pcb, const void *pcb2) {
    return ((ProcessControlBlock_t *) pcb)->remaining_burst_time >
           ((ProcessControlBlock_t *) pcb2)->remaining_burst_time ? -1 :
           ((ProcessControlBlock_t *) pcb)->remaining_burst_time <
           ((ProcessControlBlock_t *) pcb2)->remaining_burst_time ? 1 : 0;
}

bool shortest_job_first(dyn_array_t *ready_queue, ScheduleResult_t *result) {
    if (ready_queue == NULL || result == NULL) {
        return false;
    }
    // Figure out how many processes are on the ready queue
    int size = dyn_array_size(ready_queue);
    size_t totalTime = 0;
    size_t totalTurnaroundTime = 0;

    ProcessControlBlock_t currentPCB;
    /*
     * This algorithm was difficult to implement because it required double sorting of the PCBS
     * I accomplished this by creating a sub array of processes that had arrived and then  sorting that
     * by burst time. When all arrived processes have finished executing I go back to the original ready queue
     * and add the newly arrived processes to a new arrived sub array and repeat.
     */
    do {
        // Sort the array by arrival time
        dyn_array_sort(ready_queue, compare);
        // Create a sub array of processes that have arrived
        dyn_array_t *arrivedProcs = dyn_array_create(size, sizeof(ProcessControlBlock_t), NULL);
        // Push the first process on the arrived process array
        dyn_array_extract_back(ready_queue, &currentPCB);
        dyn_array_push_back(arrivedProcs, &currentPCB);
        do {
            ProcessControlBlock_t nextPCB;
            // Get the next PCB from the ready queue
            dyn_array_extract_back(ready_queue, &nextPCB);
            // If it has arrived add it to the subarray otherwise push it back on the ready queue
            if (nextPCB.arrival <= totalTime) {
                dyn_array_push_back(arrivedProcs, &nextPCB);
                continue;
            } else {
                dyn_array_push_back(ready_queue, &nextPCB);
                break;
            }
        } while (!dyn_array_empty(ready_queue));
        // Sort the arrived processes by burst time
        dyn_array_sort(arrivedProcs, compareBurst);

        // Since this is non-preemptive we can schedule until all arrived processes have completed
        while (!dyn_array_empty(arrivedProcs)) {
            ProcessControlBlock_t schedulePCB;
            dyn_array_extract_back(arrivedProcs, &schedulePCB);
            while (schedulePCB.remaining_burst_time > 0) {
                virtual_cpu(&schedulePCB);
                totalTime++;
            }
            totalTurnaroundTime += totalTime - schedulePCB.arrival;
        }
        // Cleanup sub array
        dyn_array_destroy(arrivedProcs);
    } while (!dyn_array_empty(ready_queue));

    // Calculate average statistics
    float avgWaitTime = (float) (totalTurnaroundTime - totalTime) / (float) size;
    float avgTurnaroundTime = (float) totalTurnaroundTime / (float) size;
    result->total_run_time = totalTime;
    result->average_turnaround_time = avgTurnaroundTime;
    result->average_waiting_time = avgWaitTime;

    return true;
}

bool round_robin(dyn_array_t *ready_queue, ScheduleResult_t *result, size_t quantum) {
    if (ready_queue == NULL || result == NULL) {
        return false;
    }
    size_t size = dyn_array_size(ready_queue);
    size_t totalTime = 0;
    size_t totalTurnaroundTime = 0;

    ProcessControlBlock_t currentPCB;
    /*
     * This algorithm is fairly  simple as all processes get an equal time slice and simply have to be scheduled following
     * this circular pattern.
     */
    while (!dyn_array_empty(ready_queue)) {
        dyn_array_extract_back(ready_queue, &currentPCB);

        // For the provided quantum schedule the process for the duration of the quantum or until it is finished.
        for (size_t i = 0; i < quantum; i++) {
            if (currentPCB.remaining_burst_time <= 0) {
                break;
            }
            virtual_cpu(&currentPCB);
            totalTime++;
        }
        // If the process finished calculate timing statistics otherwise push it to the front of the ready queue
        if (currentPCB.remaining_burst_time <= 0) {
            totalTurnaroundTime += totalTime;
            continue;
        } else {
            dyn_array_push_front(ready_queue, &currentPCB);
        }
    }
    // Calculate average statistics
    float avgWaitTime = (float) (totalTurnaroundTime - totalTime) / (float) size;
    float avgTurnaroundTime = (float) totalTurnaroundTime / (float) size;
    result->total_run_time = totalTime;
    result->average_turnaround_time = avgTurnaroundTime;
    result->average_waiting_time = avgWaitTime;
    return true;
}

dyn_array_t *load_process_control_blocks(const char *input_file) {
    if (input_file == NULL) {
        return NULL;
    }
    FILE *pFile;
    pFile = fopen(input_file, "rb");
    if (pFile == NULL) {
        return NULL;
    }
    fseek(pFile, 0, SEEK_END); // seek to end of file
    size_t size = ftell(pFile); // get current file pointer
    fseek(pFile, 0, SEEK_SET); // seek back to beginning of file
    if (size <= 0) {
        fclose(pFile);
        return NULL;
    }
    // Load the input file into a buffer
    uint32_t pcbs[size];
    fread(pcbs, sizeof(pcbs), 1, pFile); // read file to buffer
    fclose(pFile);

    // Calculte the number of processes in the buffer
    uint32_t dec_pcb_num = pcbs[0];
    uint32_t num_pcb = (size / 3) / sizeof(uint32_t);
    if (num_pcb < dec_pcb_num) {
        return NULL;
    }
    // Set a pointer to the first job in the array
    uint32_t *pcbs_ptr = pcbs;
    pcbs_ptr += 1;
    // Create a dyn_array_t for the process control blocks
    dyn_array_t *result = dyn_array_create(num_pcb, sizeof(ProcessControlBlock_t), NULL);
    for (size_t i = 0; i < num_pcb; ++i) {
        // Load a ProcessControlBlock from the array and push it to the resulting array
        ProcessControlBlock_t pPCB = {pcbs_ptr[i * 3 + 0], pcbs_ptr[i * 3 + 1], pcbs_ptr[i * 3 + 2], 0};
        dyn_array_push_back(result, &pPCB);
    }
    return result;
}

bool shortest_remaining_time_first(dyn_array_t *ready_queue, ScheduleResult_t *result) {
    if (ready_queue == NULL || result == NULL) {
        return false;
    }
    // Figure out how many processes are on the ready queue
    size_t size = dyn_array_size(ready_queue);
    size_t totalTime = 0;
    size_t totalTurnaroundTime = 0;

    ProcessControlBlock_t currentPCB;
    size_t nextArrival = 0;
    /*
     * This algorithm was difficult to implement because it required double sorting of the PCBS and managing arrival time
     * I accomplished this by creating a sub array of processes that had arrived and then sorting that
     * by burst time. This algorithm then differs from the shortest job first because it only schedules the current process
     * until a new process arrives. When a new process arrives it pushes all non-complete processes back on the ready queue in the
     * correct order and makes a new sub array with processes that have arrived and schedules until completion or another process
     * arrives
     */
    do {
        // Sort the array by arrival time
        dyn_array_sort(ready_queue, compare);
        // Create a sub array of processes that have arrived
        dyn_array_t *arrivedProcs = dyn_array_create(size, sizeof(ProcessControlBlock_t), NULL);
        // Push the first process on the arrived process array
        dyn_array_extract_back(ready_queue, &currentPCB);
        dyn_array_push_back(arrivedProcs, &currentPCB);
        do {
            ProcessControlBlock_t nextPCB;
            // Get the next PCB from the ready queue
            dyn_array_extract_back(ready_queue, &nextPCB);
            nextArrival = nextPCB.arrival;
            // If it has arrived add it to the subarray otherwise push it back on the ready queue
            if (nextPCB.arrival <= totalTime) {
                dyn_array_push_back(arrivedProcs, &nextPCB);
                continue;
            } else {
                dyn_array_push_back(ready_queue, &nextPCB);
                break;
            }
        } while (!dyn_array_empty(ready_queue));
        // Sort the arrived processes by burst time
        dyn_array_sort(arrivedProcs, compareBurst);

        ProcessControlBlock_t schedulePCB;
        dyn_array_extract_back(arrivedProcs, &schedulePCB);
        /*
         * Since this is preemptive we can only schedule until the next process has arrived
         * The scheduling will schedule the arrived processes one clock tick at a time and evaluate if they have completed
         * If the process is complete then a new process will be pulled off the arrived processes sub array otherwise
         * the loop will break. The important portion of the loop is that it can only schedule until a new process shows up
         * because when the new process shows up it could have a burst time shorter then the currently arrived processes and
         * would then need to be scheduled.
         */
        while (totalTime < nextArrival || dyn_array_empty(ready_queue)) {
            virtual_cpu(&schedulePCB);
            totalTime++;
            if (schedulePCB.remaining_burst_time > 0) {
                continue;
            } else {
                totalTurnaroundTime += totalTime - schedulePCB.arrival;
                if (!dyn_array_empty(arrivedProcs)) {
                    dyn_array_extract_back(arrivedProcs, &schedulePCB);
                    continue;
                } else {
                    break;
                }
            }
        }
        // Handle pushing processes on the sub array that have not completed back on to the ready queue in the correct order
        if (schedulePCB.remaining_burst_time > 0) {
            dyn_array_push_front(arrivedProcs, &schedulePCB);
        }
        while (!dyn_array_empty(arrivedProcs)) {
            ProcessControlBlock_t waitingProc;
            dyn_array_extract_back(arrivedProcs, &waitingProc);
            if (waitingProc.remaining_burst_time > 0) {
                dyn_array_push_back(ready_queue, &waitingProc);
            }
        }
        // Cleanup sub array
        dyn_array_destroy(arrivedProcs);
    } while (!dyn_array_empty(ready_queue));

    // Calculate average statistics
    float avgWaitTime = (float) (totalTurnaroundTime - totalTime) / (float) size;
    float avgTurnaroundTime = (float) totalTurnaroundTime / (float) size;
    result->total_run_time = totalTime;
    result->average_turnaround_time = avgTurnaroundTime;
    result->average_waiting_time = avgWaitTime;
    return true;
}
