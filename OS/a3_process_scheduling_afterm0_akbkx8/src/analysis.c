#include <stdio.h>
#include <stdlib.h>

#include "dyn_array.h"
#include "processing_scheduling.h"

#define FCFS "FCFS"
#define P "P"
#define RR "RR"
#define SJF "SJF"
#define SRTF "SRTF"

// Add and comment your analysis code in this function.
// THIS IS NOT FINISHED.
int main(int argc, char **argv) {
    // I wrote a script called "timesToReadme.sh"  to run all of the scheduling algorithms and output to the readme
    // This script is at the root of the repository and can be run or edited accordingly to accomplish this task
    if (argc < 3) {
        printf("%s <pcb file> <schedule algorithm> [quantum]\n", argv[0]);
        return EXIT_FAILURE;
    }

    dyn_array_t* dyn_array = load_process_control_blocks(argv[1]);

    if(dyn_array == NULL){
        printf("Process Control Block cannot be NULL\n");
        return EXIT_FAILURE;
    }
    ScheduleResult_t* result = (ScheduleResult_t*) malloc(sizeof(ScheduleResult_t));
    result->average_waiting_time = 0;
    result->average_turnaround_time = 0;
    result-> total_run_time = 0;

    printf("\n");
    if(strcmp(FCFS, argv[2]) == 0 ){
        printf("First Come First Served executing\n");
        first_come_first_serve(dyn_array, result);
    }

    if(strcmp(RR, argv[2]) == 0){
        printf("Round Robin executing\n");
        if (argc < 4){
            //quantum not specified
            printf("A quantum must be specified for Round Robin Scheduling\n");
            return EXIT_FAILURE;
        }
        int quantum = atoi(argv[3]);
        round_robin(dyn_array, result,quantum);
    }

    if(strcmp(SJF, argv[2]) == 0){
        printf("Shortest Job First executing\n");
        shortest_job_first(dyn_array, result);
    }

    if(strcmp(SRTF, argv[2]) == 0){
        printf("Shortest Remaining Time First executing\n");
        shortest_remaining_time_first(dyn_array, result);
    }
    printf("Average Waiting Time: %f\n", result->average_waiting_time);
    printf("Average Turnaround Time: %f\n", result->average_turnaround_time);
    printf("Total Run Time: %lu\n", result-> total_run_time);
    dyn_array_destroy(dyn_array);
    free(result);
    return EXIT_SUCCESS;
}
