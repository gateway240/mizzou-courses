# Assignment 3

Milestone 0: CMake and Unit Testing filled out 

Milestone 1: PCB file loading and First Come, First Served 

Milestone 2: Shortest Job First, Round Robin, and analysis of algorithms 

Note: 
You can manually copy the time analysis from console and paste it to this readme file, but directly output from your program is strongly recommended.     
---------------------------------------------------------------------------
Add your scheduling algorithm analysis below this line in a readable format. 
---------------------------------------------------------------------------

First Come First Served executing
Average Waiting Time: 185.233337
Average Turnaround Time: 198.800003
Total Run Time: 407

real	0m0.007s
user	0m0.000s
sys	0m0.000s

Round Robin executing
Average Waiting Time: 230.399994
Average Turnaround Time: 243.966660
Total Run Time: 407

real	0m0.006s
user	0m0.000s
sys	0m0.000s

Shortest Job First executing
Average Waiting Time: 146.533340
Average Turnaround Time: 160.100006
Total Run Time: 407

real	0m0.007s
user	0m0.000s
sys	0m0.000s

Shortest Remaining Time First executing
Average Waiting Time: 104.133331
Average Turnaround Time: 117.699997
Total Run Time: 407

real	0m0.006s
user	0m0.000s
sys	0m0.000s
