// Alex Beattie
// M5.1 PseudoCode

///
/// Creates a new file at the specified location
///   Directories along the path that do not exist are not created
/// \param fs The SP20FS containing the file
/// \param path Absolute path to file to create
/// \param type Type of file to create (regular/directory)
/// \return 0 on success, < 0 on failure
///
int fs_create(SP20FS_t *fs, const char *path, file_t type){
	1. Check for valid input (fs != NULL)
	2. Allocate block storage (block_store) for the raw file data
	3. reate the iNode tables for the raw data and associate with the block_store
	4. Create a new file_record_t for the file with the correct type
	5. Link the newly created resources back to the original fs object
	6. return 0 on success, < 0 on failure
	
}

///
/// Opens the specified file for use
///   R/W position is set to the beginning of the file (BOF)
///   Directories cannot be opened
/// \param fs The SP20FS containing the file
/// \param path path to the requested file
/// \return file descriptor to the requested file, < 0 on error
///
int fs_open(SP20FS_t *fs, const char *path) {
	1. Check for valid inputs (fs != NULL)
	2. Parse input path to seperate directories and files
	3. For each directory from root
		a. Search file index for current directory
		b. If desired dir found follow pointer to it
		c. Otherwise return error
		d. repeat until the desired directories are reached
	4. Search the final directory for the desired file and follow pointer
	5. Create file descriptor and store value in refrence table to file
	6. return file descriptor
}

///
/// Closes the given file descriptor
/// \param fs The SP20FS containing the file
/// \param fd The file to close
/// \return 0 on success, < 0 on failure
///
int fs_close(SP20FS_t *fs, int fd){ 
	1. Check for valid input (fs != NULL)
	2. Lookup fd in refrence table
		a. if fd !exist return <0 failure
	3. Check no active i/o operation is occuring
	4. Remove fd record from refrence table
}

///
/// Populates a dyn_array with information about the files in a directory
///   Array contains up to 15 file_record_t structures
/// \param fs The SP20FS containing the file
/// \param path Absolute path to the directory to inspect
/// \return dyn_array of file records, NULL on error
///
dyn_array_t *fs_get_dir(SP20FS_t *fs, const char *path) {
	1. Check for valid inputs (fs != NULL)
	2. Parse input path to seperate directories 
	3. For each directory from root
		a. Search file index for current directory
		b. If desired dir found follow pointer to it
		c. Otherwise return error
		d. repeat until the desired directory is reached
	4. Allocate dyn_array_t
	5. Itterate through resultant directory 
		a. if file is a file
			add file refrence to dyn_array
		b. else break;
	6. return dyn_array
}