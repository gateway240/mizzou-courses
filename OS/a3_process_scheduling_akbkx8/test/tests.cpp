#include <fcntl.h>
#include <stdio.h>
#include "gtest/gtest.h"
#include <pthread.h>
#include "../include/processing_scheduling.h"

// Using a C library requires extern "C" to prevent function managling
extern "C" 
{
#include <dyn_array.h>
}


#define NUM_PCB 30
#define QUANTUM 5 // Used for Robin Round for process as the run time limit

unsigned int score;
unsigned int total;

class GradeEnvironment : public testing::Environment 
{
    public:
        virtual void SetUp() 
        {
            score = 0;
            total = 160;
        }

        virtual void TearDown()
        {
            ::testing::Test::RecordProperty("points_given", score);
            ::testing::Test::RecordProperty("points_total", total);
            std::cout << "SCORE: " << score << '/' << total << std::endl;
        }
};

// Load Process Control Block Tests
TEST (load_process_control_blocks, nullInputCheck) {
    const char *input_file = NULL;
    dyn_array_t *processControlBlocks = load_process_control_blocks(input_file);
    EXPECT_EQ(NULL,processControlBlocks);
}
TEST (load_process_control_blocks, validInputCheck) {
    const char *input_file = "./src/input_file";
    ProcessControlBlock_t firstPCB = {8,0,0,false};
    dyn_array_t *processControlBlocks = load_process_control_blocks(input_file);
    ProcessControlBlock_t *frontObject = (ProcessControlBlock_t*) dyn_array_front(processControlBlocks);
    EXPECT_EQ(firstPCB.arrival,frontObject->arrival);
    EXPECT_EQ(firstPCB.remaining_burst_time,frontObject->remaining_burst_time);
    EXPECT_EQ(firstPCB.priority,frontObject->priority);
    EXPECT_EQ(firstPCB.started,frontObject->started);
}
// First Come First Serve Tests
TEST (first_come_first_serve, nullInputCheck) {
    ScheduleResult_t *scheduleResult = NULL;
    dyn_array_t *processControlBlocks = NULL;
    bool result = first_come_first_serve(processControlBlocks,scheduleResult);
    EXPECT_EQ(false,result);
}
TEST (first_come_first_serve, validInputCheck) {
    ScheduleResult_t *scheduleResult = new ScheduleResult_t;
    dyn_array_t *processControlBlocks = dyn_array_create(3,sizeof(ProcessControlBlock_t),NULL);
    ProcessControlBlock_t pcb_init[3] = {
            [0] = {12,0,0,false},
            [1] = {18,0,0,false},
            [2] = {6,0,0,false}
    };
    for(int i=2;i>=0;i--){
        dyn_array_push_back(processControlBlocks,&pcb_init[i]);
    }
    bool result = first_come_first_serve(processControlBlocks,scheduleResult);
    EXPECT_EQ(true,result);
    ScheduleResult_t correctAns = {14,26,36};
    EXPECT_EQ(correctAns.average_waiting_time,scheduleResult->average_waiting_time);
    EXPECT_EQ(correctAns.average_turnaround_time,scheduleResult->average_turnaround_time);
    EXPECT_EQ(correctAns.total_run_time,scheduleResult->total_run_time);
    dyn_array_destroy(processControlBlocks);
    delete scheduleResult;
}
// Shortest Job First Tests
TEST (shortest_job_first, nullInputCheck) {
    ScheduleResult_t *scheduleResult = NULL;
    dyn_array_t *processControlBlocks = NULL;
    bool result = shortest_job_first(processControlBlocks,scheduleResult);
    EXPECT_EQ(false,result);
}
TEST (shortest_job_first, validInputCheck) {
    ScheduleResult_t *scheduleResult = new ScheduleResult_t;
    dyn_array_t *processControlBlocks = dyn_array_create(3,sizeof(ProcessControlBlock_t),NULL);
    ProcessControlBlock_t pcb_init[3] = {
            [0] = {12,0,0,false},
            [1] = {18,0,0,false},
            [2] = {6,0,0,false}
    };
    for(int i=2;i>=0;i--){
        dyn_array_push_back(processControlBlocks,&pcb_init[i]);
    }
    bool result = shortest_job_first(processControlBlocks,scheduleResult);
    EXPECT_EQ(true,result);
    ScheduleResult_t correctAns = {8,22,36};
    EXPECT_EQ(correctAns.average_waiting_time,scheduleResult->average_waiting_time);
    EXPECT_EQ(correctAns.average_turnaround_time,scheduleResult->average_turnaround_time);
    EXPECT_EQ(correctAns.total_run_time,scheduleResult->total_run_time);
    dyn_array_destroy(processControlBlocks);
    delete scheduleResult;
}
// Round Robin Tests
TEST (round_robin, nullInputCheck) {
    ScheduleResult_t *scheduleResult = NULL;
    dyn_array_t *processControlBlocks = NULL;
    bool result = round_robin(processControlBlocks,scheduleResult,QUANTUM);
    EXPECT_EQ(false,result);
}
TEST (round_robin, validInputCheck) {
    ScheduleResult_t *scheduleResult = new ScheduleResult_t;
    dyn_array_t *processControlBlocks = dyn_array_create(3,sizeof(ProcessControlBlock_t),NULL);
    ProcessControlBlock_t pcb_init[3] = {
            [0] = {12,0,0,false},
            [1] = {18,0,0,false},
            [2] = {6,0,0,false}
    };
    for(int i=2;i>=0;i--){
        dyn_array_push_back(processControlBlocks,&pcb_init[i]);
    }
    bool result = round_robin(processControlBlocks,scheduleResult,QUANTUM);
    EXPECT_EQ(true,result);
    ScheduleResult_t correctAns = {18.666667,30.666667,36};
    EXPECT_EQ(correctAns.average_waiting_time,scheduleResult->average_waiting_time);
    EXPECT_EQ(correctAns.average_turnaround_time,scheduleResult->average_turnaround_time);
    EXPECT_EQ(correctAns.total_run_time,scheduleResult->total_run_time);
    dyn_array_destroy(processControlBlocks);
    delete scheduleResult;
}
// Shortest Remaining Time First Tests
TEST (shortest_remaining_time_first, nullInputCheck) {
    ScheduleResult_t *scheduleResult = NULL;
    dyn_array_t *processControlBlocks = NULL;
    bool result = shortest_remaining_time_first(processControlBlocks,scheduleResult);
    EXPECT_EQ(false,result);
}
TEST (shortest_remaining_time_first, validInputCheck) {
    ScheduleResult_t *scheduleResult = new ScheduleResult_t;
    dyn_array_t *processControlBlocks = dyn_array_create(3,sizeof(ProcessControlBlock_t),NULL);
    ProcessControlBlock_t pcb_init[3] = {
            [0] = {12,0,0,false},
            [1] = {18,0,0,false},
            [2] = {6,0,0,false}
    };
    for(int i=2;i>=0;i--){
        dyn_array_push_back(processControlBlocks,&pcb_init[i]);
    }
    bool result = shortest_remaining_time_first(processControlBlocks,scheduleResult);
    EXPECT_EQ(true,result);
    ScheduleResult_t correctAns = {10,22,36};
    EXPECT_EQ(correctAns.average_waiting_time,scheduleResult->average_waiting_time);
    EXPECT_EQ(correctAns.average_turnaround_time,scheduleResult->average_turnaround_time);
    EXPECT_EQ(correctAns.total_run_time,scheduleResult->total_run_time);
    dyn_array_destroy(processControlBlocks);
    delete scheduleResult;
}

int main(int argc, char **argv) 
{
    ::testing::InitGoogleTest(&argc, argv);
    ::testing::AddGlobalTestEnvironment(new GradeEnvironment);
    return RUN_ALL_TESTS();
}
