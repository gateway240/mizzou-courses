syms x y z s t
s = 3;
t = 1/3;
eqn1 = x + 2*y+ s*z == 1;
eqn2 = 2*x -2*y -z == 3*t;
eqn3 = 3*x -y +z == 2;
[A,B] = equationsToMatrix([eqn1, eqn2, eqn3], [x, y, z])
combined = [A,B]
rref(combined)
X = linsolve(A,B)