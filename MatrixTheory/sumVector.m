
   function A = sumVector (sum , numRows)
   c = nchoosek(2:sum,numRows-1);
   m = size(c,1);
   A = zeros(m,numRows);
   for ix = 1:m
     A(ix,:) = diff([1,c(ix,:),sum+1]);
   end