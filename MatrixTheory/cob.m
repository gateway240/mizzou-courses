function C = cob(A, B)
% Returns C, which is the change of basis matrix from A to B,
% that is, given basis A and B, we represent B in terms of A.
% Assumes that A and B are square matrices

n = size(A, 1);

% Creates a square matrix full of zeros 
% of the same size as the number of rows of A.
C = zeros(n);

for i=1:n
   C(i, :) = (A\B(:, i))';
end

end