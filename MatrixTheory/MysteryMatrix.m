A = sumVector(9,3); %All possible vectors for first row
B = sumVector(10,3);%All possible vectors for second row
C = sumVector(11,3);%All possible vectors for third row
%Matrix constraints
detVal = 184;
traceVal = 18;
matSum = 30;
triSum = 25;
%Iterate through every possible combination of the above rows
for i = 1:length(A)
    for j = 1:length(B)
        for k = 1:length(C)
            %Create a matrix with the current combination
            Matrix = [A(i,:);B(j,:);C(k,:)];
            %Check if it meets above constrains
            if (det(Matrix) == detVal && trace(Matrix)== traceVal && sum(sum(tril(Matrix))) == triSum)
                %If so print it out
                disp(Matrix);
            end           
        end
    end
end

