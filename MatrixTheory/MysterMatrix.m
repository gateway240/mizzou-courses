A = sumVector(10,3);
B = sumVector(9,3);
%C = sumVector(10,3);
for m = 3:12
   D = sumVector(m,3);
   C = [C;D]; 
end
detVal = 184;
traceVal = 18;
answerCount = 1;
answerMat= zeros(50,3);
for i = 1:length(A)
    for j = 1:length(B)
        for k = 1:length(C)
            Matrix = [A(i,:);B(j,:);C(k,:)];
            if (det(Matrix) == detVal && trace(Matrix)== traceVal)
                disp(Matrix);
            answerMat(answerCount,:)= [i,j,k];
            answerCount = answerCount+1;
            end           
        end
    end
end

