/* Name: Alex Beattie
 * Lab: Lab1
 * Section: CS1050C
 * Pawprint: akbkx8
 * */

#include <stdio.h>
int main(void)

{

    int num1;
    printf("Enter the first number:");
    scanf("%d",&num1);
    while (num1 < 0){

        printf("Enter a non-negative number:");
        scanf("%d",&num1); 
    }

    int num2;
    printf("Enter the second number:");
    scanf("%d",&num2);
    while(num2<0){
        printf("Enter a non-negative number:");
        scanf("%d",&num2);
    }

    printf("ADDITION\n");
    printf("The sum of %d and %d is %d.\n\n",num1,num2,num1+num2);
    printf("MULTIPLICATION\n");
    printf("The product of %d and %d is %d.\n",num1,num2,num1*num2);
    return 0;
}
