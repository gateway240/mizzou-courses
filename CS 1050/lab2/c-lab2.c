/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 1-30-2017 
 * Lab: Lab2
 * Section: CS1050C Monday 3:00 PM
 * */

#include <stdio.h>

int NumCheck(testnum){ //this function tests to see if an integer is positive
    while(testnum<0){ //if the input is not positive
        printf("Enter a non-negative number:"); //prompt the user to enter non-negative number
        scanf("%d",&testnum);
    }
    return testnum;//return the new, valid user input
}

int main(void)

{

    int num1;
    printf("Enter the first number:");
    scanf("%d",&num1);
    num1 = NumCheck(num1); //call NumCheck function above

    int num2;
    printf("Enter the second number:");
    scanf("%d",&num2);
    num2 = NumCheck(num2); //call NumCheck function above
    printf("\n");

    printf("ADDITION\n");
    printf("The sum of %d and %d is %d.\n\n",num1,num2,num1+num2);

    printf("SUBTRACTION\n");
    printf("The difference of %d and %d is %d.\n\n",num1,num2,num1-num2);

    printf("MULTIPLICATION\n");
    printf("The product of %d and %d is %d.\n\n",num1,num2,num1*num2);

    printf("DIVISION\n");
    while (num2 == 0){ // if num2 is zero prompt user for a non-zero value until they enter non-zero value
        printf("Cannot divide by zero!\n");
        printf("Enter a number again:");
        scanf("%d",&num2);
        printf("\n");
    }

    printf("%d divided by %d is %.2f.\n\n",num1,num2,(float)num1/(float)num2);//convert int values to float to allow for decimals in division

    if (num1 > num2){
        printf("%d is the biggest number\n",num1);
    }
    else if (num2 > num1){
        printf("%d is the biggest number\n",num2);
    }
    else 
        printf("The numbers you entered are equivalent\n\n");


    return 0;

}



