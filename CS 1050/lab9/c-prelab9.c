/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 4-3-2017 
 * Lab: Lab9
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define MAX 50

void initialize_array(int *,int);
void print_array(int *,int);
int check_size(int);
float mean(int *,int);

int main(void){
    srand(time(NULL));
    int array[MAX];
    int *p=array;
    int usrsize;
    printf("Enter the size of the input:");
    scanf("%d",&usrsize);
    while(check_size(usrsize)!= 1){
        printf("Invalid input. Enter the size of the array again:");
        scanf("%d",&usrsize);
    }
    initialize_array(p,usrsize);
    printf("\nInput array\n");
    print_array(p,usrsize);
    printf("\n");
    printf("Average is: %.3f\n",mean(p,usrsize));



    return 0;
}

void initialize_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        *(pointer+i) = (rand() % 5)+1;
    }
}
void print_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        printf("%d",*(pointer+i));
    }
}
int check_size(int usrnum){
    return(usrnum>1 && usrnum <50);
}
float mean(int *pointer,int size){
    int i,count = 0;
    for (i=0;i<size;i++){
        count += *(pointer+i);
    }
    return((float)count/(float)size);
}


