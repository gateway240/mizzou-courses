/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 4-3-2017 
 * Lab: Lab9
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define MAX 50

void initialize_array(int *,int);
void print_array(int *,int);
int check_size(int);
float median(int*,int);
float standard_deviation(int*,int);
float mean(int*,int);

int main(void){
    srand(time(NULL));
    int array[MAX];
    int *p=array;
    int usrsize;
    printf("Enter the size of the input:");
    scanf("%d",&usrsize);
    while(check_size(usrsize)!= 1){
        printf("Invalid input. Enter the size of the array again:");
        scanf("%d",&usrsize);
    }
    initialize_array(p,usrsize);
    printf("\nInput array\n");
    print_array(p,usrsize);
    printf("\n");
    printf("Median of the array is %.2f\n",median(p,usrsize));
    printf("Standard deviation is %.2f\n",standard_deviation(p,usrsize));
    return 0;
}

void initialize_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        *(pointer+i) = (rand() % 5)+1;
    }
}
void print_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        printf("%d",*(pointer+i));
    }
}
int check_size(int usrnum){
    return(usrnum>=1 && usrnum <=50);
}
float standard_deviation(int *pointer,int size){
    float sum = 0.0, average, stdDev = 0.0;
    int i;
    for (i=0;i<size;i++){
        sum += *(pointer+i);
    }

    average = mean(pointer,size);
    for(i=0;i<size;i++){
        stdDev += pow((*(pointer+i)- average),2);

    }

    return sqrt(stdDev/(size-1));


}

float median(int *pointer, int size){
    int i,j,temp = 0;
    for (i=0;i<size;i++){
        for(j=i+1;j<size;j++){
            if(*(pointer+i) > *(pointer+j))
            {
                temp = *(pointer+i);
                *(pointer+i)=*(pointer+j);
                *(pointer+j)= temp;
            }
        }
    }
    float median = 0;
    printf("Sorted array\n");
    print_array(pointer,size);
    printf("\n\n");

    if(size % 2 == 1){
        median = *(pointer +((size/2)+1));
    }
    else{
        float num1,num2;
        num1 = *(pointer+(size/2));
        num2= *(pointer+((size/2)+1));
        median = (num1+num2)/2;
    }

    return median;
}
float mean(int *pointer,int size){
    int i,count = 0;
    for (i=0;i<size;i++){
        count += *(pointer+i);
    }
    return((float)count/(float)size);
}


