/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 4-28-2017 
 * Lab: hw3
 * Section: CS1050C Monday 3:00 PM
 * */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <regex.h>
bool load_data(char *, int *, double *,char *,int);
void print_data(int *,double *,char *,int);
int highest_amount(double *,int);
int lowest_amount(double *, int);
float average_amount(double *,int);
bool write_data(char*, int *, double *, char *, int);
int check_palindrome(int);
int clean_data(int *, double *, char *, int*, double*, char*,int);
void sort_data(int *, double *,char *,int);
bool bonusWriteData(char*,int*,double*,char*,int);
int AvgHighLow(double*,float,int,int); 
int main(int argc,char **argv){
    if (argc != 5){
        printf("USAGE ERROR--->ARGUMENT COUNT MUST BE 5\n");
        return 1;
    }
    int usrsize = atoi(*(argv+1)); 
    //pointers that corespond to the fields present in the data file
    int *accountptr;
    double *ammountptr;
    char *designationptr;
    accountptr = malloc(sizeof(int)*usrsize);
    ammountptr = malloc(sizeof(double)*usrsize);
    designationptr = malloc(sizeof(char)*usrsize);
    char *infilename = *(argv+2);
    if (load_data(infilename,accountptr,ammountptr,designationptr,usrsize) == false){
        printf("ERROR LOADING DATA FROM %s\n",infilename);
        return 1;
    }
    print_data(accountptr,ammountptr,designationptr,usrsize);
    int highid = highest_amount(ammountptr,usrsize);
    int lowid =  lowest_amount(ammountptr,usrsize);  
    printf("\nThe TA with ID %d has the maximum amount of %.2f from the original file data\n\n",*(accountptr+highid),*(ammountptr+highid)); 
    printf("The TA with ID %d has the minimum amount of %.2f from the original file data\n\n",*(accountptr+lowid),*(ammountptr+lowid));
    printf("The average across all TA's is: %4.2f\n\n",average_amount(ammountptr,usrsize));
    if(write_data(*(argv+3),accountptr,ammountptr,designationptr,usrsize) == false){
        printf("ERROR WRITING DATA TO %s\n",*(argv+3));
        return 1;
    }
    printf("Data from %s is now written to %s\n\n",infilename,*(argv+3));
    //new pointers to store the cleaned data
    int *CleanAccountPtr;
    double *CleanAmmountPtr;
    char *CleanDesignationPtr;
    CleanAccountPtr = malloc(sizeof(int)*usrsize);
    CleanAmmountPtr = malloc(sizeof(double)*usrsize);
    CleanDesignationPtr = malloc(sizeof(char)*usrsize);
    int CleanSize = clean_data(accountptr,ammountptr,designationptr,CleanAccountPtr,CleanAmmountPtr,CleanDesignationPtr,usrsize);
    printf("\n**********DATA AFTER CLEANING**********\n\n");
    sort_data(CleanAccountPtr,CleanAmmountPtr,CleanDesignationPtr,CleanSize);
    print_data(CleanAccountPtr,CleanAmmountPtr,CleanDesignationPtr,CleanSize);
    int highid2 = highest_amount(CleanAmmountPtr,CleanSize);
    int lowid2 =  lowest_amount(CleanAmmountPtr,CleanSize);  
    printf("\nThe TA with ID %d has the maximum amount of %.2f from the updated file data\n\n",*(CleanAccountPtr+highid2),*(CleanAmmountPtr+highid2)); 
    printf("The TA with ID %d has the minimum amount of %.2f from the updated file data\n\n",*(CleanAccountPtr+lowid2),*(CleanAmmountPtr+lowid2));
    printf("The average across all TA's is: %4.2f\n\n",average_amount(CleanAmmountPtr,CleanSize));
    if(write_data(*(argv+4),CleanAccountPtr,CleanAmmountPtr,CleanDesignationPtr,CleanSize)== false){
        printf("ERROR WRITING DATA TO %s\n",*(argv+4));
        return 1;
    }

    printf("Updated output written to %s\n\n",*(argv+4));
    if(bonusWriteData(*(argv+4),CleanAccountPtr,CleanAmmountPtr,CleanDesignationPtr,CleanSize)== false){
        printf("ERROR WRITING DATA TO %s\n",*(argv+4));
        return 1;
    }

    printf("Bonus appended to %s\n\n",*(argv+4));
    free(accountptr);
    free(ammountptr);
    free(designationptr);
    free(CleanAmmountPtr);
    free(CleanAccountPtr);
    free(CleanDesignationPtr);

    return 0;
}
int AvgHighLow(double *amnt,float avg,int size,int highlow){
    /*Finds the index of the least amount which is greater than or equal to the average value of the ammounts or the greatest amount which is lesser than or equal to the average value of the ammounts.
      last input highlow controls weather function returns least greatest <1> or greatest least <0>
      searches the array to find the element above and below the avarage*/
    int first = 0, last = size-1;
    int middle = (first+last)/2;
    while(first <= last){
        if (*(amnt+middle)==avg)
            return middle;
        else if(*(amnt+middle)<avg){
            first=middle+1;
            if(*(amnt+middle+1)>avg){
                if(highlow)
                    return middle+1;
                else
                    return middle;
            }
        }
        else 
            last = middle -1;
        middle = (first+last)/2;
    }
    return -1;
}

bool bonusWriteData(char*filename,int*acntid,double*amnt,char*designation,int size){//sorts the values by ammount while mantaining the integrity of the seperate arrays. Appends the sorted values to the input file.
    FILE *infile;
    if ((infile = fopen(filename,"a"))== NULL){
        printf("Cannot read %s for opening\n",filename);
        return false;
    }
    int TempId;
    char TempDes;
    double TempAmnt;
    int i,j;
    for(i=0;i<size-1;i++){//bubble sort
        for(j=0;j<size-i-1;j++){

            if (*(amnt+j)>*(amnt+j+1)){//bubble sort for all arrays to ensure values are indexed correctly together
                TempDes = *(designation+j);
                *(designation+j)=*(designation+j+1);
                *(designation+j+1)= TempDes;
                TempAmnt = *(amnt+j);
                *(amnt+j)=*(amnt+j+1);
                *(amnt+j+1)= TempAmnt;
                TempId = *(acntid+j);
                *(acntid+j)=*(acntid+j+1);
                *(acntid+j+1)= TempId;
            }
        }
    }
    fprintf(infile,"\n**********BONUS**********\n\n");
    fprintf(infile,"%9s%10s%15s\n","ACCOUNTS","AMOUNTS","DESIGNATION");
    for(i=0;i<size;i++){
        fprintf(infile,"%6d\t",*(acntid+i));
        fprintf(infile,"%12.2lf\t",*(amnt+i));
        fprintf(infile," %c",*(designation+i));
        fprintf(infile,"\n");
    }
    float avg = average_amount(amnt,size);
    int highid = AvgHighLow(amnt,avg,size,1);
    int lowid = AvgHighLow(amnt,avg,size,0);

    fprintf(infile,"\nThe smallest amount greather than or equal to the average is %.2f by the TA with ID %d with the designation %c\n",*(amnt+highid),*(acntid+highid),*(designation+highid)); 
    fprintf(infile,"\nThe greatest amount lesser than or equal to the average is %.2f by the TA with ID %d with the designation %c\n",*(amnt+lowid),*(acntid+lowid),*(designation+lowid)); 


    fclose(infile);
    return true;
}

void sort_data(int *acntid,double*amnt,char*designation,int size){//sorts the input arrays by Designation while maintaing index integrity of the other arrays
    int i,j;
    char TempDes;
    double TempAmnt;
    int TempId;
    char TempDesVal;
    char TempDesVal2;
    //tmparray = malloc(sizeof(int)*3);

    for(i=0;i<size-1;i++){//bubble sort
        for(j=0;j<size-i-1;j++){
            //assigns the designator value to a temporary variable
            TempDesVal = *(designation+j);
            TempDesVal2 = *(designation+j+1);
            //if either value is an S make it's value for sorting purposes a K because K is between J and M in the ascii table
            if(TempDesVal == 'S')
                TempDesVal = 'K';
            if(TempDesVal2 =='S')
                TempDesVal2 = 'K';
            if (TempDesVal>TempDesVal2){//bubble sort for all arrays to ensure values are indexed correctly together
                TempDes = *(designation+j);
                *(designation+j)=*(designation+j+1);
                *(designation+j+1)= TempDes;
                TempAmnt = *(amnt+j);
                *(amnt+j)=*(amnt+j+1);
                *(amnt+j+1)= TempAmnt;
                TempId = *(acntid+j);
                *(acntid+j)=*(acntid+j+1);
                *(acntid+j+1)= TempId;
            }
        }
    }
}

int clean_data(int* acntid,double*amnt,char*designation,int*newacntid,double*newamnt,char*newdesignation,int size){//checks to see if data matches established parameters and if so loads it into a new array 
    int i,recordcount = 0;
    for (i=0;i<size;i++){//checks valid data entry parameters: ID is Palindrome, Amount between 500 and 5000, and Designation not P
        if(check_palindrome(*(acntid+i))==1){
            if(*(amnt+i)>=500 && *(amnt+i)<=5000.00){
                if(*(designation+i) != 'P'){//inputs values into new array
                    *(newacntid+recordcount) = *(acntid+i);
                    *(newamnt+recordcount) = *(amnt +i);
                    *(newdesignation+recordcount)= *(designation+i);
                    recordcount++;//bumps array count for new array

                }
            }
        }

    }


    return recordcount;
}

bool write_data(char *filename,int* acntid,double *amnt,char *designation,int size){//writes the input arrays to a specified text file
    FILE *infile;
    if ((infile = fopen(filename,"w"))== NULL){
        printf("Cannot read %s for opening\n",filename);
        return false;
    }
    int i;
    fprintf(infile,"%9s%10s%15s\n","ACCOUNTS","AMOUNTS","DESIGNATION");
    for(i=0;i<size;i++){
        fprintf(infile,"%6d\t",*(acntid+i));
        fprintf(infile,"%12.2lf\t",*(amnt+i));
        fprintf(infile," %c",*(designation+i));
        fprintf(infile,"\n");
    }
    fclose(infile);
    return true;
}





int check_palindrome(int innum){//checks input number to see if it is a palindrome
    int temp, remainder, reverse = 0;
    temp = innum;
    while(innum>0){//extracts each digit from the input number and adds it to the begging of a new number and shifts the decimal place appropriatley and divides by ten to eliminte the use number on the next pass. It flips the number around.
        remainder = innum%10;
        reverse = reverse*10 +remainder;
        innum /= 10;
    }
    if (temp == reverse){
        return 1;
    }
    return 0;
}
int highest_amount(double *amnt,int size){//finds the index of the highest value in the input array
    int i,maxindex ,max = -1;
    for(i=0;i<size;i++){
        if(*(amnt+i)>max){
            max = *(amnt+i);
            maxindex = i;
        }
    }
    return maxindex;
}
int lowest_amount(double *amnt,int size){//finds the index of the lowest value in the input array
    int i,minindex ,min = *(amnt);
    for(i=0;i<size;i++){
        if(*(amnt+i)<min){
            min = *(amnt+i);
            minindex = i;
        }
    }
    return minindex;
}
float average_amount(double *amnt,int size){//finds the average of all values in the array
    int i;
    float total=0;
    for(i=0;i<size;i++){
        total += *(amnt+i);
    }
    return total/size;
}
bool load_data(char *filename, int* acntid,double *amnt, char*designation,int size){// loads the values from the input files into the specified arrays
    FILE *infile;
    if ((infile = fopen(filename,"r"))== NULL){
        printf("Cannot read %s for opening\n",filename);
        return false;
    }
    int i;
    for(i=0;i<size;i++){
        fscanf(infile,"%d",(acntid+i));
        fscanf(infile,"%lf",(amnt+i));
        fscanf(infile," %c",(designation+i));

    }
    fclose(infile);
    return true; 
}
void print_data(int* acntid, double *amnt, char*designation,int size){//prints out the specified arrays
    int i;
    printf("%9s%10s%15s\n","ACCOUNTS","AMOUNTS","DESIGNATION");
    for(i=0;i<size;i++){
        printf("%6d\t",*(acntid+i));
        printf("%12.2lf\t",*(amnt+i));
        printf(" %c",*(designation+i));
        printf("\n");
    }

}



