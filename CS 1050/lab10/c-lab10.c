/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 4-10-2017 
 * Lab: Lab10
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
int loadNumbers(char*,int*,int);
void printNumbers(int *,int);
float findAverage(int*,int);
int findMax(int*,int);
int findMin(int*,int);
int printReverse(int*,int);

int main(int argc,char  **argv){
    printf("\n");
    if (argc != 4){//if the number of input arguments is print error and exit the program.
        printf("USAGE ERROR: CHECK THE COMMAND LINE ARGUMENTS\n");
        return 1;
    }
    int *aptr;
    int *aptr2;
    int usrsize= atoi(*(argv+1));//finds the cmdline specified size value and converts it to an integer
    aptr = malloc(sizeof(int)*usrsize);//allocates the user specified amount of space in memory for the first array
    aptr2 =malloc(sizeof(int)*usrsize);//allocates the user speceifed amount of space in memory for the second array
    int *usrptr;
    int *usrptr2;
    usrptr = aptr;//establishes usrptr to the adress of the malloc-d block
    char *fileval = *(argv+2);//returns file name  
    if(loadNumbers(fileval,usrptr,usrsize)==0){
        printf("FILE %s CANNOT BE READ\n",fileval);
        return 2;
    }

    printf("The numbers retrieved from the file %s are:\n",fileval);
    printNumbers(usrptr,usrsize);
    printf("\nMean of array: %.2f\n",findAverage(usrptr,usrsize));
    printf("Minimum number in array: %d\n",findMin(usrptr,usrsize));
    printf("Maximum number in array: %d\n\n",findMax(usrptr,usrsize));

    usrptr2 = aptr2;//establishes usrptr2 to the adress of the malloc-d block
    char *fileval2 = *(argv+3);//returns file name  
    if(loadNumbers(fileval2,usrptr2,usrsize)==0){
        printf("FILE %s CANNOT BE READ\n",fileval);
        return 2;
    }

    printf("The numbers retrieved from the file %s are:\n",fileval2);
    printNumbers(usrptr2,usrsize);
    printf("\nMean of array: %.2f\n",findAverage(usrptr2,usrsize));
    printf("Minimum number in array: %d\n",findMin(usrptr2,usrsize));
    printf("Maximum number in array: %d\n\n",findMax(usrptr2,usrsize));

    printf("*****BONUS*****\n\n");
    printf("The numbers reversed from the file %s are:\n",fileval);
    printReverse(usrptr,usrsize);
    printf("The numbers reversed from the file %s are:\n",fileval2);
    printReverse(usrptr2,usrsize);
    free(aptr);
    free(aptr2);

    return 0;
}
int printReverse(int *usrarray,int size){//prints the values from the file in reverse
    int i;
    for (i=size-1;i>=0;i--){
        printf("%d ",*(usrarray+i));
    }
    printf("\n");
    return 0;
}

int loadNumbers(char *filename, int *array, int size){//opens a file outside the program and loads the values from it to an array within the program. The ammount of numbers is give by the "size" variable
    int i;
    FILE *inFile;
    if((inFile = fopen(filename,"r"))==NULL){
        return 0;}



    for(i=0;i<size;i++){
        fscanf(inFile,"%d",(array+i));
    }
    fclose(inFile);
    return 1;

}

void printNumbers(int *pointer,int size){//prints out each element of an array
    int i;
    for(i=0;i<size;i++){
        printf("%d ",*(pointer+i));
    }
}
float findAverage(int *pointer,int size){//finds and returns the average value of all elements in an array
    int i,count = 0;
    for (i=0;i<size;i++){
        count += *(pointer+i);
    }
    return((float)count/(float)size);
}
int findMax(int *pointer,int size){//finds and returns the maximum value in an input array
    int i, maxnum=0;
    for(i=0;i<size;i++){
        if(*(pointer+i)>maxnum){
            maxnum = *(pointer+i);

        }
    }
    return maxnum;

}
int findMin(int *pointer,int size){//finds and returns the minimum value in an input array

    int i, minnum=5;
    for(i=0;i<size;i++){
        if(*(pointer+i)<minnum){
            minnum = *(pointer+i);

        }
    }
    return minnum;

}
