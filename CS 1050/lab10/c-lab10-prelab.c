/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 4-3-2017 
 * Lab: Lab9
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
void initialize_array(int*,int);
void print_array(int *,int);
float mean(int*,int);
int max(int*,int);
float mean(int*,int);
int max(int*,int);
int min(int*,int);

int main(void){
    srand(time(NULL));
    int *aptr;
    int usrsize=10;
    aptr = malloc(sizeof(int)*usrsize);
    int *usrptr;
    usrptr = aptr;
    initialize_array(usrptr,usrsize);
    printf("Initial array:");
    print_array(usrptr,usrsize);
    printf("\nMean of array: %.2f\n",mean(usrptr,usrsize));
    printf("Maximum number in array: %d\n",max(usrptr,usrsize));
    printf("Minimum number in array: %d\n",min(usrptr,usrsize));

    free(aptr);


    return 0;
}

void initialize_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        *(pointer+i) = (rand() % 5)+1;
    }
}
void print_array(int *pointer,int size){
    int i;
    for(i=0;i<size;i++){
        printf("%d",*(pointer+i));
    }
}
float mean(int *pointer,int size){
    int i,count = 0;
    for (i=0;i<size;i++){
        count += *(pointer+i);
    }
    return((float)count/(float)size);
}
int max(int *pointer,int size){
    int i, maxnum=0;
    for(i=0;i<size;i++){
        if(*(pointer+i)>maxnum){
            maxnum = *(pointer+i);

        }
    }
    return maxnum;

}
int min(int *pointer,int size){

    int i, minnum=5;
    for(i=0;i<size;i++){
        if(*(pointer+i)<minnum){
            minnum = *(pointer+i);

        }
    }
    return minnum;

}
