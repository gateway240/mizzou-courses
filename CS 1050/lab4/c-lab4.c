/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 2-20-2017 
 * Lab: Lab4
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>

int errorCheck(int);

int checkEven(int);

int checkPrime(int);

int addDigits(int);

void printMizzou(int);

int smallestLargestSum(int);

int numDigits(int);

int main(void){
    int usrnum;
    printf("Enter the first positive number: ");
    scanf("%d",&usrnum);
    printf("\n");
    while (errorCheck(usrnum)==0){
        printf("Error! Enter a positive number only: ");
        scanf("%d",&usrnum);
        errorCheck(usrnum);
    }

    if (checkEven(usrnum)==1){
        printf("\n%d is an Even number\n",usrnum);}
    else {
        printf("\n%d is an Odd number\n",usrnum);}
    if (checkPrime(usrnum)==1){
        printf("%d is a Prime number\n",usrnum);
    }
    else{
        printf("%d is not a Prime number \n",usrnum);
    }

    printf("The sum of digits in %d is %d\n\n",usrnum,addDigits(usrnum));

    int secondnum;
    printf("Enter the second positive number: ");
    scanf("%d",&secondnum);

    while (errorCheck(secondnum)==0){
        printf("Error! Enter a positive number only: ");
        scanf("%d",&secondnum);
        errorCheck(secondnum);
    }
    printf("\nCalling the mizzou function, the output is:\n");
    printMizzou(secondnum);

    printf("\n*****BONUS*****");
    printf("\nThe sum of the smallest and largest digit in %d is %d\n\n",usrnum,smallestLargestSum(usrnum));
    return 0;
}

int errorCheck(num){

    return (num>0);
}
int checkEven(num){
    int evencheck;
    evencheck = (num%2==0) ? 1:0;
    return evencheck;
}
int checkPrime(num){//uses a for loop to try and divide the input by every integer up to the number/2 because then it would check the same set. If it finds an integer that divides the original number it breaks out and will return 0 indicating prime. 
    int counter,flag = 0;
    for (counter=2;counter<=num/2; ++counter){
        if (num%counter == 0){
            flag = 1;
            break; 
        }

    }
    if (num == 1)
        return 0;
    if (flag == 1)
        return 0;
    else 
        return 1;
}

int addDigits(num){//finds the remainder of dividing the number by 10 to isolate one digit and adds it to the total. Then divides the number by 10 to eliminate the digit that was just used and repeats until it reaches 1.
    int numout =0;
    do{
        numout += num%10;
        num /= 10;
    }while(num>=1);
    return numout;
}
void printMizzou(num){//Iterates through every value from 1 to the input number. Prints MIZZOU if a number is divisible by 3 and 5. Prints MIZ if a number is divisble by 3. Prints ZOU if a number is divisible by 5. Otherwise it prints the number.
    int i;
    for (i=1;i<=num;i++){
        if (i%3==0 && i%5==0){
            printf("MIZZOU ");

        }

        else if (i%3==0){
            printf("MIZ ");
        }

        else if (i%5==0){
            printf("ZOU ");
        }
        else
            printf("%d ",i);

    }
    printf("\n");
}
int numDigits(num){//returns the number of digits in an integer
    int i = 0;
    while(num != 0){
        num /= 10;
        ++i;

    }
    return i;

}
int smallestLargestSum(num){//stores each value of the input number in an array and then determines the maximum and minimum value in the array
    int digits = numDigits(num);
    int numarray[digits];//creates an array the size of the amount of digits in the number
    int i;
    for(i = 0; num>=1; i++ ){//stores each digit in the array
        numarray[i] = num%10;
        num /= 10; 
    }
    int minimum = numarray[0];
    int maximum = numarray[0];
    for(i = 1; i < digits; i++){//finds the maximum and minimum values of the array
        if (numarray[i]<minimum){
            minimum = numarray[i];
        }
        else if (numarray[i]>maximum){
            maximum = numarray[i];
        }

    }
    int sum;

    sum = minimum+maximum;
    return sum;
}



