/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 2-20-2017 
 * Lab: Lab4
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>

int checkError(int);

int checkEven(int);

int main(void){

    int usrnum;
    printf("Enter a positive number: ");
    scanf("%d",&usrnum);
    printf("\n");
    while (checkError(usrnum)==0){
        printf("Error! Enter a positive number only: ");
        scanf("%d",&usrnum);
        checkError(usrnum);
    }

    if (checkEven(usrnum)==1){
        printf("\n%d is an Even number\n",usrnum);}
    else {
        printf("\n%d is an Odd number\n",usrnum);}

    return 0;
}

int checkError(num){

    return (num>0);
}
int checkEven(num){
    int evencheck;
    evencheck = num % 2;
    if (evencheck == 0)
        return 1;
    else
        return 0;
}

