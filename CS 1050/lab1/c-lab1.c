/* Name: Alex Beattie
 * Lab: Lab1
 * Section: CS1050C
 * Pawprint: akbkx8
 * */

#include <stdio.h>
int main(void)

{
    printf("Hello World!\n");


    int a_distance;
    printf("Enter the distance:");
    scanf("%d",&a_distance);
    while (a_distance < 0){

        printf("Distance should be a positive number");
        printf("Enter the Distance again:");
        scanf("%d",&a_distance); 
    }
    return 0;
}
