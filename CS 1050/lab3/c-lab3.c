/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 2-13-2017 
 * Lab: Lab3
 * Section: CS1050C Monday 3:00 PM
 * */

#include <stdio.h>
#include <math.h>
float power(float x, int y){

    float temp ;
    if (y == 0)
        return 1;
    temp = power(x,y/2);
    if(y%2 ==0)
        return temp*temp;
    else
    {
        if(y>0)
            return x*temp*temp;
        else
            return (temp*temp)/x;
    }
}



float powcalc(){
    printf("*****Bonus part*****\n\n");
    float usrnum;
    int exp;
    float  output;
    printf("Enter a number: ");
    scanf("%f",&usrnum);
    printf("\n");
    printf("Enter an exponent: ");
    scanf("%d",&exp);

    output = power(usrnum,exp);

    getchar();
    if (output<1)
        printf("pow(%.6f, %d) = %.5f",usrnum,exp,output);
    else
        printf("pow(%.6f, %d) = %.2f",usrnum,exp,output);


    printf("\n");
    return 0;

}


int selectaccount(){//function prompting user to select their account type, returns use value
    int usrchoice;
    printf("Bank account type\n\n");
    printf("(1) Checking \n");
    printf("(2) Savings\n");
    printf("(3) Fixed Deposit\n");
    printf("Please make a selection: ");
    scanf("%d",&usrchoice);
    printf("\n");
    return usrchoice;
}
int selectyear(){// function prompting user to select the year, error checks to ensure input is between 1-10 then returns selection
    int usrchoice;
    printf("Enter a year:");
    scanf("%d",&usrchoice);
    while(usrchoice<1 || usrchoice>11){
        printf("Incorrect value, year should be between 1-10\n");
        printf("Enter a year:");
        scanf("%d",&usrchoice);
    }
    printf("\n");
    return usrchoice;
}

int posnum(){//function prompting user to enter their target amount, checks to make sure value is positive and returns it
    int usrnum;
    printf("Enter the amount:");
    scanf("%d",&usrnum);
    printf("\n");
    while(usrnum<=0){
        printf("Incorrect value, enter the value again:\n");
        printf("Enter the amount again: ");
        scanf("%d",&usrnum); 
    }
    printf("\n");
    return usrnum;
}


int main (void){
    int accountchoice;
    int yearchoice;
    int amount;
    float amountci;
    float totalintrest;
    float rate1=5 ;
    float rate2=10 ;
    float rate3=15 ;
    accountchoice = selectaccount();// calls selectaccount function to determine user selection
    while (accountchoice < 1 || accountchoice > 3){//error checks user input to ensure its between 1 and 3
        printf("Incorrect choice, please make a choice again\n\n");//prints error message
        accountchoice = selectaccount();//calls select account function again
    }
    yearchoice = selectyear();//calls selectyear function to determine users year selection
    amount = posnum();//calls posnum function to return users amount selection
    switch (accountchoice){//preforms intrest rate calculations based on user choice and outputs the result
        case 1:
            amountci = amount*pow((1+(rate1/100)),yearchoice);
            totalintrest= amountci-(float)amount;
            printf("Total amount after %d years is $%.2f\n",yearchoice,amountci);
            printf("Total intrest earned on the amount $%d with the rate %.2f%% is $%.2f\n",amount,rate1,totalintrest); 
            break;

        case 2:
            amountci = amount*pow((1+(rate3/100)),yearchoice);
            totalintrest= amountci-(float)amount;
            printf("Total amount after %d years is $%.2f\n",yearchoice,amountci);
            printf("Total intrest earned on the amount $%d with the rate %.2f%% is $%.2f\n",amount,rate2,totalintrest); 
            break;
        case 3:
            amountci = amount*pow((1+(rate3/100)),yearchoice);
            totalintrest= amountci-(float)amount;
            printf("Total amount after %d years is $%.2f\n",yearchoice,amountci);
            printf("Total intrest earned on the amount $%d with the rate %.2f%% is $%.2f\n",amount,rate3,totalintrest); 
            break;
        case '\n':
        case '\t':
        case ' ':
            break;
        default:
            printf("No valid selection made");
    }

    powcalc();
    return 0;

}

