#include <stdio.h>
#include <math.h>

int squarecalc(length){
    int area;
    area = pow(length,2);
    return area;
}

int cubecalc(length){
    int area;
    area = 6*pow(length,2);
    return area;
}

float circlecalc(radius){
    float area;
    area = 3.14159*pow(radius,2);
    return area;
}

int selectshape(){
    int usrchoice;
    printf("Area calculation\n");
    printf("(1) Square\n");
    printf("(2) Cube \n");
    printf("(3) Circle \n");
    printf("Please make a selection: ");
    scanf("%d",&usrchoice);
    return usrchoice;
}
int posnum(){
    int usrnum;
    printf("Enter a positive number: ");
    scanf("%d",&usrnum);
    printf("\n");
    while(usrnum<=0){
        printf("Number can't be negative or zero\n");
        printf("Enter a positive number: ");
        scanf("%d",&usrnum); 
    }
    return usrnum;
}


int main (void){
    int userchoice;
    int arearect;
    float arearound;
    userchoice = selectshape();
    int num; 
    while (userchoice < 1 || userchoice > 3){
        printf("Incorrect choice\n\n");
        userchoice = selectshape();
    }
    switch (userchoice){
        case 1:
            num = posnum();
            arearect = squarecalc(num);
            printf("Length of the side of the square is %d\n",num);
            printf("Area of the square is %d\n",arearect);
            break;
        case 2:
            num = posnum();
            arearect = cubecalc(num);
            printf("Length of the side of the cube is %d\n",num);
            printf("Area of the cube is %d\n",arearect);
            break;
        case 3:
            num = posnum();
            arearound = circlecalc(num);
            printf("Radius of the circle is %d\n",num);
            printf("Area of the circle is %.2f\n",arearound);
            break;
        default:
            printf("No valid selection made");
    }


    return 0;

}

