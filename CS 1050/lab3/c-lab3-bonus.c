#include <stdio.h>

float power(float x, int y){

    float temp ;
    if (y == 0)
        return 1;
    temp = power(x,y/2);
    if(y%2 ==0)
        return temp*temp;
    else
    {
        if(y>0)
            return x*temp*temp;
        else
            return (temp*temp)/x;
    }
}



float powcalc(){
    printf("*****Bonus part*****\n\n");
    float usrnum;
    int exp;
    float  output;
    printf("Enter a number: ");
    scanf("%f",&usrnum);
    printf("\n");
    printf("Enter an exponent: ");
    scanf("%d",&exp);

    output = power(usrnum,exp);

    getchar();
    if (output<1)
        printf("pow(%.6f, %d) = %.5f",usrnum,exp,output);
    else
        printf("pow(%.6f, %d) = %.2f",usrnum,exp,output);


    printf("\n");
    return 0;

}
int main(void)
{


    powcalc();
    return 0;
}
