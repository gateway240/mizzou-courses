/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 2-27-2017 
 * Lab: Lab5
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void drinkMenu(void);
void displayMenu(void);
void displayRecipt(void);
float calculateTax(float);
int calculateRandomDiscount(void);
int errorCheck(int);
float drinkcost(int);
float foodcost(int);
int game (int,int);
int gameerror(int);
int main(void){
    srand(time(NULL));
    drinkMenu();
    printf("Enter an option: ");
    int usrnum;
    scanf("%d",&usrnum);
    while(errorCheck(usrnum)!=1){
        printf("Not a valid option! Please enter again:");
        scanf("%d",&usrnum);
    }

    displayMenu();
    printf("Enter an option: ");
    int usrnum2;
    scanf("%d",&usrnum2);
    while(errorCheck(usrnum2)!=1){
        printf("Not a valid option! Please enter again:");
        scanf("%d",&usrnum2);
    }
    printf("\nThank you for your order! It will be made shortly!\n");
    float subtotal;
    float taxes;
    int discount;
    float subtotaldiscount;
    subtotal = drinkcost(usrnum)+foodcost(usrnum2); 
    taxes = calculateTax(subtotal);
    discount = calculateRandomDiscount();
    subtotaldiscount = subtotal*(1-(((float)discount)/100));
    printf("Subtotal: %.2f\n",subtotal);
    printf("Taxes: %.2f\n",taxes);
    printf("You get a random discount of %d%% on this meal\n",discount);
    printf("Total: %.2f\n",taxes+subtotaldiscount);
    displayRecipt();
    int gamenum;
    int randgamenum;
    int result;
    randgamenum = rand()%25 + 1;
    printf("\n***** BONUS! *****\n\n");
    printf("Let's play a game! Try to guess what number I am thinking.\n");
    printf("Enter in a number between 1-25: ");
    scanf("%d",&gamenum);
    while(gameerror(gamenum)!= 1){
        printf("Please try again: ");
        scanf("%d",&gamenum);
    }
    result = game(gamenum,randgamenum);
    while (result != 2){
        if (result == 1){
            printf("Your number is too high. Enter again:");
            scanf("%d",&gamenum);
        }
        else if (result == 0){
            printf("Your number is too low. Enter again:");
            scanf("%d",&gamenum);
        }
        result = game(gamenum,randgamenum);
    }


    printf("\nCongratulations!!! You won the game !!!\n");
    printf("Your number %d matched %d!\n",gamenum,randgamenum);
    return 0;

}
int game(int usrnum, int randnum){
    if (usrnum < randnum){
        return 0;
    }
    else if (usrnum > randnum){
        return 1;
    }

    else if (usrnum == randnum){
        return 2;
    }
    else 
        return 999;
}
int gameerror(int usrnum){
    return (usrnum>=1 && usrnum<=25);
}
void drinkMenu(void){
    printf("What would you like to drink?\n");
    printf("1. Water: free\n");
    printf("2. Coke: $1.00\n");
    printf("3. Lemonade: $1.50\n");
    printf("4. Chocolate Milk: $1.75\n\n");
}
void displayMenu(void){
    printf("1. Cheeseburger: $4.50\n");
    printf("2. Hotdog: $3.00\n");
    printf("3. Chicken Strips: $5.00\n");
    printf("4. Fries and Burger Combo: $7.20\n\n");
}
float drinkcost(int choice){
    switch(choice){
        case 1 :
            return (0);
            break;
        case 2 :
            return(1.00);
            break;
        case 3 :
            return(1.50);
            break;
        case 4 :
            return(1.75);
            break;
        default :
            return(0);
    }
}
float foodcost(int choice){
    switch(choice){
        case 1 :
            return (4.50);
            break;
        case 2 :
            return(3.00);
            break;
        case 3 :
            return(5.00);
            break;
        case 4 :
            return(7.20);
            break;
        default :
            return(0);
    }
}

float calculateTax(float cost){
    float base;
    base = 0.5;
    if (cost <= 4){
        return (cost*0.1 + base);
    }
    else if (cost > 4){
        return (cost*0.25 +base);
    }
    else
        return 0;
}

int errorCheck(int num){
    return((num<=4) && (num>=1));
}
int calculateRandomDiscount(void){
    return(rand()%25 + 1);
}
void displayRecipt(void){
    int randnum;
    randnum = rand()%100000000;
    printf("Your recipt number is: #%d\n",randnum);
}


