/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 2-27-2017 
 * Lab: Lab5
 * Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void drinkMenu(void);

int errorCheck(int);

int calculateRandomNumber(void);

int main(void){
    drinkMenu();
    printf("Enter an option: ");
    int usrnum;
    scanf("%d",&usrnum);
    while(errorCheck(usrnum)!=1){
        printf("Not a valid option! Please enter again:");
        scanf("%d",&usrnum);
    }
    printf("\n");
    printf("The random number is %d\n",calculateRandomNumber());

    return 0;

}

void drinkMenu(void){
    printf("What would you like to drink?\n");
    printf("1. Water: free\n");
    printf("2. Coke: $1.00\n");
    printf("3. Lemonade: $1.50\n");
    printf("4. Chocalte Milk: $1.75\n\n");
}

int errorCheck(num){
    return((num<=4) && (num>=1));
}
int calculateRandomNumber(void){

    srand(time(NULL));
    return(rand()%26);
}


