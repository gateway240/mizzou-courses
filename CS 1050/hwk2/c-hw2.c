/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 3-7-2017 
 * Homework 2
 * Lab Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 13
//#define DEBUG 1
void display_menu();
int error_check(int);
void shuffle(int[]);
int draw_card(int[]);
int lucky_seven(int);
int high_card(int,int);
double calculate_winnings(int, int, double);
int match_card(int,int);
void printarray(int[]);
int match_card_draw(int[][SIZE],int,int);
int main(void){
    srand(time(NULL));
    printf("\nWelcome to the Guilliams Casino!\n\n");
    int usrnum;
    int cards[SIZE] = {};
    shuffle(cards);
    float usrmoney = 100;
    double usrbet = 0;
    int i,j,quit = 0;

    int deck[4][SIZE] = {};
    int row = 4; 
    for (i=0;i<row;i++){
        for(j=0;j<SIZE;j++){
            deck[i][j]=j+1;
        }
    }

    while ((quit!=1) &&(usrmoney>0)){
        display_menu();
        scanf("%d",&usrnum);
        while(error_check(usrnum) != 1){
            printf("\n\nInvalid Choice; Try again\n\n");
            display_menu();
            scanf("%d",&usrnum);
        }

        if(usrnum ==1){
            printf("How much do you want to bet? $");
            scanf("%lf",&usrbet);
            while((usrbet < 0) || (usrbet>usrmoney)){
                printf("You can't bet negative or more than you have. You have $%.2f. Try again: $",usrmoney);
                scanf("%lf",&usrbet);
            }
            int win, usrcard;
            usrcard = draw_card(cards);
            win = lucky_seven(usrcard);
            if (win == 1){
                printf("Congratulations!!! You drew %d.\n",usrcard);
                printf("You won $%.2f\n",usrbet);
                usrmoney += calculate_winnings(usrnum,win,usrbet);
            }
            else{
                printf("You drew %d.\n",usrcard);
                printf("You lost $%.2f. Too bad\n",usrbet);
                usrmoney += calculate_winnings(usrnum,win,usrbet);
            }

            printf("\n\nYour total money is $%.2f.\n\n\n",usrmoney);

        }
        else if (usrnum==2){
            printf("How much do you want to bet?$");
            scanf("%lf",&usrbet);
            while((usrbet < 0) || (usrbet>usrmoney)){
                printf("You can't bet negative or more than you have. You have $%.2f. Try again: $", usrmoney);
                scanf("%lf",&usrbet);
            }
            //int win,usrcard,dealercard;
            int dealercard,usrcard2,win;
            usrcard2 = draw_card(cards);
#ifdef DEBUG
            printarray(cards);
#endif
            dealercard = draw_card(cards);
            win = high_card(usrcard2,dealercard); 
            if (win == 1){
                printf("Congratulations!!! You drew %d. The dealer drew %d\n",usrcard2,dealercard);
                printf("You won $%.2f\n",usrbet);
                usrmoney += calculate_winnings(usrnum,win,usrbet);
            }
            else{
                printf("You drew %d. The dealer drew %d\n",usrcard2,dealercard);
                printf("You lost $%.2f. Too bad\n",usrbet);
                usrmoney += calculate_winnings(usrnum,win,usrbet);
            }


            printf("\n\nYour total money is $%.2f.\n\n\n",usrmoney);
        }
        else{
            printf("\nBONUS\n");
            printf("Wait! Before you go, do you want a chance to double your money???\n");
            printf("1)Yes\n");
            printf("2)No\n");
            scanf("%d",&usrnum);
            while(usrnum >2 || usrnum <1){
                printf("Invalid Answer. Please enter a valid choice: ");
                scanf("%d",&usrnum);
            }
            if (usrnum ==1){
                printf("\n\nMatch Card\n");
                int matchcard1,matchcard2;
                matchcard1 = match_card_draw(deck,4,SIZE);
                matchcard2 = match_card_draw(deck,4,SIZE);
                printf("You drew %d. The dealer drew %d.\n\n",matchcard1,matchcard2);
#ifdef DEBUG
                for (i=0;i<row;i++){
                    printarray(deck[i]);
                }
#endif
                if(match_card(matchcard1,matchcard2)!=1){
                    usrmoney = 0;
                    printf("\nYou lost everything!!!\nYour total is now $%.2f\nThanks for playing!\n\n",usrmoney);
                    return 0;
                }

                else{
                    usrmoney *= 2;
                    printf("Congratulations!!! Your total is now $%.2f\nThanks for playing!\n\n",usrmoney);
                    return 0;
                }

            }

            else {
                printf("Bye!\n\n");
                return 0;

            }

        }

    }
    printf("You have been thrown out of the casino.\nBe more careful with you money next time.\n\n");
    return 0;
}

int match_card_draw(int a[][SIZE],int row,int column){//draws a random card from the array for the match card game
    int card,rrow,rcolumn ;
    rrow = rand()%row;
    rcolumn = rand()%column;
    while (a[rrow][rcolumn]==-1){
        rrow=rand()%row;
        rcolumn=rand()%row;
    }
    if(a[rrow][rcolumn]!=-1){
        card = a[rrow][rcolumn];
        a[rrow][rcolumn] = -1;
        return card;
    }
    return 0;
}

int match_card(int card1, int card2){//returns whether the two cards are equal
    return(card1 == card2);
}

double calculate_winnings(int game, int win, double bet){//calculates winnings based on values in winning table
    switch(game){
        case 1:
            if (win == 1)
                return(bet*7);
            else 
                return(-bet);
        case 2:
            if (win == 1)
                return bet; 
            else 
                return -bet;
        default:
            return 0;
    }
}

void display_menu(void){
    printf("Which game do you want to play?\n");
    printf("1)Lucky Seven\n");
    printf("2)High Card\n");
    printf("3)Exit\n");
}
int error_check(int num){
    return ((num >= 1) && (num <4));
}

void shuffle(int a[]){//shuffles the deck of cards randomly
    int i;
    for (i=0;i<SIZE;i++){
        a[i] = i+1;
    }

    for(i=0;i<SIZE;i++){
        int j = rand() % (SIZE);
        int t = a[j];
        a[j] = a[i];
        a[i] = t;
    }
}
int draw_card(int a[]){//picks a card that hasen't already been taken
    int i,card;
    for (i=0;i<SIZE;i++){
        if (a[i]!=-1){
            card = a[i];
            a[i] = -1;
            return card;
        }
    }
    return 0;
}

int lucky_seven(int card){
    return (card == 7);
}
int high_card(int usrcard, int dealercard){
    return (usrcard>dealercard);
}

void printarray(int a[]){
    int i;
    for (i=0;i<SIZE;i++){
        printf("%d ",a[i]);
    }
    printf("\n");
}

