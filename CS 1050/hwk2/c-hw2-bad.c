/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 3-7-2017 
 * Homework 2
 * Lab Section: CS1050C Monday 3:00 PM
 * */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 13
#define DEBUG 1
void display_menu();
int error_check(int);
void shuffle(int[]);
int draw_card(int[]);
int lucky_seven(int);
int high_card(int,int);
double calculate_winnings(int, int, double);
int match_card(int,int);
void printarray(int[]);
int lucky_seven_print(int);
int main(void){
    srand(time(NULL));
    printf("\nWelcome to the Guilliams Casino!\n\n");
    int usrnum;
    int cards[SIZE] = {};
    shuffle(cards);
    float usrmoney = 100;
    float usrbet = 0;
    int sentinel = 0;
    while ((usrmoney >= 0)|| (sentinel != 0)){
        display_menu();
        scanf("%d",&usrnum);
        while(error_check(usrnum) != 1){
            printf("\n\nInvalid Choice; Try again\n\n");
            display_menu();
            scanf("%d",&usrnum);
        }


        switch (usrnum){
            case 1:
                {
                    printf("How much do you want to bet? $");
                    scanf("%f",&usrbet);
                    while((usrbet < 0) || (usrbet>usrmoney)){
                        printf("You can't bet negative or more than you have. You have $%.2f. Try again: ",usrmoney);
                        scanf("%f",&usrbet);
                    }
                    int win, usrcard;
                    usrcard = draw_card(cards);
#ifdef DEBUG
                    printarray(cards);
#endif
                    win = lucky_seven(usrcard);
                    if (win == 1){
                        printf("Congratulations!!! You drew %d.\n",usrcard);
                        printf("You won $%.2f\n",usrbet);
                        usrmoney += usrbet;
                    }
                    else{
                        printf("You drew %d.\n",usrcard);
                        printf("You lost $%.2f. Too bad\n",usrbet);
                        usrmoney -= usrbet;
                    }
                    printf("\n\nYour total money is $%.2f.\n",usrmoney);

                    return usrmoney;

                    break;}
            case 2:{
                       printf("How much do you want to bet?$");
                       scanf("%f",&usrbet);
                       while((usrbet < 0) || (usrbet>usrmoney)){
                           printf("You can't bet negative or more than you have. You have $%.2f. Try again: ", usrmoney);
                           scanf("%f",&usrbet);
                       }
                       //int win,usrcard,dealercard;
                       int dealercard,usrcard2,win2;
                       usrcard2 = draw_card(cards);
#ifdef DEBUG
                       printarray(cards);
#endif
                       dealercard = draw_card(cards);
                       win2 = high_card(usrcard2,dealercard); 
                       if (win2 == 1){
                           printf("Congratulations!!! You drew %d. The dealer drew %d\n",usrcard2,dealercard);
                           printf("You won $%.2f\n",usrbet);
                           usrmoney += usrbet;
                       }
                       else{
                           printf("You drew %d. The dealer drew %d\n",usrcard2,dealercard);
                           printf("You lost $%.2f. Too bad\n",usrbet);
                           usrmoney -= usrbet;
                       }

                       printf("\n\nYour total money is $%.2f.\n",usrmoney);
                       return usrmoney;
                       break;}
            case 3:
                   sentinel = 1;
                   printf("Exiting\n");
                   return sentinel;
                   break;
            default:
                   sentinel = 1;
                   printf("ERROR Exiting\n");
                   return sentinel;


                   break;
        }
    }

#ifdef DEBUG
    printarray(cards);
#endif

    return 0;
}
//int lucky_seven_print(int 
void display_menu(void){
    printf("Which game do you want to play?\n");
    printf("1)Lucky Seven\n");
    printf("2)High Card\n");
    printf("3)Exit\n");
}
int error_check(int num){
    return ((num >= 1) && (num <4));
}

void shuffle(int a[]){
    int i;
    for (i=0;i<SIZE;i++){
        a[i] = i+1;
    }

    for(i=0;i<SIZE;i++){
        int j = rand() % (SIZE);
        int t = a[j];
        a[j] = a[i];
        a[i] = t;
    }
}
int draw_card(int a[]){
    int i,card;
    for (i=0;i<SIZE;i++){
        if (a[i]!=-1){
            card = a[i];
            a[i] = -1;
            return card;
        }
    }
    return 0;
}

int lucky_seven(int card){
    return (card == 7);
}
int high_card(int usrcard, int dealercard){
    return (usrcard>dealercard);
}

void printarray(int a[]){
    int i;
    for (i=0;i<SIZE;i++){
        printf("%d ",a[i]);
    }
    printf("\n");
}

