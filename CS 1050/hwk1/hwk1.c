#include <stdio.h>

float rescalc(){ //function that calculates the users cost if they choose residential

    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    }
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*7.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*10;}
    else if (usrinput <= 1500){
        usrcost = usrinput*13.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*15;}

    connectioncharge = 25;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;
}
float comcalc(){//function that calculates the users cost if they choose commercial
    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    }
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*10.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*14;}
    else if (usrinput <= 1500){
        usrcost = usrinput*17.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*20;}

    connectioncharge = 90;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;


}

float indcalc(){//function that claculates the user cost if they choose industrial
    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    }
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*36.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*40;}
    else if (usrinput <= 1500){
        usrcost = usrinput*45.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*50;}

    connectioncharge = 850;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;



}
int choosetype(){
    int selection;
    printf("1. Residential\n2. Commerical\n3. Industrial\n\n");
    printf("Choose the type of connection: ");
    scanf("%d",&selection);
    printf("\n\n");
    while ((selection < 1) | (selection >3)){
        printf("Invalid Choice! Please enter a valid choice\n\n");
        printf("1. Residential\n2. Commerical\n3. Industrial\n\n");
        printf("Choose the type of connection: ");
        scanf("%d",&selection);
        printf("\n\n");
    }


    return selection;
}




int main(void){
    printf("***** ELECTRICITY BILL CALCULATOR *****\n\n\n");
    int sentinal;
    int counter;
    int selection;
    float totalcost;
    totalcost = 0;
    counter = 0;
    sentinal = 1;
    while (sentinal != 0){
        selection = choosetype();
        if (selection == 1){
            totalcost = totalcost + rescalc();}
        else if (selection ==2){
            totalcost = totalcost + comcalc();}
        else if (selection == 3){
            totalcost = totalcost + indcalc();}
        printf("Do you want to continue and calculate another bill? If Yes enter 1 else 0: ");
        scanf("%d",&sentinal);
        printf("\n\n");
        counter = counter + 1;
    }
    printf("You calculated the bill %d times and the total amount from all the bills due is $%.2f\n\n",counter,totalcost);
    printf("EXISTING ELECTRICTY BILL CALCULATOR\n\n");
    return 0;
}
