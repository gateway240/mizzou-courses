#include <stdio.h>

float rescalc(){ //function that calculates the users cost if they choose residential

    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    }
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*7.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*10;}
    else if (usrinput <= 1500){
        usrcost = usrinput*13.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*15;}

    connectioncharge = 25;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;
}
float comcalc(){//function that calculates the users cost if they choose commercial
    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    } 
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*10.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*14;}
    else if (usrinput <= 1500){
        usrcost = usrinput*17.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*20;}

    connectioncharge = 90;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;


}

float indcalc(){//function that claculates the user cost if they choose industrial
    int usrinput;
    printf("Enter the number of units (in kWh): ");
    scanf("%d",&usrinput);
    printf("\n\n");
    while (usrinput < 0) {
        printf("Invalid input! Please enter a positive value: ");
        scanf("%d",&usrinput);
        printf("\n\n");
    }
    float usrcost;
    float connectioncharge;
    if (usrinput <= 300){//rates based on chart provided
        usrcost = usrinput*36.5;}
    else if (usrinput <= 750){
        usrcost = usrinput*40;}
    else if (usrinput <= 1500){
        usrcost = usrinput*45.5;}
    else if (usrinput > 1500){
        usrcost = usrinput*50;}

    connectioncharge = 850;//initializes connection charge
    usrcost = usrcost/100;//converts cost calculated from cents to dollars
    float totalcost;
    totalcost = connectioncharge+usrcost;
    printf("Total energy charge for the customer is: $%.2f\n\n",usrcost);
    printf("Total bill due from this connection is: $%.2f\n\n",totalcost);
    return totalcost;



}
int choosetype(){//function for selecting which type of power. Returns th users choice as the function output
    int selection;
    printf("1. Residential\n2. Commerical\n3. Industrial\n\n");
    printf("Choose the type of connection: ");
    scanf("%d",&selection);
    printf("\n\n");
    while ((selection < 1) | (selection >3)){
        printf("Invalid Choice! Please enter a valid choice\n\n");
        printf("1. Residential\n2. Commerical\n3. Industrial\n\n");
        printf("Choose the type of connection: ");
        scanf("%d",&selection);
        printf("\n\n");
    }


    return selection;
}

int bonus(number){

    int newnum;
    int testnum;
    int evensum =0;
    int oddsum=0;
    int i, sum=0;

    testnum = -1;
    int n[number];
    printf("The numbers are:");
    for(newnum = 0; newnum <= number; newnum++ ){

        testnum = testnum+1;
        n[newnum] = testnum;
        printf("%d ",n[newnum]);
    }
    printf("\n");
    for (i=0; i<=number;i++){
        sum = sum +n[i];
        if (n[i]%2 == 0){
            evensum = evensum + n[i];
        }
        else{
            oddsum = oddsum + n[i];}

    }
    printf("The sum of all numbers from 0 to %d is %d\n",number,sum);
    printf("Sum of Even numbers = %d\n", evensum);
    printf("Sum of Odd numbers = %d\n", oddsum);


    return 0;
}


int main(void){

    printf("***** ELECTRICITY BILL CALCULATOR *****\n\n\n");
    int sentinal;
    int counter;
    int selection;
    float totalcost;
    totalcost = 0;
    counter = 0;
    sentinal = 1;
    while (sentinal != 0){//ask users if they want to calculate another electrical bill
        selection = choosetype();//calls function to select type of power
        //if statments below call the appropriate functions for calculating user cost based on the type the user chose.
        if (selection == 1){
            totalcost = totalcost + rescalc();}
        else if (selection ==2){
            totalcost = totalcost + comcalc();}
        else if (selection == 3){
            totalcost = totalcost + indcalc();}
        printf("Do you want to continue and calculate another bill? If Yes enter 1 else 0: ");
        scanf("%d",&sentinal);
        printf("\n\n");
        counter = counter + 1;//counts how many time the loop has repeated
    }
    printf("You calculated the bill %d times and the total amount from all the bills due is $%.2f\n\n",counter,totalcost);
    printf("EXISTING ELECTRICTY BILL CALCULATOR\n\n");
    printf("***BONUS***\n");
    int usrnum;
    do {
        printf("Enter a positive integer: ");
        scanf("%d",&usrnum);
    }while( usrnum < 0);
    bonus(usrnum);
    return 0;
}
