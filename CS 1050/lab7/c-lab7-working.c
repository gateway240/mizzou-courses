/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 3-13-2017 
 * Lab: Lab7
 * Section: CS1050C Monday 3:00 PM
 * */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 20 
//function prototypes
int check_error(int);
void initialize_2Darray(int array[MAX][MAX],int);
void print_2Darray(int [MAX][MAX],int);
int findZeros(int array[][MAX],int);
int findEvenOdd(int [][MAX],int);
void loadDiagonal(int[][MAX],int,int[MAX]);
void loadDiagonalodd(int[][MAX],int,int[MAX]);
void print1DArray(int [],int);
void printTranspose(int [][MAX],int);
void findMaxNumber(int[][MAX],int);
int main (void){
    srand(time(NULL));
    int array1[MAX][MAX]={};
    int usrnum;
    printf("Enter the side of the 2-D array: ");
    scanf("%d",&usrnum);
    while (check_error(usrnum)!=1){
        printf("Please enter a value between 1-20 only:");
        scanf("%d",&usrnum);
    }
    initialize_2Darray(array1,usrnum);
    printf("\nThe first 2-D array is:\n");
    print_2Darray(array1,usrnum);
    printf("The number of zeros in 2-D Array: %d\n",findZeros(array1,usrnum));
    int evennum,oddnum;
    evennum = findEvenOdd(array1,usrnum);
    oddnum = usrnum*usrnum-evennum;//deterimns number of odd values by subtracting the number of even values from total values in matrix
    printf("The number of Even numbers: %d\n",evennum);
    printf("The number of Odd numbers: %d\n",oddnum);
    int diagarray[MAX]={};
    loadDiagonal(array1,usrnum,diagarray);
    int diagarrayodd[MAX]={};
    loadDiagonalodd(array1,usrnum,diagarrayodd);
    printf("The primary diagonal elements in the 2-D array, loaded into a 1-D array are: ");
    print1DArray(diagarray,usrnum);
    printf("\n");
    printf("The odd diagonal elements in the 2-D array, loaded into a 1-D array are: ");
    print1DArray(diagarrayodd,usrnum);

    printf("\n");
    printTranspose(array1,usrnum);
    printf("\n******BONUS******\n");
    findMaxNumber(array1,usrnum);

    return 0;
}
void findMaxNumber(int a[][MAX],int size){
    int counta[MAX][MAX]={}, i,j;
    int countb[10]={};
    int numval = 10;
    int mosttimes[MAX]={};
    int bufferarray[MAX]={};
    int bufferarray2[MAX]={};
    int bufferval = 0;
    int mostval = -1;
    print1DArray(mosttimes,MAX);
    printf("\n");
    for(i=0;i<size;i++){
        countb[a[0][i]]++;
    }

    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            counta[i][a[i][j]]++;

        }
    }
    print_2Darray(counta,numval);

    for(i=0;i<numval;i++){
        if(countb[i]>bufferval){
            //mostval=counta[1][i];
            bufferval = countb[i]; 
            mostval=i;
        }
    }

    printf("mostval is %d\n",mostval);

    for(i=0;i<numval;i++){
        for(j=0;j<numval;j++){
            if (counta[i][j]>bufferarray[i]){
                bufferarray[i]=counta[i][j];
                mosttimes[i]=j;

            }
        }
    }
    for(i=0;i<numval;i++){
        for(j=0;j<numval;j++){
            if (counta[i][j]>bufferarray2[i]){
                bufferarray2[i]=counta[i][j];

            }
        }
    }

    for (i=0;i<numval;i++){
        printf("%d",mosttimes[i]);
        printf("ROW %d:------>%d occurs %d times\n",i,mosttimes[i],bufferarray2[i]);
    }
    printf("\n\n");
}

int check_error(int num){//ensures number is between 1 and 20 returns 1 if true
    return(num>0 && num<=20);
}
void initialize_2Darray(int array[MAX][MAX],int size){//fills a 2D array with random values from 0-9
    int i,j;
    for (i=0;i<size;i++){
        for(j=0;j<size;j++){
            array[i][j]=rand() %10;
        }
    }
}
void print_2Darray(int array[MAX][MAX],int size){//prints a 2D array
    int i,j;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            printf("%d",array[i][j]);
        }
        printf("\n");
    }
}
int findZeros(int array[][MAX],int size){//returns number of zeros in 2D number
    int i,j;
    int zerocount;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            if(array[i][j] == 0){
                zerocount++;
            }
        }
    }
    return zerocount;
}
int findEvenOdd(int array[][MAX], int size){//returns number of even values in 2D array
    int i,j;
    int evencount;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            if(array[i][j] % 2 == 0){
                evencount++;
            }
        }
    }
    return evencount;
}
void loadDiagonal(int array[][MAX],int size, int diagarray[MAX]){//populates a 1D array with the values of the primary diagonal of a 2D input array
    int i,j;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            if (i==j){
                diagarray[i]=array[i][j];
            }
        }
    }
}

void loadDiagonalodd(int array[][MAX],int size, int diagarray[MAX]){//populates a 1D array with the values of the primary diagonal of a 2D input array

    int i,j;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            if (i+1==size-j){
                diagarray[i]=array[i][j];
            }
        }
    }
}

void print1DArray(int a[],int size){//prints a 1D array
    int i;
    for(i=0;i<size;i++){
        printf("%d",a[i]);
    }
}
void printTranspose(int a[][MAX],int size){//prints the input array transposed Ex. Rows to Columns
    int i,j;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            printf("%d",a[j][i]);
        }

        printf("\n");
    }
}






