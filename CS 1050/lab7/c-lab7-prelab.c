/* Name: Alex Beattie
 * Pawprint: akbkx8
 * Date: 3-6-2017 
 * Lab: Lab6
 * Section: CS1050C Monday 3:00 PM
 * */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define MAX 100

int check_error(int);
void initialize_2Darray(int array[MAX][MAX],int);
void print_2Darray(int array[MAX][MAX],int);
int main (void){
    srand(time(NULL));
    int array1[MAX][MAX]={};
    int usrnum;
    printf("Enter the side of the 2-D array:");
    scanf("%d",&usrnum);
    while (check_error(usrnum)!=1){
        printf("Please enter a value between 1-20 only:");
        scanf("%d",&usrnum);
    }
    initialize_2Darray(array1,usrnum);
    printf("\n\nThe first 2-D array is:\n");
    print_2Darray(array1,usrnum);

    return 0;
}

int check_error(int num){
    return(num>0 && num<=20);
}
void initialize_2Darray(int array[MAX][MAX],int size){
    int i,j;
    for (i=0;i<size;i++){
        for(j=0;j<size;j++){
            array[i][j]=rand() %10;
        }
    }
}
void print_2Darray(int array[MAX][MAX],int size){
    int i,j;
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            printf("%d",array[i][j]);
        }
        printf("\n");
    }
}


