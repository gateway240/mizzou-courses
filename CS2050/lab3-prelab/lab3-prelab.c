#include <stdio.h>
#include <stdlib.h>
#include"minunit.h"
#include "lab2Alex.h"

/*-----------------------------------------------------------------------------
 *  *  Testing for functions
 *   *-----------------------------------------------------------------------------*/
#define LISTSIZE 100
#define STARTVAL 0
#define INTERVAL 1
#define FLOATVAL 0.5
#define RANDOMINDEX 23

int tests_run = 0;

List testList;
static char * testCreateArray(){
  mu_assert("array not initialized succesfully", createList(&testList,LISTSIZE) != 0);
  return 0;
}
static char * testFillArray(){
  int i = 0;
  for (i=STARTVAL;i<testList.maxSizeOfList;i+=INTERVAL){
    addItem(i+FLOATVAL,&testList);
    mu_assert("input value does not match array value", getItem(i,testList) == i+FLOATVAL);
  }
  return 0;
}
static char * testOverfillArray(){
  mu_assert("Array allowed to be overfilled", addItem(STARTVAL+FLOATVAL,&testList) == 1);
  return 0;
}
static char * testGetItem(){
  float testVal = STARTVAL + (LISTSIZE-RANDOMINDEX)*INTERVAL + FLOATVAL;
  int testIndex = LISTSIZE-RANDOMINDEX;
  mu_assert("Expected value != array value",getItem(testIndex,testList) == testVal);
  return 0;
}
static char * testSizeOfList(){
  mu_assert("sizeOfList() returns incorrect value",sizeOfList(testList) == LISTSIZE+1);
  return 0;
}
static char * testDeleteList(){
  deleteList(&testList);
  mu_assert("List not deleted correctly. Array not free", testList.array == NULL);
  return 0;
}
static char * all_tests(){
  printf("Beginning testing...\n");
  int i = 0;
  for (;i<2;i++){
    mu_run_test(testCreateArray);
    mu_run_test(testFillArray);
    mu_run_test(testOverfillArray);
    mu_run_test(testSizeOfList);
    mu_run_test(testGetItem);
    mu_run_test(testDeleteList);
  }
  return 0;
}

/*-----------------------------------------------------------------------------
 *  *  Main program
 *   *-----------------------------------------------------------------------------*/
int main (void){
  char *result = all_tests();
  printf("Tests run: %d\n",tests_run);
  if (result != 0 ) {
    printf("%s\n",result);
  }
  else {
    printf("\nALL TESTS PASSED\n\n");
  }
  return result != 0;
}
