#include <stdio.h>
#include <stdlib.h>
#include"minunit.h"
#include "lab3Alex.h"

/*-----------------------------------------------------------------------------
 *  *  Testing for functions
 *   *-----------------------------------------------------------------------------*/
#define LISTSIZE 100
#define STARTVAL 0
#define INTERVAL 1
#define FLOATVAL 0.5
#define RANDOMINDEX 23
#define DELETEINDEX 1
#define OVERFILLAMOUNT 3

int tests_run = 0;
int overfillCounter = 0;
List testList;
List secondTestList;
List appendedList;
static char * testCreateList(){
  mu_assert("array not initialized succesfully", createList(&testList,LISTSIZE) != 0);
  return 0;
}
static char * testFillArray(){
  int i = 0;
  int j = 0;
  for (i=STARTVAL;i<testList.maxSizeOfList;i+=INTERVAL){
    addItem(i+FLOATVAL,&testList);
    mu_assert("input value does not match array value", getItem(j,testList) == i+FLOATVAL);
    j++;
  }
  return 0;
}
static char * testOverfillArray(){
  int i = 0;
  for (i=STARTVAL;i<LISTSIZE*OVERFILLAMOUNT;i+=INTERVAL){
    mu_assert("Array not resized properly on overfill", addItem(i+FLOATVAL,&testList) == 1);
    //printf("%f ",testList.array[i]);
    overfillCounter++;
  }
  return 0;
}
static char * testGetItem(){
  float testVal = STARTVAL + (LISTSIZE-RANDOMINDEX)*INTERVAL + FLOATVAL;
  int testIndex = LISTSIZE-RANDOMINDEX;
  mu_assert("Expected value != array value",getItem(testIndex,testList) == testVal);
  return 0;
}
static char * testSizeOfList(){
  mu_assert("sizeOfList() returns incorrect value",sizeOfList(testList) == overfillCounter);
  return 0;
}
static char * testDeleteList(){
  deleteList(&testList);
  mu_assert("List not deleted correctly. Array not free", testList.array == NULL);
  return 0;
}
static char * testRemoveItems(){
  float removedValuefromArray = getItem(DELETEINDEX,testList);
  float removedItem = deleteItem(DELETEINDEX,&testList);
  mu_assert("removed incorrect value", removedItem == removedValuefromArray);
  int i = 0;
  for (i=0;i<LISTSIZE;i++){
    if ( i == DELETEINDEX ) {
      continue;
    }
    if (i > DELETEINDEX ){
      mu_assert("input value does not match array value after deletion", getItem(i,testList) == (STARTVAL+ (i+1)*INTERVAL + FLOATVAL) );
      i++;
    }
    else {
      mu_assert("input value does not match array value before deletion", getItem(i,testList) == (STARTVAL+ (i)*INTERVAL + FLOATVAL));
      i++;
    }
  }
  return 0;
}
static char * testRemoveInvalidIndex(){
  mu_assert("removeing invalid index not handeled correctly", deleteItem(LISTSIZE+LISTSIZE*OVERFILLAMOUNT+1,&testList) == -1);
  return 0;
}
static char * testAppendList(){
  mu_assert("array not initialized succesfully", createList(&testList,LISTSIZE) != 0);
  mu_assert("array not initialized succesfully", createList(&secondTestList,LISTSIZE) != 0);
  mu_assert("append list failed", appendLists(testList,secondTestList, &appendedList) == 1);
  int i = 0;
  int j = 0;
  for (i=STARTVAL;i<secondTestList.maxSizeOfList;i+=INTERVAL){
    addItem(i+FLOATVAL,&testList);
    addItem(i+FLOATVAL,&secondTestList);
    mu_assert("input value does not match array value", getItem(j,testList) == i+FLOATVAL);
    mu_assert("input value does not match array value", getItem(j,secondTestList) == i+FLOATVAL);
    j++;
  }
  for (i=0;i<LISTSIZE*2;i++){
    if ( i < LISTSIZE ) {
      mu_assert("first list values incorrect after append", getItem(i,testList) == (STARTVAL+ (i)*INTERVAL + FLOATVAL) );
    }
    else {
      mu_assert("second list values incorrect after append", getItem(i-LISTSIZE,secondTestList) == (STARTVAL+ (i-LISTSIZE)*INTERVAL + FLOATVAL) );
    }
  }
  deleteList(&testList);
  deleteList(&secondTestList);
  return 0;
}
static char * testMergeList(){
  List firstListMerge;
  List secondListMerge;
  List mergedList;
  mu_assert("array not initialized succesfully", createList(&firstListMerge,10) != 0);
  mu_assert("array not initialized succesfully", createList(&secondListMerge,10) != 0);
  float firstArray[] = {1, 1, 2, 2.5, 3.6, 4, 5, 6, 2, 3.2};
  float secondArray[] ={6, 1, 2.5, 18, 23, 4.3, 5, 5, 4, 5.6};
  float mergedArray[] = {1, 2, 2.5, 3.6, 4, 5, 6, 3.2, 18, 23, 4.3, 5.6 };
  size_t arrayAdd = 0;
  for (; arrayAdd< 10;arrayAdd++){
    addItem(firstArray[arrayAdd],&firstListMerge);
  }
  for (arrayAdd = 0; arrayAdd< 10;arrayAdd++){
    addItem(secondArray[arrayAdd],&secondListMerge);
  }
  mergedList = mergeList(firstListMerge,secondListMerge);
  size_t i = 0;
  for (; i<mergedList.tail;i++){
    //printf("%f ",getItem(i,mergedList));
    mu_assert("merged value does not match true value", getItem(i,mergedList) == mergedArray[i]);
  }
  deleteList(&firstListMerge);
  deleteList(&secondListMerge);
  deleteList(&mergedList);
  return 0;
}
static char * all_tests(){
  printf("Beginning testing...\n");
  int i = 0;
  for (;i<2;i++){
    overfillCounter = LISTSIZE;
    mu_run_test(testCreateList);
    mu_run_test(testFillArray);
    mu_run_test(testOverfillArray);
    mu_run_test(testSizeOfList);
    mu_run_test(testGetItem);
    mu_run_test(testRemoveItems);
    mu_run_test(testRemoveInvalidIndex);
    mu_run_test(testDeleteList);
  }
  mu_run_test(testAppendList);
  mu_run_test(testMergeList);
  return 0;
}

/*-----------------------------------------------------------------------------
 *  *  Main program
 *   *-----------------------------------------------------------------------------*/
int main (void){
  char *result = all_tests();
  printf("Tests run: %d\n",tests_run);
  if (result != 0 ) {
    printf("%s\n",result);
  }
  else {
    printf("\nALL TESTS PASSED\n\n");
  }
  return result != 0;
}
