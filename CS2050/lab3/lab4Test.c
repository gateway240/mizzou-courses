#include <stdio.h>
#include <stdlib.h>
#include "lab4Alex.c"

/*-----------------------------------------------------------------------------
 * Main program
 *-----------------------------------------------------------------------------*/
int main (void){
	List usrList;
	static float usrArray[7] = {1.11,2.22,1.11,1.11,3.33,2.22,2.22};
	usrList.array = usrArray;
	usrList.maxSizeOfList = 7;
	usrList.tail = 7;
	printf("Occurrences of 1.11: %d \n",countOccurrences(1.11,usrList));
	printf("Occurrences of 2.22: %d \n\n",countOccurrences(2.22,usrList));
	printf("Promoted item at index 1 with value: %.2f\n",promoteAtIndex(1,usrList));
	printList(usrList);
	printf("\nPromoted item at index 4 with value: %.2f\n",promoteAtIndex(4,usrList));
	printList(usrList);
	return 1;
}
