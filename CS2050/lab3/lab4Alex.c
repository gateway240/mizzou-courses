/*
 * =====================================================================================
 *
 *       Filename:  lab4Alex.c
 *
 *    Description:  Lab 4 Implementation and source code
 *
 *        Version:  1.0
 *        Created:  09/07/2017 08:32:15 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#include	"lab4Alex.h"
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  sizeOfList
 *  Description:  Returns the current size of the list
 * =====================================================================================
 */
int
sizeOfList(List usrList){
	return usrList.tail;
}		/* -----  end of function sizeOfList  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  countOccurrences
 *  Description:  Counts the number of occurrences of the float value specified in the
 *  first argument in the List array specified in the second argument. Returns the
 *  number of occurrences as an integer.
 * =====================================================================================
 */
	int
countOccurrences (float item, List usrList  )
{
	size_t i = 0;
	size_t occurenceCounter = 0;
	for (i=0;i<usrList.tail;i++){
		if( usrList.array[i] == item ){
			occurenceCounter++;
		}
	}
	return occurenceCounter;
}		/* -----  end of function countOccurences  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  promoteAtIndex
 *  Description:  Swaps item at the index provided in first input argument with the item
 *  at the begenning of the array in the List struct provided in the second argument.
 *  Returns the promoted value or -1 upon failure.
 * =====================================================================================
 */
	float
promoteAtIndex (int index, List usrList  )
{
	if(index <= usrList.tail){
		float tempValue = 0;
		tempValue = usrList.array[0];
		usrList.array[0] = usrList.array[index];
		usrList.array[index] = tempValue;
		return usrList.array[0];
	}
	else {
		return -1;
	}
}		/* -----  end of function promoteAtIndex  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  printList
 *  Description:  Prints out array contents and size of the array in a user defined List
 *  struct. Accepts the List struct as an argument and returns void.
 * =====================================================================================
 */
	void
printList (List usrList )
{
	size_t i = 0;
	printf("Size of List: %d \n",sizeOfList(usrList));
	printf("Contents of List: \n");
	for(i=0;i<usrList.tail;i++){
		printf("%.2f ",usrList.array[i]);
	}
	printf("\n");
	return ;
}		/* -----  end of function printList  ----- */


