#include <stdio.h>
#include <stdlib.h>

/*-----------------------------------------------------------------------------
 *  List struct definition
 *-----------------------------------------------------------------------------*/
typedef struct listnode {
	int maxSizeOfList, tail;
	float *array;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/
int sizeOfList(List usrList);
int countOccurrences (float item, List usrList );
float promoteAtIndex (int index, List usrList );
void printList (List usrList );
