/*
 * =====================================================================================
 *
 *       Filename:  lab11.h
 *
 *    Description:  Header files for lab 11
 *
 *        Version:  1.0
 *        Created:  11/01/2017 07:46:52 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef struct {
	size_t numEl1;
	size_t numEl2;
	size_t dataSize;
	int (*cmpfunc)(const void*,const void*);
} mergeConfig;
int merge (void* firstArray, void* secondArray, void* mergedArray, mergeConfig usrConfig );
int compareInt (const void* usrIntVal,const void* compIntVal );
void printInt (void* usrIntVal );
void printArray(void* usrArray,size_t dataSize, size_t numElements, void (*printFun)(void *));
