/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Test program for lab 11
 *
 *        Version:  1.0
 *        Created:  11/01/2017 07:47:20 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#include "lab11.h"
	int
main ( void )
{
	printf ( "program STARTING\n" );

	int array1[10] = {1,2,5,5,7,9,10,14,17,18};
	int array2[10] = {2,3,5,6,7,9,12,13};
	int mergedArray[20] = {0};
	mergeConfig usrConfig;
	usrConfig.numEl1 = 10;
	usrConfig.numEl2 = 8;
	usrConfig.dataSize = sizeof(int);
	usrConfig.cmpfunc = compareInt;
	printf ( "First Array contains\n" );
	printArray(&array1,sizeof(int),10,printInt);
	printf ( "Second Array contains\n" );
	printArray(&array2,sizeof(int),10,printInt);
	merge(&array1,&array2,&mergedArray,usrConfig);

	printf ( "Merged Array contains\n" );
	printArray(&mergedArray,sizeof(int),18,printInt);
	printf ( "program EXITING\n" );
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

