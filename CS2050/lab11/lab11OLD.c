/*
 * =====================================================================================
 *
 *       Filename:  lab11.c
 *
 *    Description:  Function implementaitons for lab11
 *
 *        Version:  1.0
 *        Created:  11/01/2017 07:47:02 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include "lab11.h"

	int
merge (void* firstArray, void* secondArray, void* mergedArray, mergeConfig usrConfig  )
{
	size_t numEl1 = usrConfig.numEl1;
	size_t numEl2 = usrConfig.numEl2;
	size_t dataSize = usrConfig.dataSize;
	int (*cmpfunc)(const void*,const void*) = usrConfig.cmpfunc;
	void* firstVal = firstArray;
	void* secondVal = secondArray;
	void* tempMergedArray = mergedArray;
	if (cmpfunc(firstVal,secondVal) >= 0){
		memcpy((char*)tempMergedArray,firstVal,dataSize);
		memcpy((char*)tempMergedArray+dataSize,secondVal,dataSize);
	}
	else if (cmpfunc(firstVal,secondVal) == -1){
		memcpy((char*)tempMergedArray,secondVal,dataSize);
		memcpy((char*)tempMergedArray+dataSize, firstVal,dataSize);
	}
	int count = numEl1-numEl2;
	//	printf("count %d numEl1 %u numEl2 %u\n", count, numEl1,numEl2);
	if ((numEl1 == 0 || numEl2 == 0) && count != 0 ) {
		if ( count > 0 ){
			memcpy((char*)tempMergedArray,firstVal,dataSize*abs(count));
		}
		else{
			memcpy((char*)tempMergedArray,secondVal,dataSize*abs(count));
		}
		return 1;
	}
	else if ((numEl1 == 0 || numEl2 == 0) && count == 0 ){
		return 0;
	}
	firstVal = firstVal+ dataSize;
	secondVal = secondVal + dataSize;
	tempMergedArray = tempMergedArray + 2*dataSize;
	mergeConfig tempConfig = usrConfig;
	tempConfig.numEl1--;
	tempConfig.numEl2--;
	//	printf("tempconfig numEl1 %u numEl2 %u\n", tempConfig.numEl1, tempConfig.numEl2);

	merge(firstVal,secondVal,tempMergedArray,tempConfig);
	return -1;
}		/* -----  end of function merge  ----- */
	int
compareInt (const void* usrIntVal,const void* compIntVal  )
{
	int firstVal = *((int*)usrIntVal);
	int secondVal = *((int*)compIntVal);
	if (firstVal < secondVal) return 1;
	if (firstVal > secondVal) return-1;
	return 0;
}		/* -----  end of function compareInt  ----- */
	void
printInt (void* usrIntVal  )
{
	int* intValue= (int*)usrIntVal;
	printf("%d ",*intValue);
	return ;
}		/* -----  end of function printInt  ----- */
void printArray(void* usrArray,size_t dataSize, size_t numElements, void (*printFun)(void *)) {
	while(numElements--){
		printFun(usrArray);
		usrArray= (char *)usrArray+dataSize;
	}
	printf("\n");
}
