/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Test program for lab 4
 *
 *        Version:  1.0
 *        Created:  09/14/2017 08:08:00 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"lab4Alex.c"

	int
main ( void )
{
	//Initialize a test list with the following values
	List usrList;
	static float usrArray[8] = {1.11,2.22,1.11,1.11,3.33,2.22,1.11,3.33};
	usrList.array = usrArray;
	usrList.maxSizeOfList = 8;
	usrList.tail = 8;

	//Replace values in list with specified value at given index
	printf("\n\nSTARTING program\n\n");
	printList( usrList);
	float replacedVal = replaceAtIndex(4.44,2,usrList);
	errorCheck(replacedVal,2,4.44);
	printList( usrList);
	replacedVal = replaceAtIndex(5.55,10,usrList);
	errorCheck(replacedVal,10, 5.55);
	printList( usrList);
	//Copy contents of the first list to the second without duplicates
	List secondList;
	printf("\nSize of new list: %d\n",copyArrayWithoutDuplicate (usrList, &secondList));
	printList(secondList);
	deleteList (&secondList);

	printf ( "\n\nEXITING Program\n\n" );
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */

