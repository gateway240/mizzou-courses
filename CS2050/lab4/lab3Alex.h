#include <stdio.h>
#include <stdlib.h>

/*-----------------------------------------------------------------------------
 *  List struct definition
 *-----------------------------------------------------------------------------*/
typedef struct listnode {
  int maxSizeOfList, tail;
  float *array;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/
int createList(List *, int);
float getItem(int, List);
int sizeOfList(List);
void deleteList(List *);
float deleteItem(int, List*);
int appendLists(List, List, List*);
List mergeList (List firstUsrList, List secondUsrList);
int duplicateCheck( float value, float* array, int arraySize);
/*-----------------------------------------------------------------------------
 *  Implemented functions
 *-----------------------------------------------------------------------------*/
//Initializes struct pointer provided by user. Returns 0 if array is not initialized and 1 for success
int createList(List* usrList, int listSize){
  usrList->maxSizeOfList = listSize;
  usrList->tail = 0;
  if (!(usrList->array = (float*) malloc(sizeof(float)*listSize))){
    return 0;
  }
  return 1;
}
//Adds item to list. Accepts listValue and address of a list. Adds value to next avaliable space in array,
//increment array position counter, and increase the size of the array if adding an item would exceed array size.
int addItem(float listVal, List *usrList){
  if (usrList->tail >= usrList->maxSizeOfList){
    float *tempArray = realloc(usrList->array,sizeof(float)*(usrList->maxSizeOfList)*2);
    if(tempArray){
    usrList->array = tempArray;
    usrList->maxSizeOfList*=2;
    }
    else{
      free(tempArray);
      return 0;
    }
  }
  usrList->array[usrList->tail] = listVal;
  usrList->tail++;
  return 1;
}
//gets value present in array at specefied index starting at 0. Returns 0 if index has not been initialized
float getItem( int indexVal, List usrList){
  if (indexVal > usrList.tail){
    return -1;
  }
  return  usrList.array[indexVal];
}
//Returns the current size of the list
int sizeOfList(List usrList){
  return usrList.tail;
}
//Deletes the list by freeing the array and reseting the tail value
void deleteList(List* usrList){
  usrList->tail = 0;
  free(usrList->array);
  usrList->array = NULL;
}
//Deletes an item from the list and shifts items up on the list to fill the deleted space. On sucess returns the value deleted.
//Returns zero if user specified index has not been initialized or is outside the bounds of the array

float deleteItem(int usrIndex, List* usrList){
  float deletedValue = getItem(usrIndex,*usrList);
  int i = 0;
  for (;i<usrList->maxSizeOfList;i++){
    if (i>= usrIndex){
      usrList->array[i] = usrList->array[i+1];
    }
  }
  usrList->tail--;
  return deletedValue;
}
//Takes two lists and concatenates them. Combined list is set in third argument pointer. Returns 0 if memory allocation fails. 1 for success
int appendLists(List firstList, List secondList, List * combinedList){
  float* combinedArray = malloc(sizeof(float)*firstList.maxSizeOfList+secondList.maxSizeOfList);
  if(!combinedArray){
    return 0;
  }
  int i = 0;
  for(;i<firstList.tail;i++){
    *(combinedArray+i) =  firstList.array[i];
  }
  int j = 0;
  for(;j<secondList.tail;j++){
    *(combinedArray+i) = secondList.array[j];
    i++;
  }
    combinedList->array = combinedArray;
    combinedList->tail = i-1;
    free(combinedArray);
    return 1;
  }
int duplicateCheck( float value, float* array, int arraySize){
    size_t i = 0 ;
    for (i=0;i<arraySize;i++){
      if (array[i] == value){
      //  printf("Array value:%f Usr value: %f \n",array[i],value);
        return 1;
      }
      //printf("Passed array value %d: %f \n",i, array[i]);
    } return 0;
}


List mergeList (List firstList, List secondList){
  List mergedList;
  int listSize = firstList.maxSizeOfList+secondList.maxSizeOfList;
  float* combinedArray = calloc(listSize,sizeof(float));
  size_t i = 0;
  size_t combinedCounter = 0;
  for(;i<firstList.maxSizeOfList;i++){
      if (!duplicateCheck(firstList.array[i],combinedArray, listSize)){
        combinedArray[combinedCounter++]=firstList.array[i];
      }
    }
  for(i=0;i<secondList.maxSizeOfList;i++){
      if (!duplicateCheck(secondList.array[i],combinedArray,listSize)){
        combinedArray[combinedCounter++]=secondList.array[i];
      }
    }
mergedList.maxSizeOfList = listSize;
mergedList.array = combinedArray;
mergedList.tail = combinedCounter-1;
return mergedList;
}

