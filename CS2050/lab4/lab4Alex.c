/*
 * =====================================================================================
 *
 *       Filename:  lab4Alex.c
 *
 *    Description:  Implementation of functions for lab 4
 *
 *        Version:  1.0
 *        Created:  09/14/2017 08:07:27 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"lab4Alex.h"

//Initializes struct pointer provided by user. Returns 0 if array is not initialized and 1 for success
int createList(List* usrList, int listSize){
	usrList->maxSizeOfList = listSize;
	usrList->tail = 0;
	if (!(usrList->array = (float*) malloc(sizeof(float)*listSize))){
		return 0;
	}
	return 1;
}
//Deletes the list by freeing the array and reseting the tail value
void deleteList(List* usrList){
	usrList->tail = 0;
	free(usrList->array);
	usrList->array = NULL;
}
//Prints the contents of a List
	void
printList (List usrList )
{
	size_t i = 0;
	printf("Contents of List: \n");
	for(i=0;i<usrList.tail;i++){
		printf("%.2f ",usrList.array[i]);
	}
	printf("\n");
	return ;
}		/* -----  end of function printList  ----- */
//Replaces the value at the user specified index with the user specified value in the List
	float
replaceAtIndex (float item, int index, List usrList  )
{
	if (index <= usrList.maxSizeOfList){
		float deletedVal = usrList.array[index];
		usrList.array[index] = item;
		return deletedVal;
	}
	else {
		return -1.0;
	}
}		/* -----  end of function replaceAtIndex  ----- */
//Checks for duplicates in a float array
int duplicateCheck( float value, float* array, int arraySize){
	size_t i = 0 ;
	for (i=0;i<arraySize;i++){
		if (array[i] == value){
			return 1;
		}
	}
	return 0;
}
//Copies the contenst of the first list to the second list without duplicates
int copyArrayWithoutDuplicate (List firstList, List *secondList){
	int listSize = firstList.maxSizeOfList;
	float* combinedArray = calloc(listSize,sizeof(float));
	size_t i = 0;
	size_t combinedCounter = 0;
	for(;i<firstList.maxSizeOfList;i++){
		if (!duplicateCheck(firstList.array[i],combinedArray, listSize)){
			combinedArray[combinedCounter++]=firstList.array[i];
		}
	}
	secondList->maxSizeOfList = listSize;
	secondList->array = combinedArray;
	secondList->tail = combinedCounter;
	return secondList->tail;
}
//Performs error checking on the return value of replaceAtIndex()
	void
errorCheck ( float replacedVal,int index, float usrVal  )
{
	if (replacedVal != -1){
		printf("\nReplaced value: %.2f at index: %d with %.2f\n",replacedVal,index,usrVal);
	}
	else {
		printf("\nInvalid index value: %d. Array index does not exist\n",index);
	}

	return ;
}		/* -----  end of function errorCheck  ----- */
