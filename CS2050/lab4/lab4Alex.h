/*
 * =====================================================================================
 *
 *       Filename:  lab4Alex.h
 *
 *    Description:  Header file for lab 4
 *
 *        Version:  1.0
 *        Created:  09/14/2017 08:07:43 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include	<stdlib.h>

/*-----------------------------------------------------------------------------
 *  List struct definition
 *-----------------------------------------------------------------------------*/
typedef struct listnode {
	int maxSizeOfList, tail;
	float *array;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  createList
 *  Description:  Initializes strcut pointer provided by the user with the provided size.
 *  Returns 0 if array is not initalized and 1 for success.
 * =====================================================================================
 */
int createList(List *, int);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteList
 *  Description:  Deletes the list specified by freeing the array and reseting the tail
 *  value.
 * =====================================================================================
 */
void deleteList(List *);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  replaceAtIndex
 *  Description:  Replaces the specified item and the specified index in the List
 *  provided by the user. Function returns the value it replaced in the list if
 *  successful. Upon failure function returns -1.
 * =====================================================================================
 */
float replaceAtIndex (float item, int index, List usrList  );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  duplicateCheck
 *  Description:  Checks for the provided float value in the provided array with the
 *  provided size. Returns 0 if no duplicate is found in the array. Returns 1 if a
 *  duplicate value is found.
 * =====================================================================================
 */
int duplicateCheck( float value, float* array, int arraySize);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  copyArrayWithoutDuplicate
 *  Description:  Initalizes the provided List secondList. Copies the contents from
 *  the provided firstList without duplicated values to the secondList. Resets list tail
 *  to appropriate value and returns the new size of the contents of the list.
 * =====================================================================================
 */
int copyArrayWithoutDuplicate (List firstList, List *secondList);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  printList
 *  Description:  Prints out array contents and size of the array in a user defined List
 *  struct. Accepts the List struct as an argument and returns void.
 * =====================================================================================
 */
void printList (List usrList );

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  errorCheck
 *  Description:  Performs error checking on the return value of the replaceAtIndex
 *  function. Function returns nothing and primarily prints to the console output.
 * =====================================================================================
 */
void errorCheck ( float replacedVal,int index, float usrVal );
