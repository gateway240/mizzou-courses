// C program for generic linked list

#include	"LinkList.c"



/* Driver program to test above function */
int main()
{
	printf("program starting\n");
	List* usrList = createList();
	// Create and print an int linked list
	unsigned int_size = sizeof(int);
	int arr[] = {10, 20, 30, 40, 50}, i;
	for (i=4; i>=0; i--)
		push(usrList, &arr[i], int_size);
	printf("Created integer linked list is \n");
	printList(usrList->head, printInt);
	printf("size of list: %d\n",getListSize(usrList));
	int a = 5;
	printf("comp value %d\n",listContainsItem(&a,usrList,compareInt));

	// Create and print a float linked list
	unsigned float_size = sizeof(float);
	deleteList(usrList);
	usrList = createList();

	float arr2[] = {10.1, 20.2, 30.3, 40.4, 50.5};
	for (i=4; i>=0; i--)
		push(usrList, &arr2[i], float_size);
	printf("\n\nCreated float linked list is \n");
	printList(usrList->head, printFloat);
	printf("\n");
	deleteList (usrList);

	return 0;
}
