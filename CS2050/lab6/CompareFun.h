/*
 * =====================================================================================
 *
 *       Filename:  CompareFun.h
 *
 *    Description:  Functions for comparing two values
 *
 *        Version:  1.0
 *        Created:  09/20/2017 10:46:14 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

	int
compareInt (void* usrIntVal, void* compIntVal  )
{
	//printf("First value: %d, Second Value: %d\n",*(int*)usrIntVal,*(int*)compIntVal);
	return (*(int*)usrIntVal == *(int*)compIntVal);
}		/* -----  end of function compareInt  ----- */

