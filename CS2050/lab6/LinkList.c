/*
 * =====================================================================================
 *
 *       Filename:  LinkList.c
 *
 *    Description:  Implentation of Link List for lab5
 *
 *        Version:  1.0
 *        Created:  09/21/2017 12:24:21 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"LinkList.h"
#include	"PrintFun.h"
//Creates a linked list with null values
	List*
createList (  )
{
	List* usrList = malloc(sizeof(List*));
	struct Node *start = NULL;
	usrList->size = 0;
	usrList->head = start;
	usrList->tail = start;

	return usrList ;
}		/* -----  end of function createList  ----- */
//Deletes linked list
	void
deleteList (List* usrList  )
{
	struct Node* tempNode;
	//traverses the link list until the end and frees the pointers along the way
	while(usrList->head != NULL){
		tempNode=usrList->head;
		usrList->head = usrList->head->next;
		deleteNode(tempNode);
	}
	free(usrList);
	return ;
}		/* -----  end of function deleteList  ----- */
/* Function to add a node at the beginning of Linked List.
   This function expects a pointer to the data to be added
   and size of the data type */
	struct Node*
newNode ( void* new_data, int data_size )
{
	// Allocate memory for node
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	if (new_node){
		new_node->data  = malloc(data_size);
	}
	int i;
	//copies data bytewise for the given data value to allow for any data type
	for (i=0; i<data_size; i++)
		*(char *)(new_node->data + i) = *(char *)(new_data + i);
	return new_node ;
}		/* -----  end of function newNode  ----- */
//frees memory for one node upon deletion
	void
deleteNode (struct Node* usrNode  )
{
	if (usrNode){//frees the data and the node itself for link list
		free(usrNode->data);
		free(usrNode);
		usrNode=NULL;
	}
	return ;
}		/* -----  end of function deleteNode  ----- */
	struct Node*
createNode(int usrVal)
{
	struct Node* usrNode;
	usrNode = newNode(&usrVal,sizeof(int));
	return usrNode;
}		/* -----  end of function createNode(int)  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  push
 *  Description:  Accepts link list, void pointer, and size of data contained in void
 *  pointer. Pushs value contained in void pointer to the top of linked list
 * =====================================================================================
 */
void push(List* usrList, void *new_data, size_t data_size)
{
	// Allocate memory for node
	struct Node* new_node = newNode(new_data,data_size);
	if (new_node){
		new_node->next = usrList->head;//set next value of new node to current head value

		if(usrList->head == NULL){//account for empty list
			usrList->tail = new_node;
		}
		// Change head pointer as new node is added at the beginning
		usrList->head    = new_node;
		usrList->size++;
	}
}
	List*
popOffStack (List* usrList)
{
	if(usrList-> head == NULL){
		printf("Empty Stack\n");
		return usrList;
	}
	int* usrVal = (int*) usrList->head->data;//retreives the value poped off sack
	printf("Popped %d off the stack\n", *usrVal);
	struct Node* tempPtr = usrList->head;//stores temporary node to free upon removal
	usrList->head = usrList->head->next;// moves the head pointer up one to pop node off stack
	deleteNode(tempPtr);//frees node no longer in list
	return usrList ;
}		/* -----  end of function popOffStack(List*)  ----- */
	List*
deQueue (List * usrList)
{
	if(usrList->head == NULL){
		printf("The queue is empty\n");
		return usrList;
	}
	int* usrVal = (int*) usrList->head->data;//retreives value contained in deQueued node
	printf("Retrieved  %d from the queue \n", *usrVal);
	struct Node* tempPtr = usrList->head;//stores pointer for node to be removed
	usrList->head = usrList->head->next;//deQueues node from list
	deleteNode(tempPtr);//deletes node deQueued from list
	if(usrList->head == NULL){//handles end of list condition
		usrList->tail = NULL;
	}
	return usrList ;
}		/* -----  end of function deQueue  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  queue
 *  Description:  Accpts link list, void pointer, and size of data contained in void
 *  pointer. Inserts value at the end of the linked list like a queue.
 * =====================================================================================
 */
void queue(List* usrList, void *new_data, size_t data_size)
{
	// Allocate memory for node
	struct Node* new_node = newNode(new_data,data_size);
	if(new_node){
		new_node->next = NULL;//set end of list next pointer to null
		// Change head pointer as new node is added at the beginning
		if (usrList->head ==NULL){//account for empty list
			usrList->head = new_node;
			usrList->tail = new_node;
		}
		else{//Add value to the end of the list
			usrList->tail->next = new_node;
			usrList->tail = new_node;
		}
		usrList->size++;
	}
}
//adds intger value to beginning of link list
	List*
pushToStack ( List* usrList, int value  )
{
	push(usrList,&value,sizeof(int));
	printf("Pushed %d onto the stack.\n", value);
	return usrList;
}		/* -----  end of function pushToStack  ----- */
//adds integer value to end of link list
	List*
enQueue ( List* usrList, int value  )
{
	queue(usrList,&value,sizeof(int));
	printf("Entered %d in the queue.\n",value);
	return usrList ;
}		/* -----  end of function enQueue  ----- */
/* Function to print nodes in a given linked list. fpitr is used
   to access the function to be used for printing current node data.
   Note that different data types need different specifier in printf() */
void printListGeneric(struct Node *node, void (*printFun)(void *))
{
	while (node != NULL)
	{
		(*printFun)(node->data);
		node = node->next;
	}
	printf("\n");
}
//prints integer values contained in link lists
	void
printList ( List usrList  )
{
	printListGeneric(usrList.head, printInt);
	return ;
}		/* -----  end of function printList  ----- */
