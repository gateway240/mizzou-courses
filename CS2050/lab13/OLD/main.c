/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Lab 13 test program
 *
 *        Version:  1.0
 *        Created:  11/29/2017 06:30:37 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#include "lab13.h"

	int
main ( void )
{
	printf("program STARTING\n");
	Employee employeeList[ARR_SIZE];
	readfile(employeeList);
	//Create binary tree node
	Node *bt = NULL ;


	printf ( "Binary tree Insert.\n" ) ;

	//Insert values into BST
	size_t i;
	for ( i = 0 ; i <= 9999 ; i++ )
		bt = insert ( bt, employeeList[i] ) ;
	printf ( "\nIn-order traversal of binary tree:\n" ) ;
	inOrderPrintBST(bt);
	/*j	Heap* usrHeap = newHeap(1);
	  for (i=0;i<5;i++){
	 *usrHeap = insertMin(*usrHeap,i);
	 }
	 for (i=0;i<5;i++){
	 printf("%d ", deleteMin(*usrHeap));
	 }
	 */

	printf ( "program EXITING\n" );
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
