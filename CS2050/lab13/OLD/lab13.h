/*
 * =====================================================================================
 *
 *       Filename:  lab13.h
 *
 *    Description:  Lab 13 function headers
 *
 *        Version:  1.0
 *        Created:  11/29/2017 06:30:29 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ARR_SIZE 10000
typedef struct employee{
        char fname[25], lname[25];          //first name and last name of an employee
        int age;
        int salary;
 		int ssn;
}Employee;
typedef struct node{
 		Employee info;
 		struct node *left, *right;
}Node;

typedef struct heapNode {
	int key;
	int size;
	struct heapNode *left, *right;
} Heap;

Heap* newHeapNode ( int usrKey );
Heap* newHeap ( int usrKey );
Heap insertMin (Heap q, int usrKey );
int deleteMin (Heap q );
int deleteMin (Heap q );
void readfile(Employee*);

Node* newNode(Employee);

Node* insert(Node*, Employee);

void inOrderPrintBST(Node*);


void search(Node*, int, Employee*);
void printEmployee (Employee usrEmployee );
