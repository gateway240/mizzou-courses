/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  Lab 13 test program
 *
 *        Version:  1.0
 *        Created:  11/29/2017 06:30:37 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#include "lab13.h"

	int
main ( void )
{
	printf("program STARTING\n");
	//Employee employeeList[ARR_SIZE];
	Employee employeeList[10];
	readfile(employeeList);
	//Create binary tree node
	Node *bt = NULL ;


	printf ( "Binary tree Insert.\n" ) ;

	//Insert values into BST
	size_t i;
	for ( i = 0 ; i <= 9 ; i++ )
		bt = insert ( bt, employeeList[i] ) ;
	printf ( "\nIn-order traversal of binary tree:\n" ) ;
	inOrderPrintBST(bt);

	printf ( "Delete 11\n" );
	bt=	deleteNode(bt,11);
	bt=	deleteNode(bt,11);
	inOrderPrintBST(bt);
	Heap* usrHeap = NULL;
	int randNum[10] = {5,4,6,10,3,9,17,11,2,10};
	for (i=0;i<10;i++){
		usrHeap = insertMin(usrHeap,randNum[i]);
	}
	printf("\n");
	inOrderPrintHeap(usrHeap);

	printf ( "\n" );
	for (i=0;i<10;i++){
		printf("\n");
		inOrderPrintHeap(usrHeap);
		printf("Delete Val: %d  ", deleteMin(usrHeap));
	}

	printf ( "\nprogram EXITING\n" );
	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
