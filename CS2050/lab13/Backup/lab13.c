/*
 * =====================================================================================
 *
 *       Filename:  lab13.c
 *
 *    Description:  Lab 13 implementation
 *
 *        Version:  1.0
 *        Created:  11/29/2017 06:30:12 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include "lab13.h"

	Heap*
newHeapNode ( int usrKey  )
{
	Heap *newNode = malloc (sizeof (Heap));
	newNode->left = NULL;
	newNode->right= NULL;
	newNode->key = usrKey;
	newNode->size = 1;
	return newNode ;
}		/* -----  end of function newNode  ----- */
	void
deleteHeapNode (Heap* usrNode  )
{
	if (usrNode != NULL){
		//		free(usrNode);
	}
	return ;
}		/* -----  end of function deleteHeapNode  ----- */

	Heap*
insertMin (Heap* q, int usrKey  )
{
	if (q == NULL){
		return newHeapNode(usrKey);
	}
	else{
		int tempKey;
		if(usrKey < q->key){
			tempKey = q->key;
			q->key = usrKey;
			usrKey = tempKey;
		}
		if (q->left == NULL){
			(q->left) = insertMin(q->left,usrKey);
		}
		else if (q->right == NULL && q->left != NULL){
			(q->right) = insertMin(q->right,usrKey);
		}
		else{
			if(q->right->size > q->left->size){
				(q->left) = insertMin(q->left,usrKey);
			}
			else {
				(q->right) = insertMin(q->right,usrKey);
			}
		}


	}

	q->size++;
	return q;
}		/* -----  end of function insertMin  ----- */

	int
deleteMin (Heap* q  )
{
	int min;
	min = q->key;
	if(q->right == NULL && q->left == NULL){
		min = q->key;
		deleteHeapNode(q);
		q->size--;
		return min;
	}
	if (q->left == NULL){
		q->key = deleteMin((q->right));
	}
	else if (q->right == NULL){
		q->key = deleteMin((q->left));
	}
	else {
		if( q->left->key < q->right->key){
			q->key = deleteMin((q->left));
		}
		else{
			q->key = deleteMin((q->right));
		}
	}
	return min;
}		/* -----  end of function deleteMin  ----- */
void inOrderPrintHeap(Heap* tree){
	//if tree is not NULL
	if(tree != NULL){
		inOrderPrintHeap(tree->left);
		printf("%d  ", tree->key);
		inOrderPrintHeap(tree->right);
	}
}
	Node*
newNode ( Employee usrEmployee  )
{
	//Malloc space for node
	Node *sr = malloc ( sizeof ( Node ) ) ;
	//Initalize node with the values from the usrEmployee struct
	usrEmployee.count = 1;
	( sr ) -> left = NULL ;
	( sr ) -> info = usrEmployee ;
	( sr ) -> right = NULL ;
	return sr ;
}		/* -----  end of function newNode  ----- */
Node* minValueNode(Node* usrNode)
{
	Node* current = usrNode;

	/* loop down to find the leftmost leaf */
	while (current->left != NULL)
		current = current->left;

	return current;
}
//Inserts a node in the correct position in a BST with the given usrEmployee data in ascending order by SSN
Node* insert ( Node *head, Employee usrEmployee )
{
	if ( head == NULL )
	{
		return  newNode(usrEmployee);
	}

	else if ( head->info.ssn == usrEmployee.ssn){
		head->info.count++;
	}
	else
	{
		//Check social security number and insert appropriatley
		if ( usrEmployee.ssn < head -> info.ssn )
			head->left = insert(  head -> left , usrEmployee  ) ;
		else
			head->right =  insert ( head -> right , usrEmployee ) ;
	}
	return head;
}
Node* deleteNode(Node* tree, int usrKey){
	//if tree is not NULL
	if(tree == NULL) return tree;

	if (usrKey< tree->info.ssn){
		tree->left = deleteNode(tree->left, usrKey);
	}

	else if (usrKey > tree->info.ssn){
		tree->right = deleteNode(tree->right,usrKey);
	}
	else if (tree->info.count > 1){
		tree->info.count--;
	}
	else {
		if(tree->left == NULL){
			Node* temp = tree->right;
			free(tree);
			return temp;
		}
		else if (tree->right == NULL){
			Node* temp = tree->left;
			free(tree);
			return temp;
		}

		Node* temp = minValueNode(tree->right);
		tree->info.ssn = temp->info.ssn;
		tree->right = deleteNode(tree->right,temp->info.ssn);

	}
	return tree;
}
void inOrderPrintBST(Node* tree){
	//if tree is not NULL
	if(tree != NULL){
		inOrderPrintBST(tree->left);
		printf("%d  ", tree->info.ssn);
		inOrderPrintBST(tree->right);
	}
}
	void
search (Node* tree, int key, Employee* usrEmployee  )
{
	if(tree == NULL ){

		return ;
	}
	//Asign the record to the current value if the key is found
	else if (tree -> info.salary == key  ){
		*usrEmployee = tree->info;
		return;
	}
	//Keep searching
	else {
		search(tree->left,key,usrEmployee);
		search(tree->right,key,usrEmployee);
	}
}		/* -----  end of function search  ----- */

	void
printEmployee (Employee usrEmployee  )
{
	//Print out and format a single employee record
	printf("First name: %12s, Last name: %12s, Age: %2d, Salary: %5d, SSN: %10d\n",
			usrEmployee.fname,
			usrEmployee.lname,
			usrEmployee.age,
			usrEmployee.salary,
			usrEmployee.ssn
		  );
	return ;
}		/* -----  end of function printEmployee  ----- */
//This function reads from a file and creates an array of Employee structure containing their information.
void readfile(Employee employeeList[]){
	FILE *fp;
	fp = fopen("employeeData.csv", "r");
	int i = 0;
	if (fp) {
		while (i < ARR_SIZE){
			fscanf(fp, "%[^,],%[^,],%d,%d,%d\n",employeeList[i].fname,employeeList[i].lname,&employeeList[i].age,&employeeList[i].salary,&employeeList[i].ssn);
			i++;
		}
	}
	else{
		printf("Cannot find file\n");
	}

	fclose(fp);
}
