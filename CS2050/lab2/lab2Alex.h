#include <stdio.h>
#include <stdlib.h>

/*-----------------------------------------------------------------------------
 *  List struct definition
 *-----------------------------------------------------------------------------*/
typedef struct listnode {
  int maxSizeOfList, tail;
  float *array;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/
int createList(List *, int);
float getItem(int, List);
int sizeOfList(List);
void deleteList(List *);
float deleteItem(int, List*);
int appendLists(List, List, List*);

/*-----------------------------------------------------------------------------
 *  Implemented functions
 *-----------------------------------------------------------------------------*/
//Initializes struct pointer provided by user. Returns 0 if array is not initialized and 1 for success
int createList(List* usrList, int listSize){
  usrList->maxSizeOfList = listSize;
  usrList->tail = 0;
  if (!(usrList->array = malloc(sizeof(float)*listSize))){
    return 0;
  }
  return 1;
}
//Adds item to list. Accepts listValue and address of a list. Adds value to next avaliable space in array,
//increment array position counter, and increase the size of the array if adding an item would exceed array size.
int addItem(float listVal, List *usrList){
  if (usrList->tail >= usrList->maxSizeOfList){
    if(!(usrList->array = realloc(usrList->array,sizeof(float)*(usrList->maxSizeOfList)*2))){
        return 0;
        }
  }
  usrList->array[usrList->tail] = listVal;
  usrList->tail++;
  return 1;
}
//gets value present in array at specefied index starting at 0. Returns 0 if index has not been initialized
float getItem( int indexVal, List usrList){
  if (indexVal > usrList.tail){
    return 0.0;
  }
  return  usrList.array[indexVal];
}
//Returns the current size of the list
int sizeOfList(List usrList){
  return usrList.tail;
}
//Deletes the list by freeing the array and reseting the tail value
void deleteList(List* usrList){
  usrList->tail = 0;
  free(usrList->array);
  usrList->array = NULL;
}
//Deletes an item from the list and shifts items up on the list to fill the deleted space
float deleteItem(int usrIndex, List* usrList){
int i = 0;
  for (;i<usrList->maxSizeOfList;i++){
    if (i>= usrIndex){
      usrList->array[i] = usrList->array[i+1];
    }
  }
    usrList->tail--;
    return getItem(usrIndex,*usrList);
}
//Takes two lists and concatenates them. Combined list is set in third argument pointer. Returns 0 if memory allocation fails. 1 for success
int appendLists(List firstList, List secondList, List * combinedList){
  float* combinedArray = malloc(sizeof(float)*firstList.maxSizeOfList+secondList.maxSizeOfList);
  if(!combinedArray){
    return 0;
  }
  int i = 0;
  for(;i<firstList.tail;i++){
  *(combinedArray+i) =  firstList.array[i];
  }
  int j = 0;
  for(;j<secondList.tail;j++){
    *(combinedArray+i) = secondList.array[j];
    i++;
  }
  if(createList(combinedList,firstList.maxSizeOfList*secondList.maxSizeOfList)){
    combinedList->array = combinedArray;
    return 1;
  }
  else {
    return 0;
  }
}
