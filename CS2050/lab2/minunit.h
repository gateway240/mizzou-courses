/*
 * =====================================================================================
 *
 *       Filename:  minunit.h
 *
 *    Description:  Test framework for C Programs
 *
 *        Version:  1.0
 *        Created:  08/30/2017 07:23:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
 #define mu_run_test(test) do { char *message = test(); tests_run++; if (message) return message; } while (0)
 extern int tests_run;


