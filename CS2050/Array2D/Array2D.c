/*
 * =====================================================================================
 *
 *       Filename:  Array2D.c
 *
 *    Description:  2D Array ADT Implementation
 *
 *        Version:  1.0
 *        Created:  09/06/2017 06:41:21 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>

#include	"Array2D.h"
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  create2DArray
 *  Description:  Creates a 2D array and sets the number of rows and columns in position
 *  -2 and -1 respectively. Requires the size of the selected datatype, the number of
 *  rows and columns and returns a Array2D struct.
 * =====================================================================================
 */
	Array2D
create2DArray ( int dataTypeSize , size_t numRow, size_t numCol )
{
	Array2D usrArray;
	void *arrPtr;
	arrPtr	= malloc (numRow*numCol*dataTypeSize );
	if (! arrPtr ) {
		fprintf ( stderr, "\ndynamic memory allocation failed\n" );
		exit (1);
	}
	usrArray.numRow = numRow;
	usrArray.numCol = numCol;
	usrArray.dataTypeSize = dataTypeSize;
	usrArray.rowTail = 0;
	usrArray.colTail = 0;
	usrArray.array2D = arrPtr;
	return usrArray;
}		/* -----  end of function create2DArray  ----- */

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  delete2DArray
 *  Description:  Deletes 2DArray struct. Frees the array from memory and resets the
 *  other values to zero.
 * =====================================================================================
 */
	void
delete2DArray ( Array2D* usrArray  )
{
	usrArray-> dataTypeSize = 0;
	usrArray->rowTail = 0;
	usrArray->colTail = 0;
	free(usrArray->array2D);
	usrArray->array2D = NULL;
	return ;
}		/* -----  end of function delete2DArray  ----- */
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  getSizeOfArray
 *  Description:  Returns the number of rows multiplied by the number of columns
 *  as an integer indicating the totals size of the array
 * =====================================================================================
 */
	int
getSizeOfArray ( Array2D usrArray  )
{
	//return usrArray.array2D[-2] * usrArray.array2D[-1];
	return usrArray.numRow * usrArray.numCol;
}		/* -----  end of function getSizeOfArray  ----- */
	int
getNumRow ( Array2D usrArray )
{
	return usrArray.numRow ;
}		/* -----  end of function getNumRow  ----- */
	int
getNumCol ( Array2D usrArray  )
{
	return usrArray.numCol ;
}
	int
getDataTypeSize ( Array2D usrArray )
{
	return usrArray.dataTypeSize;
}		/* -----  end of function getDataTypeSize  ----- */
	int
checkOutOfBounds (int rowIndex, int colIndex,Array2D usrArray  )
{
	return ( rowIndex <= getNumRow(usrArray) && colIndex <= getNumCol(usrArray) );
}		/* -----  end of function checkOutOfBounds  ----- */
	int
addItem (void* usrValue, int rowIndex, int colIndex, Array2D usrArray  )
{
	size_t size = getDataTypeSize(usrArray);
	if ( checkOutOfBounds(rowIndex,colIndex,usrArray )) {
		memcpy((char*)usrArray.array2D+rowIndex*size*getNumCol(usrArray) +size*colIndex,usrValue,size);
		return 1;
		//void* tempArray2D = realloc(usrArray->array2D,getDataTypeSize(*usrArray)getSizeOfArray(*usrArray)*2;
	}
	return 0;

}		/* -----  end of function addItem  ----- */
	int
loadArray (void* usrValue, Array2D usrArray  )
{
	size_t i,j;
	for(i=0;i<getNumRow(usrArray);i++){
		for(j=0;j<getNumCol(usrArray);j++){
			addItem(usrValue,i,j,usrArray);
		}
	}
	return 0 ;
}		/* -----  end of function loadArray  ----- */
	void
getItem (  )
{
	return ;
}		/* -----  end of function getItem  ----- */
	void
clearArray ( Array2D usrArray  )
{
	memset(usrArray.array2D,0,getDataTypeSize(usrArray)*getSizeOfArray(usrArray));
	return ;
}		/* -----  end of function clearArray  ----- */
	int
deleteItem ( int rowIndex, int colIndex, Array2D usrArray  )
{
	size_t size = getDataTypeSize(usrArray);
	if ( checkOutOfBounds(rowIndex,colIndex,usrArray) ) {
		size_t i;
		char* destAddr;
		for(i=0;i<getNumCol(usrArray);i++){
			destAddr = (char*)usrArray.array2D+(rowIndex*size*getNumCol(usrArray))+size*i;
			if(i >= colIndex){
				memmove(destAddr,destAddr+size,size);
			}
		}
		if (destAddr){
			memset(destAddr,0, size);
		}
		return 1;
	}
	return 0 ;
}		/* -----  end of function deleteItem  ----- */
	void
printArray(Array2D usrArray, void (*printFunction)(void *))
{
	size_t arrayNumRow = getNumRow(usrArray);
	size_t arrayNumCol = getNumCol(usrArray);
	size_t size = getDataTypeSize(usrArray);
	int i;
	int j;
	printf("\nContents of Array: \n");
	void* tmpPtr = usrArray.array2D;
	for (i = 0; i < arrayNumRow; i++){
		printf("\n");
		for(j=0; j<arrayNumCol;j++){
			printFunction(((char *)tmpPtr) + i * size*arrayNumCol + j*size);
		}
	}
	printf("\n");
	return ;
}		/* -----  end of function printArray  ----- */
	void
printInt (void* usrIntVal  )
{
	int* intValue= (int*)usrIntVal;
	printf("%2d ",*intValue);
	return ;
}		/* -----  end of function printInt  ----- */
	void
printFloat (void* usrFloatVal )
{
	float* floatValue = (float*)usrFloatVal;
	printf("%.2f ",*floatValue);
	return ;
}		/* -----  end of function printFloat  ----- */
