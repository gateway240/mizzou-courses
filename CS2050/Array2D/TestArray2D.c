/*
 * =====================================================================================
 *
 *       Filename:  TestArray2D.c
 *
 *    Description:  Test program for 2D array ADT
 *
 *        Version:  1.0
 *        Created:  09/06/2017 06:43:24 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include	<stdlib.h>

#include	"Array2D.c"
#include	"unity.c"

void test_NumTest(void){
	int a = 1;
	//	TEST_ASSERT( a == 2 );
	TEST_ASSERT_EQUAL_INT(3, a);
}
	int
main ( int argc, char *argv[] )
{
	//	UNITY_BEGIN();
	//	RUN_TEST(test_NumTest);
	Array2D usrArray = create2DArray( sizeof(float),5,5);
	float a = 3.33;
	float b = 5.6;
	addItem(&a,0,0,usrArray);
	addItem(&b,3,4,usrArray);
	//void *ptrArray = usrArray.array2D;

	float usrValue = (*(float*)usrArray.array2D);
	printf("%f", usrValue);
	loadArray(&b,usrArray);
	deleteItem(1,1,usrArray);
	deleteItem(1,2,usrArray);
	printArray(usrArray,printFloat);

	Array2D usrIntArray = create2DArray( sizeof(int),6,6);
	int testIntArray[6][6]= {{1,2,3,4,5,6},
		{7,8,9,10,11,12},
		{1,2,3,4,5,6},
		{12,11,10,9,8,7},
		{5,6,7,8,9,10},
		{0,1,2,3,4,5}};
	size_t i,j;
	for (i=0;i<6;i++){
		for (j=0;j<6;j++){
			addItem(&testIntArray[i][j],i,j,usrIntArray);
		}
	}

	//int testVal = 6;
	//loadArray (&testVal,usrIntArray);
	//	int cInt = 3;
	//	addItem(&cInt,3,3,usrIntArray);

	printf ( "Array Before Deletion\n" );
	printArray(usrIntArray,printInt);
	deleteItem(0,2,usrIntArray);
	deleteItem(0,2,usrIntArray);
	printArray(usrIntArray,printInt);
	clearArray(usrIntArray);
	printArray(usrIntArray, printInt);
	delete2DArray(&usrArray);
	delete2DArray(&usrIntArray);
	//	return UNITY_END();
	return 1;

}				/* ----------  end of function main  ---------- */
