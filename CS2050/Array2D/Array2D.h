/*
 * =====================================================================================
 *
 *       Filename:  Array2D.h
 *
 *    Description:  Function prototypes and ADT declaration for 2D Array ADT
 *
 *        Version:  1.0
 *        Created:  09/06/2017 06:42:27 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

typedef struct Array2D_node {
	size_t dataTypeSize;
	size_t rowTail;
	size_t colTail;
	size_t numRow;
	size_t numCol;
	void * array2D;
}Array2D;				/* ----------  end of struct Array2D  ---------- */

