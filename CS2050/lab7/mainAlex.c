// C program for generic linked list

#include	"LinkList.h"

#include	<time.h>


/* Driver program to test above function */
int main()
{
	printf("program STARTING\n");
	// Initialize random number generation and array
	time_t t;
	srand((unsigned)time(&t));
	int randArray[5] = {0};
	//create linked list
	List* usrList = createList();
	size_t i;
	int randNum;
	//print out randomly generated numbers and assigns them to the array
	printf("Randomly generated number: ");
	for (i=0; i<5;i++){
		randNum =( rand()% 50)+1;
		randArray[i] = randNum;
		printf("%d ",randArray[i]);
	}
	printf("\n\nStack:\n\n");
	//sequentially push randomly generated numbers onto a stack
	for (i=0; i<5; i++){
		pushToStack(usrList,randArray[i]);
	}
	printf("\n\nThe created stack is:\n");
	//print the values contained in the stack
	printList(*usrList);
	printf("\n\n");
	//pop the values off the stack until the list is empty
	while(usrList->head->next != usrList->tail){
		popOffStack(usrList);
	}
	printf("The stack is now empty!\n\n");
	printf("Queue:\n\n");
	//put the same random numbers on a queue
	for (i=0; i<5; i++){
		enQueue(usrList,randArray[i]);
	}
	printf("\n\nThe created queue is:\n");
	//print the values contained in the queue
	printList(*usrList);
	printf("\n\n");
	//retreive items from the queue until the list is empty
	while(usrList->head->next != usrList->head){
		deQueue(usrList);
	}
	printf("The queue is now empty!\n");
	//print the contents of the link list
	printList(*usrList);
	//delete link list
	deleteList(usrList);
	printf ( "\nprogram EXITING\n" );


	return 0;
}
