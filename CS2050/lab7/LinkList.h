/*
 * =====================================================================================
 *
 *       Filename:  LinkList.h
 *
 *    Description:  Header file for link list ADT
 *
 *        Version:  1.0
 *        Created:  09/15/2017 03:26:18 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include	<stdlib.h>

/*-----------------------------------------------------------------------------
 *  Link List definition
 *-----------------------------------------------------------------------------*/

/* A linked list node */
struct Node
{
	// Any data type can be stored in this node
	void  *data;

	struct Node *next;
};
typedef struct {
	int size;
	struct Node *head, *tail;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/
List* createList ( );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  createList
 *  Description:  Creates linked list. Returns the pointer to the new list
 * =====================================================================================
 */
void deleteList (List* usrList );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteList
 *  Description:  Deletes linked list. Call before exiting program to free memory.
 *  Accepts pointer to Link List defined above.
 * =====================================================================================
 */
void deleteNode (struct Node* usrNode );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteNode
 *  Description:  deletes Node from given link list
 * =====================================================================================
 */
struct Node* createNode (int);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  createNode
 *  Description:  creates a node for the linked list with the given integer value
 * =====================================================================================
 */
List* pushToStack ( List* usrList, int value );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  pushToStack
 *  Description:  Adds a user specified integer value to the beginning of the user
 *  provided link list. Returns the new List pointer
 * =====================================================================================
 */
List* popOffStack(List*);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  popOffStack
 *  Description:  Pops item off stack
 * =====================================================================================
 */
List* enQueue ( List* usrList, int value );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  enQueue
 *  Description:  Adds the user specified integer value to the end of the user provided
 *  link list. Returns the new List pointer
 * =====================================================================================
 */
List* deQueue(List*);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deQueue
 *  Description:  removes values from queue;
 * =====================================================================================
 */
void printList ( List usrList );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  printListGeneric
 *  Description:  Prints the integer values contained in the user specified list.
 * =====================================================================================
 */
