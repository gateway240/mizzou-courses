/*
 * =====================================================================================
 *
 *       Filename:  LinkList.c
 *
 *    Description:  Implentation of Link List for lab5
 *
 *        Version:  1.0
 *        Created:  09/21/2017 12:24:21 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"LinkList.h"

#include	"CompareFun.h"
#include	"PrintFun.h"

	List*
createList (  )
{
	List* usrList = malloc(sizeof(List*));
	struct Node *start = NULL;
	usrList->size = 0;
	usrList->head = start;
	usrList->tail = start;

	return usrList ;
}		/* -----  end of function createList  ----- */
	void
deleteList (List* usrList  )
{
	struct Node* tempNode;
	while(usrList->head != NULL){
		tempNode=usrList->head;
		usrList->head = usrList->head->next;
		free(tempNode->data);
		free(tempNode);
	}
	free(usrList);
	return ;
}		/* -----  end of function deleteList  ----- */
/* Function to add a node at the beginning of Linked List.
   This function expects a pointer to the data to be added
   and size of the data type */
void push(List* usrList, void *new_data, size_t data_size)
{
	// Allocate memory for node
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	if (new_node){
		new_node->data  = malloc(data_size);
		new_node->next = (usrList->head);
		usrList->tail = new_node;

		// Copy contents of new_data to newly allocated memory.
		// Assumption: char takes 1 byte.
		int i;
		for (i=0; i<data_size; i++)
			*(char *)(new_node->data + i) = *(char *)(new_data + i);

		// Change head pointer as new node is added at the beginning
		usrList->head    = new_node;
		usrList->size++;
	}
}

	int
getListSize (List *usrList  )
{
	return usrList->size;
}		/* -----  end of function getListSize  ----- */
	int
listContainsItem (void* usrValue, List* usrList, int (*compFunction)(void *,void * ))
{
	struct Node* tempNode = usrList->head;
	while(tempNode != NULL){
		if((*compFunction)(usrValue,tempNode->data)){
			return 1;
		}
		tempNode = tempNode->next;
	}
	return 0;
}		/* -----  end of function listContainsItem  ----- */
/* Function to print nodes in a given linked list. fpitr is used
   to access the function to be used for printing current node data.
   Note that different data types need different specifier in printf() */
void printList(struct Node *node, void (*fptr)(void *))
{
	while (node != NULL)
	{
		(*fptr)(node->data);
		node = node->next;
	}
	printf("\n");
}
