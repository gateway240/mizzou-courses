// C program for generic linked list

#include	"LinkList.c"

#include	<time.h>


/* Driver program to test above function */
int main()
{
	printf("program STARTING\n");
	// Initialize random number generation and array
	time_t t;
	srand((unsigned)time(&t));
	int randArray[13] = {0};
	//create linked list
	List* usrList = createList();
	size_t i;
	int randNum;
	//print out randomly generated numbers and asign them to the array
	printf("Randomly generated number:\n");
	for (i=0; i<13;i++){
		randNum =( rand()% 13)+1;
		randArray[i] = randNum;
		printf("%d, ",randArray[i]);
	}
	//add the first 6 numbers to the beginning of the link list
	printf("\n");
	for (i=0; i<6; i++){
		addItemAtBeginning(usrList,randArray[i]);
	}
	//add the last 7 numbers to the end of the link list
	for (i=6; i<13; i++){
		addItemAtEnd(usrList,randArray[i]);
	}
	//print the contents of the link list
	printList(*usrList);
	//delete link list
	deleteList(usrList);
	printf ( "program EXITING\n" );


	return 0;
}
