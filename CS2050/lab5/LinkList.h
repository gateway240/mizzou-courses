/*
 * =====================================================================================
 *
 *       Filename:  LinkList.h
 *
 *    Description:  Header file for link list ADT
 *
 *        Version:  1.0
 *        Created:  09/15/2017 03:26:18 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	<stdio.h>
#include	<stdlib.h>

/*-----------------------------------------------------------------------------
 *  Link List strcut definition
 *-----------------------------------------------------------------------------*/

/* A linked list node */
struct Node
{
	// Any data type can be stored in this node
	void  *data;

	struct Node *next;
};
typedef struct {
	int size;
	struct Node *head, *tail;
} List;

/*-----------------------------------------------------------------------------
 *  Function prototypes
 *-----------------------------------------------------------------------------*/
List* createList ( );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  createList
 *  Description:  Creates linked list. Returns the pointer to the new list
 * =====================================================================================
 */
void deleteList (List* usrList );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deleteList
 *  Description:  Deletes linked list. Call before exiting program to free memory.
 *  Accepts pointer to Link List defined above.
 * =====================================================================================
 */
struct Node* newNode ( void* new_data, int data_size );
/* Function to add a node at the beginning of Linked List.
   This function expects a pointer to the data to be added
   and size of the data type */
void push(List* usrList, void *new_data, size_t data_size);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  push
 *  Description:  Accepts link list, void pointer, and size of data contained in void
 *  pointer. Pushs value contained in void pointer to the top of linked list
 * =====================================================================================
 */
void queue(List* usrList, void *new_data, size_t data_size);
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  queue
 *  Description:  Accpts link list, void pointer, and size of data contained in void
 *  pointer. Inserts value at the end of the linked list like a queue.
 * =====================================================================================
 */
List* addItemAtBeginning ( List* usrList, int value );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  addItemAtBeginning
 *  Description:  Adds a user specified integer value to the beginning of the user
 *  provided link list. Returns the new List pointer
 * =====================================================================================
 */
List* addItemAtEnd ( List* usrList, int value );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  addItemAtEnd
 *  Description:  Adds the user specified integer value to the end of the user provided
 *  link list. Returns the new List pointer
 * =====================================================================================
 */
void printList ( List usrList );
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  printListGeneric
 *  Description:  Prints the integer values contained in the user specified list.
 * =====================================================================================
 */
void printListGeneric(struct Node *node, void (*printFun)(void *));
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  printListGeneric
 *  Description:  Generic function to print value of a node in linke list. Acccepts
 *  Node pointer and function pointer to the approiate variable type.
 * =====================================================================================
 */
