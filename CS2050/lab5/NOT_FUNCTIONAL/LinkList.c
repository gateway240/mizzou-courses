/*
 *       Filename:  LinkList.c
 *
 *    Description:  Implementation of link list ADT
 *
 *        Version:  1.0
 *        Created:  09/15/2017 03:26:32 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"LinkList.h"

#include	<string.h>
#include	"CompareFun.h"

#include	"PrintFun.h"
	List*
createLinkList ( int listSize  )
{
	List* usrList = malloc(sizeof(List));
	Node* headNode = malloc(sizeof(Node));
	Node* tailNode = malloc(sizeof(Node));
	usrList->head = headNode;
	usrList->tail = tailNode;
	usrList->head->next = NULL;
	usrList-> size = listSize;

	return usrList ;
}		/* -----  end of function createLinkList  ----- */
	Node*
newNode ( void* data, Node *next  )
{
	Node *newNode = malloc(sizeof(*newNode));
	if( newNode != NULL){
		newNode->item = data;
		newNode->next = next;
	}
	return newNode ;
}		/* -----  end of function newNode  ----- */
	void
addItem ( List* usrList, void* data, int dataSize  )
{
	Node* current = usrList->head;
	while ( current->next != NULL){
		current = current->next;
	}
	current->next = malloc(sizeof(Node));
	current->next->item = data;
	current->next->next = NULL;
	return ;
}		/* -----  end of function addItem  ----- */
	int
getListSize (List *usrList  )
{
	return usrList->size;
}		/* -----  end of function getListSize  ----- */
	int
listContainsItem (void* usrValue, List* usrList, int (*compFunction)(void *,void * ))
{
	while(usrList != NULL){
		if((*compFunction)(usrValue,usrList->head->item)){
			return 1;
		}
		usrList->head = usrList->head->next;
	}
	return 0;
}		/* -----  end of function listContainsItem  ----- */
	int
getSizeOfItem ( Node usrNode )
{
	return usrNode.itemSize;
}		/* -----  end of function getSizeOfItem  ----- */
	void
printList(List* usrList, void (*printFunction)(void *))
{
	Node *current;
	printf("\nContents of Link List \n");
	current = usrList->head;
	while ( current != NULL){
		printf("%p\n",current->item);
		(*printFunction)(current->item);
		current = current->next;
	}
	printf("\n");
	return ;
}		/* -----  end of function printArray  ----- */
