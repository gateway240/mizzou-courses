/*
 * =====================================================================================
 *
 *       Filename:  Test.c
 *
 *    Description:  Test program for link lists
 *
 *        Version:  1.0
 *        Created:  09/15/2017 03:27:08 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"LinkList.c"


	int
main ( void )
{
	List* usrList = createLinkList(5);
	int firstVal[3] = {23,4,5};
	size_t i;
	for(i=0;i<3;i++){
		addItem(usrList,&firstVal[i],sizeof(int));
	}

	printList(usrList,printInt);

	return EXIT_SUCCESS;
}				/* ----------  end of function main  ---------- */
