/*
 * =====================================================================================
 *
 *       Filename:  printFun.h
 *
 *    Description:  Print functions for void* ADTS
 *
 *        Version:  1.0
 *        Created:  09/15/2017 03:47:40 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */
//function to print integer values
	void
printInt (void* usrIntVal  )
{
	int* intValue= (int*)usrIntVal;
	printf("%d, ",*intValue);
	return ;
}		/* -----  end of function printInt  ----- */

