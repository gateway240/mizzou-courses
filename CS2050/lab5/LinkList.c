/*
 * =====================================================================================
 *
 *       Filename:  LinkList.c
 *
 *    Description:  Implentation of Link List for lab5
 *
 *        Version:  1.0
 *        Created:  09/21/2017 12:24:21 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Alex Beattie (AB), akbkx8@mail.missouri.edu
 *        Company:  Mizzou Computer Science
 *
 * =====================================================================================
 */

#include	"LinkList.h"
#include	"PrintFun.h"
//Creates a linked list
	List*
createList (  )
{
	List* usrList = malloc(sizeof(List*));
	struct Node *start = NULL;
	usrList->size = 0;
	usrList->head = start;
	usrList->tail = start;

	return usrList ;
}		/* -----  end of function createList  ----- */
//Deletes linked list
	void
deleteList (List* usrList  )
{
	struct Node* tempNode;
	while(usrList->head != NULL){
		tempNode=usrList->head;
		usrList->head = usrList->head->next;
		free(tempNode->data);
		free(tempNode);
	}
	free(usrList);
	return ;
}		/* -----  end of function deleteList  ----- */
//creates new node for link list
	struct Node*
newNode ( void* new_data, int data_size )
{
	// Allocate memory for node
	struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
	if (new_node){
		new_node->data  = malloc(data_size);
	}
	int i;
	for (i=0; i<data_size; i++)
		*(char *)(new_node->data + i) = *(char *)(new_data + i);
	return new_node ;
}		/* -----  end of function newNode  ----- */
//adds new node to beginning of link list
void push(List* usrList, void *new_data, size_t data_size)
{
	// Allocate memory for node
	struct Node* new_node = newNode(new_data,data_size);
	if (new_node){
		new_node->next = usrList->head;

		if(usrList->head == NULL){
			usrList->tail = new_node;
		}
		// Change head pointer as new node is added at the beginning
		usrList->head    = new_node;
		usrList->size++;
	}
}
//Adds item at the end of linked list
void queue(List* usrList, void *new_data, size_t data_size)
{
	// Allocate memory for node
	struct Node* new_node = newNode(new_data,data_size);
	if(new_node){
		new_node->next = NULL;
		// Copy contents of new_data to newly allocated memory.
		// Assumption: char takes 1 byte.

		// Change head pointer as new node is added at the beginning
		if (usrList->head ==NULL){
			usrList->head = new_node;
			usrList->tail = new_node;
		}
		else{
			usrList->tail->next = new_node;
			usrList->tail = new_node;
		}
		usrList->size++;
	}
}
//adds intger value to beginning of link list
	List*
addItemAtBeginning ( List* usrList, int value  )
{
	push(usrList,&value,sizeof(int));
	return usrList;
}		/* -----  end of function addItemAtBeginning  ----- */
//adds integer value to end of link list
	List*
addItemAtEnd ( List* usrList, int value  )
{
	queue(usrList,&value,sizeof(int));
	return usrList ;
}		/* -----  end of function addItemAtEnd  ----- */
/* Function to print nodes in a given linked list. fpitr is used
   to access the function to be used for printing current node data.
   Note that different data types need different specifier in printf() */
void printListGeneric(struct Node *node, void (*printFun)(void *))
{
	while (node != NULL)
	{
		(*printFun)(node->data);
		node = node->next;
	}
	printf("\n");
}
//prints integer values contained in link lists
	void
printList ( List usrList  )
{
	printf("Created integer linked list contains: \n");
	printListGeneric(usrList.head, printInt);
	return ;
}		/* -----  end of function printList  ----- */
