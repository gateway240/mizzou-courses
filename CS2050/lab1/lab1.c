#include <stdio.h>
#include "lab1.h"
  
int main (void){

    float firstNum;
    float secondNum;
    printf("Enter the first value: ");
    scanf("%f",&firstNum);
    printf("Enter the second value: ");
    scanf("%f",&secondNum);
    int ratio = 0;
    //Address of declared value ratio provided to function
    if (floatRatioCalc (firstNum,secondNum,&ratio)){
            printf("The computed integer ratio is %d \n", ratio);
            return 1;
            }
            else {
                printf("Function Error. Make sure second value isn't zero\n");
                return 0;
            }

} 
