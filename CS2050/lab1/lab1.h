/*Function accepts two floating point numbers and an integer pointer to compute the integer ratio between the two.
 It first checks if the second float value is zero to avoid a divide by zero error and returns a boolean representation accordingly.
 To access the value of the computed ratio the calling program needs to derference the pointer value provided in the third argument
 */
int floatRatioCalc (float firstNum, float secondNum, int* ratio){
    if (secondNum){
    *ratio  = ((int)firstNum/(int)secondNum);
        return 1;
    }
    else {
        return 0;
    }
}

