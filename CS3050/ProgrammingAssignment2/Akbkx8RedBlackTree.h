/**
 * @Author: Alex Beattie <Akbkx8>
 * @Date:   2018-04-08T12:42:01-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: Akbkx8RedBlackTree.h
 * @Last modified by:   Akbkx8
 * @Last modified time: 2018-04-09T21:58:11-05:00
 */



#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define TRUE 1
#define MAX_Q_SIZE 500
#define COUNT 10
enum Color {RED,BLACK};


typedef struct node
{
    int data;
    bool color;
    struct node *left, *right, *parent;
}Node ;

Node* newNode(int data);
Node* BSTInsert(Node* root, Node* newNode);
void rotateLeft(Node **root, Node *x);
void rotateRight(Node **root, Node *x);
void swapColor(Node* ptr1, Node* ptr2);
void fixViolation (Node **root, Node *current);
void printNode(Node* node);
void RBInsert (int data, Node** root);
int height(Node* node);
void printLevelOrder(struct node* root);
