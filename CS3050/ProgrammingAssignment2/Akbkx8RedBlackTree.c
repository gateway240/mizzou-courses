/**
 * @Author: Alex Beattie <Akbkx8>
 * @Date:   2018-04-08T12:41:13-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: Akbkx8RedBlackTree.c
 * @Last modified by:   Akbkx8
 * @Last modified time: 2018-04-09T21:58:54-05:00
 */



#include "Akbkx8RedBlackTree.h"

void printNode(Node* node);
Node* newNode(int data){
    Node* newNode = malloc (sizeof(Node));
    newNode->data = data;
    newNode->left =  NULL;
    newNode->right = NULL;
    newNode->parent = NULL;
    return newNode;
}

Node* BSTInsert(Node* root, Node* newNode){
    //If the tree is empty return the new node as root
    if(root == NULL)
        return newNode;
    //Otherwise go through the tree
    if ( newNode->data < root -> data){
        root -> left = BSTInsert(root->left,newNode);
        root -> left -> parent = root;
    }
    else if (newNode -> data > root -> data){
        root -> right = BSTInsert (root->right, newNode);
        root -> right -> parent = root;
    }
    return root;
}
void rotateLeft(Node **root, Node *x){
    if(x->right == NULL){
        return;
    }else{
        Node* y = x->right;
        x -> right = y -> left;
        if ( x -> left != NULL)
            x->left->parent = x;

        y -> parent = x->parent;
        if(x->parent == NULL)
            *root = y;
        else
            if (x == (x ->parent) ->left)
                x->parent->left = y;
            else
                x->parent->right = y;

        y ->left = x;
        x -> parent = y;
    }
}
void rotateRight(Node **root, Node *x){
    if(x->left == NULL){
        return;
    }
    else{

        Node* y = x->left;
        x -> left = y -> right;
        if ( x -> right != NULL)
            x->right->parent = x;

        y -> parent = x->parent;
        if(x->parent == NULL)
            *root = y;
        else
            if (x == (x ->parent) ->right)
                x->parent->right = y;
            else
                x->parent->left = y;

        y ->right = x;
        x -> parent = y;
    }
}

void swapColor(Node* ptr1, Node* ptr2){
    bool tempColor = ptr1 -> color;
    ptr1 -> color = ptr2 ->color;
    ptr2 -> color = tempColor;
}

void fixViolation (Node **root, Node *current){
    Node *parentPtr = NULL;
    Node *grandParentPtr = NULL;
    // printf("In fix violation\n");
    current->color = RED;
    // printf("Setting current color\n");
    if (current -> parent){
        while((current != *root) && (current ->parent ->color == RED)){
            // printf("Parent color %d\n",current->parent->color);
            // printf("In While Loop\n");
            parentPtr = current -> parent;
            grandParentPtr = current -> parent -> parent;

            //Case A

            if( parentPtr == grandParentPtr->left){
                Node *unclePtr = grandParentPtr -> right;

                //Case 1
                if(unclePtr != NULL && unclePtr -> color == RED){
                    // printf("Case 1\n");
                    grandParentPtr -> color =RED;
                    parentPtr->color = BLACK;
                    unclePtr -> color = BLACK;

                    current = grandParentPtr;
                    // printf("setting current in case 1\n");
                }
                else{
                    //Case 2
                    // printf("Case 2\n");
                    if (current == parentPtr ->right){
                        current = parentPtr;
                        rotateLeft(root,parentPtr);

                    }
                    // printf("case 3\n");
                    //Case 3
                    parentPtr->color = BLACK;
                    grandParentPtr->color = RED;
                    rotateRight(root, grandParentPtr);

                }
            }
            //Case B
            else{
                Node *unclePtr = grandParentPtr->left;

                //Case 1
                if ((unclePtr != NULL) && (unclePtr ->color == RED)){
                    // printf("case 4\n");
                    grandParentPtr -> color = RED;
                    parentPtr->color = BLACK;
                    unclePtr -> color = BLACK;
                    current = grandParentPtr;
                }
                else{
                    //Case2
                    if(current == parentPtr->left)
                    {
                        // printf("case 5\n");
                        current = parentPtr;
                        // printf("set current \n");
                        rotateRight(root,parentPtr);

                    }                //Case 3
                    // printf("case 6\n");
                    parentPtr->color = BLACK;
                    grandParentPtr->color = RED;
                    rotateLeft(root,grandParentPtr);

                }
            }

        }
    }

    (*root) ->color = BLACK;

}
// Function to print binary tree in 2D
// It does reverse inorder traversal
void print2DUtil(Node *root, int space)
{
    // Base case
    if (root == NULL)
        return;

    // Increase distance between levels
    space += COUNT;

    // Process right child first
    print2DUtil(root->right, space);

    // Print current node after space
    // count
    printf("\n");
    for (int i = COUNT; i < space; i++)
        printf(" ");
    printf("%d\n", root->data);

    // Process left child
    print2DUtil(root->left, space);
}

// Wrapper over print2DUtil()
void print2D(Node *root)
{
    // Pass initial space count as 0
    print2DUtil(root, 0);
}
void printNode(Node* node){
    if (node != NULL){
        printf("%d-",node->data);
        if(node-> color == BLACK)
            printf("B");
        else
            printf("R");
    }
    printf("; ");
}

void RBInsert (int data, Node** root){
    // printf("in rb insert\n");
    Node* node = newNode(data);
    // printf("making new node\n");
    *root = BSTInsert(*root,node);
    // printf("inserting new node\n");


    fixViolation(root,node);
    // printf("Fixed violation\n");

}
int height(Node* node){
    if (node==NULL){
        return 0;
    }
    else{
        int lheight = height (node->left);
        int rheight = height (node->right);

        if(lheight >rheight)
            return (lheight +1);
        else return (rheight+1);
    }
}



/* Print nodes at a given level */
void printGivenLevel(struct node* root, int level)
{
    if (root == NULL)
        return;
    if (level == 1)
        printNode(root);
    else if (level > 1)
    {
        printGivenLevel(root->left, level-1);
        printGivenLevel(root->right, level-1);
    }
}
/* Function to line by line print level order traversal a tree*/
void printLevelOrder(struct node* root)
{
    int h = height(root);
    int i;
    for (i=1; i<=h; i++)
    {
        printGivenLevel(root, i);

    }
    printf("\n");
}
