@Author: Alex Beattie <Akbkx8>
@Date:   2018-04-09T21:59:29-05:00
@Email:  Akbkx8@mail.missouri.edu
@Filename: Readme.txt
@Last modified by:   Akbkx8
@Last modified time: 2018-04-09T22:17:25-05:00
Instructions for a UNIX based system:
Tested on tc.rnet server

Compile the program:
gcc Akbkx8Program2Main.c Akbkx8RedBlackTree.c -Wall -Werror -std=c99

Run the program without file input using: ./a.out
Run the program with file input using: ./a.out < input.txt
