/**
 * @Author: Alex Beattie <Akbkx8>
 * @Date:   2018-04-08T12:41:48-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: Akbkx8Program2Main.c
 * @Last modified by:   Akbkx8
 * @Last modified time: 2018-04-09T22:06:54-05:00
 */

#include "Akbkx8RedBlackTree.h"


int main (void){

    printf("Please enter a set of numbers to put in the tree seperated by commas and terminated with 'X':\n");
    Node* root = NULL;
    //char string[]=  "11,2,14,1,7,15,5,8,X";
    char string[255];
    scanf("%s",string);
    char *array[100];
    int i=0;

    array[i] = strtok(string,",");

    while(array[i]!=NULL)
    {
        array[++i] = strtok(NULL,",");
    }
    int j;
    for(j = 0;j<i;j++){
        // printf("%s\n",array[j]);
        if(strcmp(array[j],"X")!= 0){
            RBInsert(atoi(array[j]),&root);
        }
    }
    // print2D(root);
    printLevelOrder(root);
    return 0;
}
