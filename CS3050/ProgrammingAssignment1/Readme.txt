Alex Beattie Mini-Facebook Application

To compile the program run the following command:

g++ -std=c++11 Person.cpp LinkedList.cpp Homework1.cpp -Wall -Werror

The program can be run by typing the commands in manually with

./a.out

or with an input text file by using 

./a.out < Input.txt

This treats the contents of input.txt as input to the program 
