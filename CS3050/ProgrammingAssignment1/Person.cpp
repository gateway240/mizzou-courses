#include "stdafx.h"
#include "Person.h"



Person::Person()
{
	friends = list();
}

Person::Person(string usrName, int usrId )
{
	friends = list();

	name = usrName;
	id = usrId;

}



Person::~Person()
{
	friends.deleteList(friends.head);
}

void Person::addFriend(string value, int id)
{

	ptr = friends.initNode(value, id);
	friends.addNode(ptr);
	//printFriends();
}

void Person::removeFriend(string value)
{
	ptr = friends.searchName(friends.head, value);
	if (ptr == NULL) {
		cout << "\nName: " << name << " not found" << endl;
	}
	else {
		//cout << "\nDeleting a node ...  ";
		//friends.displayNode(ptr);
		friends.deleteNode(ptr);
	}
}

void Person::printFriends()
{
	
		friends.displayListString(friends.head);

}

bool Person::checkFriends(string name)
{	
	if (friends.head != NULL) {
		return friends.searchName(friends.head, name);
	}
	else return false;
}
