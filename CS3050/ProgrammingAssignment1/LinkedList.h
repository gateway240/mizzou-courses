#pragma once

using namespace std;

class list
{
public:
	struct node {
		int id;
		string name;
		struct node *next;
	} *head, *tail, *ptr;

	list() :head(NULL), tail(NULL) {}	// constructor	
	~list();			// destructor

	struct node* searchName(struct list::node*, string);
	struct node* searchId(struct list::node*, int);
	struct node* initNode(string, int);

	void reverse();
	void addNode(struct list::node*);
	void insertNode(struct list::node*);
	void deleteNode(struct list::node*);
	void deleteList(struct list::node*);
	void displayList(struct list::node*)const;
	void displayListString(list::node * ptr) const;
	void displayNode(struct list::node*) const;
	void displayNodeString(list::node * ptr) const;
};