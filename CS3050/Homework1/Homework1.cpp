
/*

Alex Beattie Programming Assignment 1
Mini-Facebook Application



*/
#include "stdafx.h"
#include "Homework1.h"

HashMap<string, Person*, HASHSIZE, MyKeyHash> hmap;
int id = 0;

using namespace std;
int main()
{
	
	cout << "Mini-Facebook. Please enter your commands" << endl;

	string input ="0";
	char control = '0';
	while (control != 'X' ) {
		getline(cin, input);
		if (input.length() != 0) {
			control = input.at(0); //get first character
			processInput(input);
		}
	}

    return 0;
}
void processInput(string input) {
	
	istringstream buf(input);
	istream_iterator<string> beg(buf), end;

	vector<string> tokens(beg, end); // store the string parts delimited by spaces in a vector

	vector<string>::iterator control;  // declare an iterator to a vector of strings
	
	control = tokens.begin();
	
	string name1;
	string name2;

	char controlChar = control->at(0);
	control++;
	
	if (tokens.size() == 2) {
		name1 = *control;
		
	}
	else if (tokens.size() == 3) {
		name1 = *control;
		control++;
		name2 = *control;

	}

	switch (controlChar) {
	case 'X': return;
	case 'P':
		if (tokens.size() == 2)
			createPersonRecord(name1);
		else
			cout << "Invalid input" << endl;
		break;
	case 'F':
		if (tokens.size() == 3)
			addFriend(name1,name2);
		else
			cout << "Invalid input" << endl;
		break;

	case 'U':
		if (tokens.size() == 3) {
			unFriend(name1,name2);
		}
		else
			cout << "Invalid input" << endl;
		break;
	case 'L':
		if (tokens.size() == 2) {
			printFriends(name1);
		}
		else
			cout << "Invalid input" << endl;
		break;
	case 'Q':
		if (tokens.size() == 3) {
			checkFriends(name1,name2);
		}
		else
			cout << "Invalid input" << endl;
		break;
	default:
		cout << "Unknown Command" << endl;
	}

}
void checkFriends(string name1, string name2) {

	Person* prsn1;
	Person* prsn2;
	if (hmap.get(name1, prsn1) && hmap.get(name2,prsn2)) {
		bool friends = prsn1->checkFriends(name2);
		cout << "Result: ";
		if (friends) {
			cout << "Yes" << endl;
		}
		else {
			cout << "No" << endl;
		}
	}
	else {
		cout << "Entry not found" << endl;
	}
	
}
void addFriend(string name1	, string name2) {
	Person* prsn1;
	Person* prsn2;
	if (hmap.get(name1, prsn1) && hmap.get(name2, prsn2)) {

		int tempId1 = prsn1->id;
		int tempId2 = prsn2->id;
		prsn1->addFriend(name2, tempId2);
		prsn2->addFriend(name1, tempId1);
	}
	else {
		cout << "Entry not found"<<endl;
	}

}
void unFriend(string name1, string name2) {

	Person* prsn1;
	Person* prsn2;
	if (hmap.get(name1, prsn1) && hmap.get(name2, prsn2)) {
		prsn1->removeFriend(name2);
		prsn2->removeFriend(name1);
	}
	else {
		cout << "Entry not found"<<endl;
	}


}
void printFriends(string name1) {
	
	Person* prsn1;
		
		if (hmap.get(name1, prsn1)) {
			cout << "Result: ";
			prsn1->printFriends();
		}
		else {
			cout << "Entry not found"<<endl;
		}
		
	
	

}
void createPersonRecord(string name1) {

	Person* newPrsn = new Person(name1,id);
	id++;
	hmap.put(name1, newPrsn);

}

