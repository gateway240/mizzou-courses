#pragma once
#include "Person.h"
#include "HashMap.h"
#include "LinkedList.h"

#define HASHSIZE 27
static constexpr std::size_t MULTIPLIER = 37;

struct MyKeyHash {
	unsigned long operator()(const string & key) const
	{
		std::size_t hash_value = 0;
		for (unsigned char ch : key)
			hash_value = hash_value * MULTIPLIER + ch;
		return hash_value % HASHSIZE;
	}
};

void processInput(string name);
void addFriend(string name1, string name2);
void createPersonRecord(string name1);
void unFriend(string name1, string name2);
void printFriends(string name1);
void checkFriends(string name1, string name2);