/**
 * @Author: Alex Beattie
 * @Date:   2018-04-26T22:03:52-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: main.c
 * @Last modified by:   Alex Beattie
 * @Last modified time: 2018-05-03T13:07:06-05:00
 */
 #include "ProgrammingAssignment3.h"
int main(void){
  int   f,s,g,u,d;
  printf("Enter floors, start, goal, up, and down seperated by spaces and the press enter: \n");
  scanf("%d%d%d%d%d",&f,&s,&g,&u,&d);
  floorSearch(f,s,g,u,d);
  return 0;
}
