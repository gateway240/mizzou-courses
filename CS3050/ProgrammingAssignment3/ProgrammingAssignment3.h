/**
 * @Author: Alex Beattie
 * @Date:   2018-05-03T08:10:04-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: ProgrammingAssignment3.h
 * @Last modified by:   Alex Beattie
 * @Last modified time: 2018-05-03T08:13:31-05:00
 */
 #include "stdio.h"
 #include "ProgrammingAssignment3.c"

int gcd(int x, int y);
void printSteps(int array[] , int count);
 void floorSearch(int floors, int start, int goal, int up, int down);
