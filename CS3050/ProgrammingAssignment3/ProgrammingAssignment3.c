/**
 * @Author: Alex Beattie
 * @Date:   2018-04-26T21:26:37-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: ProgrammingAssignment3.c
 * @Last modified by:   Alex Beattie
 * @Last modified time: 2018-04-26T22:22:35-05:00
 */
 #include "stdio.h"


 int gcd(int x, int y)
 {
     int t;
      while (y!=0)
     {
         t=y;
         y=x%y;
         x=t;
     }
     return x;
 }
 void printSteps(int array[] , int count){
   int i;
   for(i=0;i<count;i++){
     printf("%d",array[i]);
     printf("->");
   }
   printf("%d\n",array[count]);

 }
void floorSearch(int floors, int start, int goal, int up, int down){
  int gcdVal = gcd(up,down);
  if(start == goal){
    printf("You're on the target floors\n");
    return;
  }
  else if (up == 0 && down!=0 ){
    if(!((start-goal)>0 && (start-goal) % down == 0)){

      printf("use the stairs\n");
      return;
    }
  }
  else if( down==0 && up!=0){
    if(!((goal-start)>0 && (goal-start)%up == 0)){

      printf("use the stairs\n");
      return;
    }
  }
  else if (down == 0 && up == 0){
    printf("use the stairs\n");
    return;
  }
  if ((goal-start)%gcdVal!=0){
    printf("use the stairs\n");
    return;
  }
  int stepCount=0;
  int stepsArray[200] = {0};
  stepsArray[0] = start;

  while(start != goal){
    if(start+up > floors && start - down < 1){
      break;
    }
    if(start + up<= floors && start <= goal){
      start += up;
    }
    else if (start-down<1 && start+up <= floors){
      start += up;
    }
    else{
      start -= down;
    }
    // printf("%d",stepsArray[stepCount]);
    stepCount++;
    stepsArray[stepCount] = start;
    }

  // printf("\n");
  // printf("step count %d\n",stepCount);
  if(start == goal){
    printSteps(stepsArray,stepCount);
  }
  else{
    printf("use the stairs\n");
  }

}
