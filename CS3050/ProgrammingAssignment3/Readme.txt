@Author: Alex Beattie
@Date:   2018-05-03T13:07:47-05:00
@Email:  Akbkx8@mail.missouri.edu
@Filename: Readme.txt
@Last modified by:   Alex Beattie
@Last modified time: 2018-05-03T13:10:00-05:00
Instructions for a UNIX based system:
Tested on tc.rnet server

Compile the program:
gcc main.c -Wall -Werror


Run the program  using: ./a.out

The program will preform the calculations and then exit
To preform the calculations again, rerun the program.
