/**
 * @Author: Alex Beattie
 * @Date:   2018-04-24T21:58:31-05:00
 * @Email:  Akbkx8@mail.missouri.edu
 * @Filename: main.c
 * @Last modified by:   Alex Beattie
 * @Last modified time: 2018-04-26T22:12:01-05:00
 */
 // #include "bfs.c"
 // int main()
 // {
 //     create_graph();
 //     BF_Traversal();
 //     return 0;
 // }
 #include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <iostream>


using namespace std;
int gcd(int x, int y)
{
    int t;
     while (y!=0)
    {
        t=y;
        y=x%y;
        x=t;
    }
    return x;
}
int main()
{
    int   f,s,g,u,d;
    // f=99;
    // s=1;
    // g=10;
    // u=2;
    // d=1;
    f=70;
    s=2;
    g=1;
    u=1;
    d=0;
    // while(scanf("%d%d%d%d%d",&f,&s,&g,&u,&d)!=EOF)
    // {
        if(s==g)
        {
            printf("0\n");
            // continue;
        }
        if(u==0&&d!=0)
        {
            if((s-g)>0&&(s-g)%d==0)
            printf("%d\n",(s-g)/d);
            else
            printf("use the stairs\n");
            // continue;
        }
        else if(d==0&&u!=0)
        {
            if((g-s)>0&&(g-s)%u==0)
            printf("%d\n",(g-s)/u);
            else
            printf("use the stairs\n");
            // continue;
        }
        else if(d==0&&u==0)
        {
            printf("use the stairs\n");
            // continue;
        }
        int num=gcd(u,d);
        if((g-s)%num!=0)
        {
            printf("use the stairs\n");
            // continue;
        }
        int counts=0;
        printf("%d ",s);

        while(s!=g)
        {
            if(s+u>f&&s-d<1)
            break;
            if(s+u<=f&&s<=g)
            s+=u;
            else if(s-d<1&&s+u<=f)
            s+=u;
            else
            s-=d;
            printf("%d ",s);

            counts++;
        }
        printf("\n");
        if(s==g)
        printf("%d\n",counts);
        else
        printf("use the stairs\n");
    // }
}
